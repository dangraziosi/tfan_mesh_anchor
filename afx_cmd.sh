mkdir ./afx
for seq in {longdress,soldier,loot,redandblack}; do
mkdir ./afx/${seq}
for qp in {7,8,9,10,11,13,16}; do 
for qt in {7,8,9,10,11,13,16}; do
mkdir ./afx/${seq}/qp${qp}_qt${qt}
for fr in {0..31..1}; do 
if [ ${seq} == "soldier" ]; then
startFr=536
elif [ ${seq} == "loot" ]; then
startFr=1000
elif [ ${seq} == "redandblack" ]; then
startFr=1450
elif [ ${seq} == "longdress" ]; then
startFr=1051
fi
fr=$(printf "%04d" $((startFr+fr)))
echo seq=${seq} fr=${fr} qp=${qp} qt=${qt}
./bin/SC3DMCEncoder /mnt/IMF/DATASET/PCC/CAT2/8i/8iVFBv2/${seq}/Obj_textured/${seq}_vox10_${fr}_uv ./afx/${seq}/qp${qp}_qt${qt}/${seq}_vox10_${fr}_uv ./tfan.cfg ${qp} ${qt} > ./afx/${seq}/qp${qp}_qt${qt}/${seq}_vox10_${fr}_uv.log
./bin/SC3DMCDecoder ./afx/${seq}/qp${qp}_qt${qt}/${seq}_vox10_${fr}_uv ./afx/${seq}/qp${qp}_qt${qt}/${seq}_vox10_${fr}_uv
done;
done;
done;
done;
