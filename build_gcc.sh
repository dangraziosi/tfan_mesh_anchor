#!/bin/bash
# Example script for building SC3DMC with gcc

trap exit ERR

mkdir -p build.gcc
cd build.gcc
export CC=gcc-9.1
export CXX=g++-9.1
export CXXFLAGS="-Wall -Wextra -Wpedantic -std=c++17"
cmake \
    -DCMAKE_INSTALL_PREFIX=../install.gcc \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
    -G "Unix Makefiles" ..
cd ..

make -j 8 -C build.gcc VERBOSE=1
make -j 8 -C build.gcc test
make -j 8 -C build.gcc install
