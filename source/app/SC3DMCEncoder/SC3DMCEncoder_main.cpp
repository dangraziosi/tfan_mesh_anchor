//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom, ETRI and Hanyang University 2008, 2009
// Licensed Materials, Program Property of Institut Telecom, ETRI and Hanyang University
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// ETRI and Hanyang University retain full right to use the code 
// for his/her own purpose, assign or donate the code to a third party and
// to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//    Marius Preda
//    Seung Wook Lee
//    Daiyong Kim
//    KyoungSoo Son
//
//
//    http://www-artemis.it-sudparis.eu/
//    http://www.etri.re.kr/
//    http://dmlab.hanyang.ac.kr/
//    
//  
//    Marius Preda       (marius.preda@int-evry.fr)
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//    Seung Wook Lee     (tajinet@etri.re.kr)  
//    Daiyong Kim        (dykim@dmlab.hanyang.ac.kr)    
//    KyoungSoo Son      (bonokensin@dmlab.hanyang.ac.kr)
//
//-------------------------------------------------------------------------


#include "stdafx.h"
#include <string.h>
#include <math.h>
#include <time.h>
#include "IndexedFaceSet.h"
#include "SC3DMCEncoder.h"
#include "EncodingParameter.h"
#include <iostream>

bool Configure(char* filename, EncodingParams* par);

//void WCharToChar( char* pstrDest, const wchar_t* pwstrSrc )
//{
//
  //  int nLen = (int)wcslen(pwstrSrc);
    //wcstombs(pstrDest, pwstrSrc, nLen+1);
//}


//int _tmain(int argc, _TCHAR* argv[])
int main(int argc, char* argv[])
{
	printf("Usage:\n");
	printf("SC3DMCEncoder inputFileName outputFileName paramFileName\n\n");
	printf("	inputFileName:   OBJ describing a 3D triangular mesh \n");
	printf("	outputFileName:  location of the output compressed file \n");
	printf("	paramFileName:   file describing the encoding parameters\n");
	printf("OR\n\n");
	printf("SC3DMCEncoder inputFileName outputFileName paramFileName QP QT\n\n");
	printf("	inputFileName:   OBJ describing a 3D triangular mesh \n");
	printf("	outputFileName:  location of the output compressed file \n");
	printf("	paramFileName:   file describing the encoding parameters\n");
	printf("	QP:              overwrite quantization parameter for coordinates\n");
	printf("	QT:              overwrite quantization parameters for texture coordinates\n");
	printf("OR\n\n");
	printf("SC3DMCEncoder inputFileName outputFileName paramFileName  QP QT CoordMin[0] CoordMin[1] CoordMin[2] CoordMax[0] CoordMax[1] CoordMax[2]\n\n");
	printf("	inputFileName:   OBJ describing a 3D triangular mesh \n");
	printf("	outputFileName:  location of the output compressed file \n");
	printf("	paramFileName:   file describing the encoding parameters\n");
	printf("	QP:              overwrite quantization parameter for coordinates\n");
	printf("	QT:              overwrite quantization parameters for texture coordinates\n");
	printf("	CoordMin:        min value for coord quantization\n");
	printf("	CoordMax:        max value for coord quantization\n");
	printf("This software was developped by: \n");
	printf(" - Khaled Mammou (khaled_mamou@yahoo.fr, Institut Telecom)\n");
	printf(" - Seung Wook Lee (tajinet@etri.re.kr, ETRI/Hanyang University)\n");	
	printf(" - Daiyong Kim (dykim@dmlab.hanyang.ac.kr, ETRI/Hanyang University)\n");
	printf(" - KyoungSoo Son (bonokensin@dmlab.hanyang.ac.kr, ETRI/Hanyang University)\n");
	printf(" - Marius Preda (marius.preda@int-evry.fr, Institut Telecom)\n\n");
	printf("Copyright Institut Telecom (http://www-artemis.it-sudparis.eu/), ETRI (http://www.etri.re.kr/) and Hanyang University (http://dmlab.hanyang.ac.kr/) 2008, 2009\n");
	printf("Licensed Materials, Program Property of Institut Telecom, ETRI and Hanyang University.\n\n\n");
	if (!((argc == 4) || (argc == 6) || (argc == 12))) {
		printf("Error: we need 3, 5 or 11 arguments! (see usage)\n");
		return -1;
	}
	char fin[1024];
	sprintf(fin, "%s.obj", argv[1]);
	char fout[1024];
	sprintf(fout, "%s.s3d", argv[2]);
	char configfilename[1024];
    strcpy(configfilename,argv[3]);

	EncodingParams params;
	Configure(configfilename,&params);
	if (argc == 6) {
		//overwrite the quantization parameters
		params.m_nQCoordBits = atoi(argv[4]);
		params.m_nQTexCoordBits = atoi(argv[5]);
		params.m_nQTexCoordHeight = pow(2, params.m_nQTexCoordBits) - 1;
		params.m_nQTexCoordWidth = pow(2, params.m_nQTexCoordBits) - 1;
	}
	if (argc == 12) {
		//overwrite the quantization parameters
		params.m_nQCoordBits = atoi(argv[4]);
		params.m_nQTexCoordBits = atoi(argv[5]);
		params.m_nQTexCoordHeight = pow(2, params.m_nQTexCoordBits) - 1;
		params.m_nQTexCoordWidth = pow(2, params.m_nQTexCoordBits) - 1;
		//fix the quantization range
		params.m_explicitCoordQuantization = true;
		params.m_explicitTexCoordQuantization = true;
		params.m_minCoord[0] = atof(argv[6]);
		params.m_minCoord[1] = atof(argv[7]);
		params.m_minCoord[2] = atof(argv[8]);
		params.m_maxCoord[0] = atof(argv[9]);
		params.m_maxCoord[1] = atof(argv[10]);
		params.m_maxCoord[2] = atof(argv[11]);
	}

	printf("inputFileName         %s\n", fin);
	printf("outputFileName        %s\n", fout);
	printf("paramFileName         %s\n", configfilename);
	printf("encodingMode          %i\n", params.m_encodingMode);
	printf("binarizationMode      %i\n", params.m_binarizationMode);
	printf("qCoord                %i\n", params.m_nQCoordBits);
	printf("qTexture              %i\n", params.m_nQTexCoordBits);
	printf("qTextureHeight        %i\n", params.m_nQTexCoordHeight);
	printf("qTextureWidth         %i\n", params.m_nQTexCoordWidth);
	printf("qNormal               %i\n", params.m_nQNormalBits);
	printf("qColor                %i\n", params.m_nQColorBits);
	printf("qOtherAttributes      %i\n", params.m_nQOtherAttributesBits);
	printf("coordPred             %i\n", params.m_coordPredMode);
	printf("texturePred           %i\n", params.m_texCoordPredMode);
	printf("normalPred            %i\n", params.m_normalPredMode);
	printf("colorPred             %i\n", params.m_colorPredMode);
	printf("otherAttributesPred   %i\n", params.m_otherAttributesPredMode);
	printf("vertexOrderPres       %i\n", params.m_vertexOrderPres);
	printf("triangleOrderPres     %i\n", params.m_triangleOrderPres);
	printf("trianglePermPres      %i\n", params.m_trianglePermPres);
	printf("\n\n");


	IndexedFaceSet myIFS;
	// read file
	myIFS.LoadIFSOBJ(fin);
	if (params.m_nQColorBits == 0)
	{
		//not transmitting the color, so remove the elements in the structure
		myIFS.FreeMemColor();
	}
	if (params.m_nQNormalBits == 0)
	{
		//not transmitting the normal, so remove the elements in the structure
		myIFS.FreeMemNormal();
	}
	if (params.m_nQTexCoordBits == 0)
	{
		//not transmitting the texture coordinates, so remove the elements in the structure
		myIFS.FreeMemTexCoord();
	}

	double elapsedTime = 0.0;
	clock_t stopTime = 0;
	clock_t startTime = clock();

	SC3DMCEncoder myEncoder;
	myEncoder.SetIFS(&myIFS);
	myEncoder.SetParams(params);
	unsigned char * compressedStream = new unsigned char[myEncoder.GetEstimatedCompressedStreamSize()];
	unsigned int compressedStreamSize = 0;
	myEncoder.Encode(compressedStream, compressedStreamSize);
	WriteStream2File(fout, compressedStream, compressedStreamSize);
	delete [] compressedStream;


	stopTime = clock();
	elapsedTime = (stopTime - startTime) / (double)CLOCKS_PER_SEC;
	printf("\n(FILENAME) %s (FILESIZE) %d (TIME) %f\n", fout, compressedStreamSize+4, elapsedTime);
	return 0;
}

//dmlab
bool Configure(char* filename, EncodingParams* par)
{
	FILE *pfPara;

	if ((pfPara = fopen (filename, "r")) == NULL ){
		printf ("Parameter File Not Found\n");
		exit (1);
	}


	char rgBuf[80];
	char* tgBuf= NULL;
	char *pch = fgets(rgBuf, 79, pfPara);

	if(strncmp("!! SC3DMC !!", rgBuf, 12)!=0)
	{
		printf("Bad magic number at start of parameter file\n");
		exit(1);
	}

	while((pch = fgets(rgBuf, 79, pfPara)) != NULL)
	{
		//	printf("%s",rgBuf);
		if((strstr(rgBuf,"EncodingMode")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_encodingMode = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"BinarizationMode")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_binarizationMode = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"QuantizationParameter.Coord")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_nQCoordBits = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"QuantizationParameter.Normal")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_nQNormalBits = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"QuantizationParameter.Color")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_nQColorBits = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"QuantizationParameter.TexCoord")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_nQTexCoordBits = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"QuantizationParameter.OtherAttributes")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_nQOtherAttributesBits = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"TexCoord.Height")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_nQTexCoordHeight = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"TexCoord.Width")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_nQTexCoordWidth = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"PredictionMode.Coord")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_coordPredMode = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"PredictionMode.Normal")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_normalPredMode = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"PredictionMode.Color")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_colorPredMode = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"PredictionMode.TexCoord")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_texCoordPredMode = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"PredictionMode.OtherAttribute")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_otherAttributesPredMode = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"PredictionMode.IndexCoord")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_coordIndexPredMode = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"PredictionMode.IndexNormal")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_normalIndexPredMode = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"PredictionMode.IndexColor")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_colorIndexPredMode = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"PredictionMode.IndexTexCoord")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_texCoordIndexPredMode = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"PredictionMode.IndexOtherAttribute")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_otherAttributesIndexPredMode = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"PredictionMode.VertexOrder")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_vertexOrderPres = atoi(tgBuf);
		}
		else if((strstr(rgBuf,"PredictionMode.TriangleOrder")) != NULL)
		{
			tgBuf = strtok(rgBuf,"=");
			tgBuf = strtok(NULL," \n");
			par->m_triangleOrderPres = atoi(tgBuf);
		}
		else if ((strstr(rgBuf, "PredictionMode.TrianglePermutationOrder")) != NULL)
		{
		    tgBuf = strtok(rgBuf, "=");
		    tgBuf = strtok(NULL, " \n");
		    par->m_trianglePermPres = atoi(tgBuf);
		}
	}


	fclose(pfPara);
	return true;
}
//~dmlab
