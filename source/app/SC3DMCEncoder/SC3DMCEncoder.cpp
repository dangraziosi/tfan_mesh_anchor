//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom, ETRI and Hanyang University 2008, 2009
// Licensed Materials, Program Property of Institut Telecom, ETRI and Hanyang University
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// ETRI and Hanyang University retain full right to use the code 
// for his/her own purpose, assign or donate the code to a third party and
// to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//    Marius Preda
//    Seung Wook Lee
//    Daiyong Kim
//    KyoungSoo Son
//
//
//    http://www-artemis.it-sudparis.eu/
//    http://www.etri.re.kr/
//    http://dmlab.hanyang.ac.kr/
//    
//  
//    Marius Preda       (marius.preda@int-evry.fr)
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//    Seung Wook Lee     (tajinet@etri.re.kr)  
//    Daiyong Kim        (dykim@dmlab.hanyang.ac.kr)    
//    KyoungSoo Son      (bonokensin@dmlab.hanyang.ac.kr)
//
//-------------------------------------------------------------------------


#include "stdafx.h"
#include "SC3DMCEncoder.h"
#include "utils.h"


#define INIT_UCHAR 0x00
#define LEN_BYTE	8
#define LEN_BIT		1
#define LEN_QP		5
#define MIN_VALUE -1000000

//dmlab
void SC3DMCEncoder::EncodeHeader(unsigned char * compressedStream, unsigned int & compressedStreamSize)
{
	unsigned int startCode = SC3DMCStartCode;
	Write2Stream(compressedStream, compressedStreamSize, &startCode, sizeof(unsigned int));

	// This information will be filled after the encoding process
	unsigned int streamSize = 0;
	Write2Stream(compressedStream, compressedStreamSize, &streamSize, sizeof(unsigned int));

	unsigned char encodingMode = (unsigned char) m_params.m_encodingMode; // 3bit -> 8bit 
	Write2Stream(compressedStream, compressedStreamSize, &encodingMode, sizeof(unsigned char));
	
	float creaseAngle = m_ifs->GetCreaseAngle();
	Write2Stream(compressedStream, compressedStreamSize, &creaseAngle, sizeof(float));
	
	unsigned char compressionMask = 0;
	bool isTriangularMesh = true;
	bool otherAttributesPerVertex = true;
	bool markerBit = true;

	if (m_ifs->GetCCW()) compressionMask += 1;
	if (m_ifs->GetSolid()) compressionMask += 2;
	if (m_ifs->GetConvex()) compressionMask += 4;
	if (m_ifs->GetColorPerVertex()) compressionMask += 8;
	if (m_ifs->GetNormalPerVertex()) compressionMask += 16;
	if (otherAttributesPerVertex) compressionMask += 32;	// otherAttributesPerVertex true
	if (isTriangularMesh) compressionMask += 64;
	if (markerBit) compressionMask += 128;

	Write2Stream(compressedStream, compressedStreamSize, &compressionMask, sizeof(unsigned char)); 

	unsigned int numberOfCoord = m_ifs->GetNCoord();
	unsigned int numberOfNormal = m_ifs->GetNNormal();
	unsigned int numberOfColor = m_ifs->GetNColor();
	unsigned int numberOfTexCoord = m_ifs->GetNTexCoord();
	unsigned int numberOfOtherAttributes = 0;
	unsigned int dimensionOfOtherAttributes = 0;
	
	unsigned int numberOfCoordIndex = m_ifs->GetNCoordIndex();
	unsigned int numberOfNormalIndex = m_ifs->GetNNormalIndex();
	unsigned int numberOfColorIndex = m_ifs->GetNColorIndex();
	unsigned int numberOfTexCoordIndex = m_ifs->GetNTexCoordIndex();
	unsigned int numberOfOtherAttributesIndex = 0;

	Write2Stream(compressedStream, compressedStreamSize, &numberOfCoord, sizeof(unsigned int));
	Write2Stream(compressedStream, compressedStreamSize, &numberOfNormal, sizeof(unsigned int));
	Write2Stream(compressedStream, compressedStreamSize, &numberOfColor, sizeof(unsigned int));
	Write2Stream(compressedStream, compressedStreamSize, &numberOfTexCoord, sizeof(unsigned int));
	Write2Stream(compressedStream, compressedStreamSize, &numberOfOtherAttributes, sizeof(unsigned int));

	unsigned char QP=0;
	if (numberOfOtherAttributes >0)
	{
		unsigned char dOfOtherAttributes = dimensionOfOtherAttributes;
		Write2Stream(compressedStream, compressedStreamSize, &dOfOtherAttributes, sizeof(unsigned char));// 2bit -> 8 bit
	}
	if (numberOfCoord>0)
	{
		Write2Stream(compressedStream, compressedStreamSize, &numberOfCoordIndex, sizeof(unsigned int));
		QP = m_params.m_nQCoordBits;
		Write2Stream(compressedStream, compressedStreamSize, &QP, sizeof(unsigned char));  // 5bit -> 8bit
	}
	if (numberOfNormal>0)
	{
		Write2Stream(compressedStream, compressedStreamSize, &numberOfNormalIndex, sizeof(unsigned int));
		QP = m_params.m_nQNormalBits;
		Write2Stream(compressedStream, compressedStreamSize, &QP , sizeof(unsigned char));  // 5bit -> 8bit
	}
	if (numberOfColor>0)
	{
		Write2Stream(compressedStream, compressedStreamSize, &numberOfColorIndex, sizeof(unsigned int));
		QP = m_params.m_nQColorBits;
		Write2Stream(compressedStream, compressedStreamSize, &QP , sizeof(unsigned char));  // 5bit -> 8bit
	
	}
	if (numberOfTexCoord>0)
	{
		Write2Stream(compressedStream, compressedStreamSize, &numberOfTexCoordIndex, sizeof(unsigned int));
		QP = m_params.m_nQTexCoordBits;
		Write2Stream(compressedStream, compressedStreamSize, &QP , sizeof(unsigned char));  // 5bit -> 8bit

		int tData;
		tData = m_params.m_nQTexCoordWidth;
		Write2Stream(compressedStream, compressedStreamSize, &tData , sizeof(unsigned int)); 
		tData = m_params.m_nQTexCoordHeight;
		Write2Stream(compressedStream, compressedStreamSize, &tData , sizeof(unsigned int)); 
	}	
	
	if (numberOfOtherAttributes >0)
	{
		Write2Stream(compressedStream, compressedStreamSize, &numberOfOtherAttributesIndex, sizeof(unsigned int));
		QP = m_params.m_nQOtherAttributesBits;
		Write2Stream(compressedStream, compressedStreamSize, &QP , sizeof(unsigned char));  // 5bit -> 8bit
	}

	if (numberOfCoord>0)
	{
		if (m_params.m_explicitCoordQuantization) {
			for (int i = 0; i < 3; i++) {
				m_ifs->rangeCoord[i] = m_params.m_maxCoord[i] - m_params.m_minCoord[i];
				m_ifs->minCoord[i] = m_params.m_minCoord[i];
				m_ifs->maxCoord[i] = m_params.m_maxCoord[i];
			}
		} else
			m_ifs->SetMinMaxCoord();
		for(int j=0;j<3;j++)
		{
			Write2Stream(compressedStream, compressedStreamSize, & m_ifs->minCoord[j], sizeof(float)); 
		}
		
		for(int j=0;j<3;j++)
		{
			Write2Stream(compressedStream, compressedStreamSize, &(m_ifs->rangeCoord[j]), sizeof(float));  
		}
	}
	if(numberOfNormal > 0)
	{
		if (m_params.m_explicitNormalQuantization) {
			for (int i = 0; i < 3; i++) {
				m_ifs->rangeNormal[i] = m_params.m_maxNormal[i]- m_params.m_minNormal[i];
				m_ifs->minNormal[i] = m_params.m_minNormal[i];
				m_ifs->maxNormal[i] = m_params.m_maxNormal[i];
			}
		}
		else
			m_ifs->SetMinMaxNormal();
		for(int j=0;j<3;j++)
		{
			Write2Stream(compressedStream, compressedStreamSize, & m_ifs->minNormal[j], sizeof(float));  
		}
		for(int j=0;j<3;j++)
		{
			Write2Stream(compressedStream, compressedStreamSize, &(m_ifs->rangeNormal[j]), sizeof(float));  
		}
	}
	if(numberOfColor > 0)
	{
		if (m_params.m_explicitColorQuantization) {
			for (int i = 0; i < 3; i++) {
				m_ifs->rangeColor[i] = m_params.m_maxColor[i] - m_params.m_minColor[i];
				m_ifs->minColor[i] = m_params.m_minColor[i];
				m_ifs->maxColor[i] = m_params.m_maxColor[i];
			}
		}
		else
			m_ifs->SetMinMaxColor();
		
		for(int j=0;j<3;j++)
		{
			Write2Stream(compressedStream, compressedStreamSize, & m_ifs->minColor[j], sizeof(float));  
		}
		for(int j=0;j<3;j++)
		{
			Write2Stream(compressedStream, compressedStreamSize, &m_ifs->rangeColor[j], sizeof(float));  
		}
	}

	if(numberOfTexCoord > 0)
	{
		if (m_params.m_explicitTexCoordQuantization) {
			for (int i = 0; i < 2; i++) {
				m_ifs->rangeTexCoord[i] = m_params.m_maxTexCoord[i] - m_params.m_minTexCoord[i];
				m_ifs->minTexCoord[i] = m_params.m_minTexCoord[i];
				m_ifs->maxTexCoord[i] = m_params.m_maxTexCoord[i];
			}
		}
		else		
			m_ifs->SetMinMaxTexCoord();
		
		for(int j=0;j<2;j++)
		{
			Write2Stream(compressedStream, compressedStreamSize, & m_ifs->minTexCoord[j], sizeof(float));  
		}
		for(int j=0;j<2;j++)
		{
			Write2Stream(compressedStream, compressedStreamSize, &m_ifs->rangeTexCoord[j], sizeof(float));  
		}
	}

	if(dimensionOfOtherAttributes > 0 )
	{
		float* minOtherAtrribute;
		minOtherAtrribute = new float [dimensionOfOtherAttributes];
		int rangeOtherAtrribute = 0;
		for(unsigned int j=0;j<dimensionOfOtherAttributes;j++)
		{
			minOtherAtrribute[j] = 0.0;
		}
		if(numberOfOtherAttributes > 0)
		{
			for(unsigned int j=0;j<dimensionOfOtherAttributes;j++)
			{
				Write2Stream(compressedStream, compressedStreamSize, & minOtherAtrribute[j], sizeof(float));  
			}
			Write2Stream(compressedStream, compressedStreamSize, & rangeOtherAtrribute, sizeof(float));
		}
		delete [] minOtherAtrribute;
	}
}
//~dmlab



void SC3DMCEncoder::EncodeDataBuffer(unsigned char * compressedStream, unsigned int & compressedStreamSize)
{

	if (m_params.m_encodingMode == TFAN)
	{
		TFANEncoder tfenc;
		tfenc.SetIFS(m_ifs);
		tfenc.SetParams(&m_params);
		tfenc.EncodeDataBuffer(compressedStream+compressedStreamSize, compressedStreamSize);
	}
	else if (m_params.m_encodingMode == SVA) 
	{
		//dmlab
		SVAEncoder svaenc;
		svaenc.SetIFS(m_ifs);
		svaenc.SetParams(&m_params);
		svaenc.EncodeDataBuffer(compressedStream+compressedStreamSize, compressedStreamSize);
		//~dmlab
	
	}
	else if (m_params.m_encodingMode == QBCR) 
	{
		//dmlab
		QBCREncoder qbcrenc;
		qbcrenc.SetIFS(m_ifs);
		qbcrenc.SetParams(&m_params);
		qbcrenc.EncodeDataBuffer(compressedStream+compressedStreamSize, compressedStreamSize);
		//~dmlab
	}
}
