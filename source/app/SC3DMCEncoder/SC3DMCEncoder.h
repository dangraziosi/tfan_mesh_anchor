//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom, ETRI and Hanyang University 2008, 2009
// Licensed Materials, Program Property of Institut Telecom, ETRI and Hanyang University
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// ETRI and Hanyang University retain full right to use the code 
// for his/her own purpose, assign or donate the code to a third party and
// to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//    Marius Preda
//    Seung Wook Lee
//    Daiyong Kim
//    KyoungSoo Son
//
//
//    http://www-artemis.it-sudparis.eu/
//    http://www.etri.re.kr/
//    http://dmlab.hanyang.ac.kr/
//    
//  
//    Marius Preda       (marius.preda@int-evry.fr)
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//    Seung Wook Lee     (tajinet@etri.re.kr)  
//    Daiyong Kim        (dykim@dmlab.hanyang.ac.kr)    
//    KyoungSoo Son      (bonokensin@dmlab.hanyang.ac.kr)
//
//-------------------------------------------------------------------------

#pragma once
#include "utils.h"
#include "IndexedFaceSet.h"
#include "TFANEncoder.h"
#include "SVA_Encoder.h"
#include "QBCR_Encoder.h"
#include "EncodingParameter.h"
#include "BinaryIO.h"

class SC3DMCEncoder
{
	IndexedFaceSet * m_ifs;
	EncodingParams m_params;

	//dmlab
	unsigned char dumpBit;
	int bitPostion;
	//~dmlab

public:
	inline void SetIFS(IndexedFaceSet * ifs){ m_ifs = ifs;}
	inline void SetParams(EncodingParams params){ m_params = params;}

	// This function is called before encode
	// It should return the nombre of bytes to be allocated for the compressed stream
	unsigned int GetEstimatedCompressedStreamSize() { return 50000000;};

	// This function encodes the geometry represented as m_dim times m_nVertices array 
	// It returns the compressed stream as a char * and the size of the memory space used to store the compressed stream
	// Rq.: no memory allocation is done inside this function. The user should allocate and free the memory. 
	int Encode(unsigned char * compressedStream, unsigned int & compressedStreamSize)
	{
		compressedStreamSize = 0;
		EncodeHeader(compressedStream, compressedStreamSize);
		EncodeDataBuffer(compressedStream, compressedStreamSize);

		// write the compressedStreamSize in the header
		// we skip 4 bytes of the startCode
		memcpy(compressedStream+4, &compressedStreamSize, sizeof(unsigned int));
		return 0;
	}
	
	//	Writes the main stream
	void EncodeDataBuffer(unsigned char * compressedStream, unsigned int & compressedStreamSize);

	//	Writes the header
	void EncodeHeader(unsigned char * compressedStream, unsigned int & compressedStreamSize);

	SC3DMCEncoder(void):m_ifs(NULL), dumpBit(0), bitPostion(0){};
	//~dmlab
	~SC3DMCEncoder(void){};
};
