//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom, ETRI and Hanyang University 2008, 2009
// Licensed Materials, Program Property of Institut Telecom, ETRI and Hanyang University
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// ETRI and Hanyang University retain full right to use the code 
// for his/her own purpose, assign or donate the code to a third party and
// to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//    Marius Preda
//    Seung Wook Lee
//    Daiyong Kim
//    KyoungSoo Son
//
//
//    http://www-artemis.it-sudparis.eu/
//    http://www.etri.re.kr/
//    http://dmlab.hanyang.ac.kr/
//    
//  
//    Marius Preda       (marius.preda@int-evry.fr)
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//    Seung Wook Lee     (tajinet@etri.re.kr)  
//    Daiyong Kim        (dykim@dmlab.hanyang.ac.kr)    
//    KyoungSoo Son      (bonokensin@dmlab.hanyang.ac.kr)
//
//-------------------------------------------------------------------------

#pragma once

#include "utils.h"
#include "IndexedFaceSet.h"
#include "TFANDecoder.h"
#include "QBCR_Decoder.h"
#include "SVA_Decoder.h"
//dmlab
#include "SC3DMCHeader.h"
//~dmlab


class SC3DMCDecoder
{
	IndexedFaceSet * m_ifs;
	SC3DMCHeader m_Header; 
	int m_encodingMode; // 2=TFAN, 1=SVA, 0=QBCR
	
	unsigned int m_streamSize;

public:
	inline void SetIFS(IndexedFaceSet * ifs){ m_ifs = ifs;}
	inline IndexedFaceSet * GetIFS(){ return m_ifs;}
	inline unsigned int GetStreamSize(){ return m_streamSize;}

	int Decode(unsigned char * compressedStream, unsigned int compressedStreamSize)
	{
		if (DecodeHeader(compressedStream))
		{
			DecodeDataBuffer(compressedStream);
			return 0;
		}
		else
		{
			return -1;
		}
	}
	//	Reads the main stream
	void DecodeDataBuffer(unsigned char * &compressedStream);

	//	Reads the header
	bool DecodeHeader(unsigned char * & compressedStream);

	SC3DMCDecoder(void): m_ifs(NULL), m_encodingMode(0), m_streamSize(0){};
	//dmlab
	~SC3DMCDecoder(void){};
};
