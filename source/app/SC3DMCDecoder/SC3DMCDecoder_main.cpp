//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom, ETRI and Hanyang University 2008, 2009
// Licensed Materials, Program Property of Institut Telecom, ETRI and Hanyang University
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// ETRI and Hanyang University retain full right to use the code 
// for his/her own purpose, assign or donate the code to a third party and
// to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//    Marius Preda
//    Seung Wook Lee
//    Daiyong Kim
//    KyoungSoo Son
//
//
//    http://www-artemis.it-sudparis.eu/
//    http://www.etri.re.kr/
//    http://dmlab.hanyang.ac.kr/
//    
//  
//    Marius Preda       (marius.preda@int-evry.fr)
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//    Seung Wook Lee     (tajinet@etri.re.kr)  
//    Daiyong Kim        (dykim@dmlab.hanyang.ac.kr)    
//    KyoungSoo Son      (bonokensin@dmlab.hanyang.ac.kr)
//
//-------------------------------------------------------------------------



#include "stdafx.h"
#include <string.h>
#include <math.h>
#include <time.h>
#include "IndexedFaceSet.h"
#include "SC3DMCDecoder.h"

int main(int argc, char* argv[])
{
	//printf("Usage:\n");
	//printf("SC3DMCDecoder inputFileName\n\n");

	//printf("	inputFileName:   compressed stream\n");

	if (argc != 3) {
		printf("Error: we need 1 arguments! (see usage)\n");
		return -1;
	}
	/*
	printf("This software was developped by: \n");
	printf(" - Khaled Mammou (khaled_mamou@yahoo.fr, Institut Telecom)\n");
	printf(" - Seung Wook Lee (tajinet@etri.re.kr, ETRI/Hanyang University)\n");	
	printf(" - Daiyong Kim (dykim@dmlab.hanyang.ac.kr, ETRI/Hanyang University)\n");
	printf(" - KyoungSoo Son (bonokensin@dmlab.hanyang.ac.kr, ETRI/Hanyang University)\n");
	printf(" - Marius Preda (marius.preda@int-evry.fr, Institut Telecom)\n\n");

	printf("Copyright Institut Telecom (http://www-artemis.it-sudparis.eu/), ETRI (http://www.etri.re.kr/) and Hanyang University (http://dmlab.hanyang.ac.kr/) 2008, 2009\n");
	printf("Licensed Materials, Program Property of Institut Telecom, ETRI and Hanyang University.\n\n\n");
*/

	//dmlab
	char filname[1024];
    strcpy(filname,argv[1]);
	char fin[1024];
	sprintf(fin, "%s.s3d",argv[1]);
	char fD[1024];
	sprintf(fD, "%s.obj", argv[2]);
	//~dmlab

	IndexedFaceSet myIFS;
	
	double elapsedTime = 0.0;
	clock_t stopTime = 0;
	clock_t startTime = clock();
	
	unsigned int compressedStreamSize = 0;
	unsigned char * compressedStream = ReadStream2File(fin,  compressedStreamSize);

	SC3DMCDecoder myDecoder;
	myDecoder.SetIFS(&myIFS);
		
	myDecoder.Decode(compressedStream, compressedStreamSize);
	delete [] compressedStream;
	
	


	stopTime = clock();
	elapsedTime = (stopTime - startTime) / (double)CLOCKS_PER_SEC;
	printf("%s\t%f\n", filname, elapsedTime);
	myIFS.SaveIFSOBJ(fD);

	return 0;
}

