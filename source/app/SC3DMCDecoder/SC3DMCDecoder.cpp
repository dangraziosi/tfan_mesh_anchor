//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom, ETRI and Hanyang University 2008, 2009
// Licensed Materials, Program Property of Institut Telecom, ETRI and Hanyang University
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// ETRI and Hanyang University retain full right to use the code 
// for his/her own purpose, assign or donate the code to a third party and
// to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//    Marius Preda
//    Seung Wook Lee
//    Daiyong Kim
//    KyoungSoo Son
//
//
//    http://www-artemis.it-sudparis.eu/
//    http://www.etri.re.kr/
//    http://dmlab.hanyang.ac.kr/
//    
//  
//    Marius Preda       (marius.preda@int-evry.fr)
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//    Seung Wook Lee     (tajinet@etri.re.kr)  
//    Daiyong Kim        (dykim@dmlab.hanyang.ac.kr)    
//    KyoungSoo Son      (bonokensin@dmlab.hanyang.ac.kr)
//
//-------------------------------------------------------------------------


#include "stdafx.h"
#include "SC3DMCDecoder.h"


//dmlab
bool SC3DMCDecoder::DecodeHeader(unsigned char * & compressedStream)
{
	unsigned char * compressedStreamO = compressedStream;
	unsigned int index;
	unsigned int startCode = 0;
	
	ReadFromStream(compressedStream, &startCode, sizeof(unsigned int));

	if (startCode != SC3DMCStartCode) return false;  //delete startcode

	m_streamSize = 0;
	ReadFromStream(compressedStream, &m_streamSize, sizeof(unsigned int)); 

	ReadFromStream(compressedStream, &m_encodingMode, sizeof(unsigned char)); // changed name

	float creaseAngle = 0.0;
	ReadFromStream(compressedStream, &creaseAngle, sizeof(float));
	m_ifs->SetCreaseAngle(creaseAngle);

	unsigned char compressionMask = 0;
	ReadFromStream(compressedStream, &compressionMask, sizeof(unsigned char));


	if (compressionMask & 1) m_ifs->SetCCW(true);
	else m_ifs->SetCCW(false);
	if (compressionMask & 2) m_ifs->SetSolid(true);
	else m_ifs->SetSolid(false);
	if (compressionMask & 4) m_ifs->SetConvex(true);
	else m_ifs->SetConvex(false);
	if (compressionMask & 8) m_ifs->SetColorPerVertex(true);
	else  m_ifs->SetColorPerVertex(false);
	if (compressionMask & 16) m_ifs->SetNormalPerVertex(true);
	else m_ifs->SetNormalPerVertex(false);
	if (compressionMask & 32) m_Header.otherAttributesPerVertex = true;
	else m_Header.otherAttributesPerVertex = false;
	if (compressionMask & 64) m_Header.isTriangularMesh = true;  // 
	else m_Header.isTriangularMesh = false;
		
	if (compressionMask & 128) int markerBit = 1; // --> insert markerBit	(always 1)


	unsigned int numberOfCoord = 0;
	unsigned int numberOfNormal = 0;
	unsigned int numberOfColor = 0;
	unsigned int numberOfTexCoord =0;
	unsigned int numberOfOtherAttributes = 0;
	unsigned char dimensionOfOtherAttributes = 0;
	
	unsigned int numberOfCoordIndex = 0;
	unsigned int numberOfNormalIndex = 0;
	unsigned int numberOfColorIndex = 0;
	unsigned int numberOfTexCoordIndex = 0;
	unsigned int numberOfOtherAttributesIndex = 0;

	ReadFromStream(compressedStream, &numberOfCoord, sizeof(unsigned int));
	ReadFromStream(compressedStream, &numberOfNormal, sizeof(unsigned int));
	ReadFromStream(compressedStream, &numberOfColor, sizeof(unsigned int));
	ReadFromStream(compressedStream, &numberOfTexCoord, sizeof(unsigned int));
	ReadFromStream(compressedStream, &numberOfOtherAttributes, sizeof(unsigned int));



	if (numberOfOtherAttributes >0)
	{
		ReadFromStream(compressedStream, &dimensionOfOtherAttributes, sizeof(unsigned char));
		m_Header.dimensionOfOtherAttribute = dimensionOfOtherAttributes;
	}

	unsigned char QP=0;
	if (numberOfCoord>0)
	{
		ReadFromStream(compressedStream, &numberOfCoordIndex, sizeof(unsigned int));		
		ReadFromStream(compressedStream, &QP, sizeof(unsigned char));
		m_Header.QPforGeometry = QP;
	}

	if (numberOfNormal>0)
	{
		ReadFromStream(compressedStream, &numberOfNormalIndex, sizeof(unsigned int));		
		ReadFromStream(compressedStream, &QP, sizeof(unsigned char));
		m_Header.QPforNormal = QP;
	}

	
	if (numberOfColor>0)
	{
		ReadFromStream(compressedStream, &numberOfColorIndex, sizeof(unsigned int));		
		ReadFromStream(compressedStream, &QP, sizeof(unsigned char));
		m_Header.QPforColor = QP;
	}

	if (numberOfTexCoord>0)
	{
		ReadFromStream(compressedStream, &numberOfTexCoordIndex, sizeof(unsigned int));		
		ReadFromStream(compressedStream, &QP, sizeof(unsigned char));;
		m_Header.QPforTexCoord = QP;
		int tDtata=0;
		ReadFromStream(compressedStream, &tDtata, sizeof(unsigned int));;
		m_Header.TexCoordWidth = tDtata;
		ReadFromStream(compressedStream, &tDtata, sizeof(unsigned int));;
		m_Header.TexCoordHeight = tDtata;
	}

	if (numberOfOtherAttributes >0)
	{
		ReadFromStream(compressedStream, &numberOfOtherAttributes, sizeof(unsigned int));		
		ReadFromStream(compressedStream, &QP, sizeof(unsigned char));;
		m_Header.QPforOtherAttribute = QP;
	}

	m_ifs->SetNCoord(numberOfCoord);
	m_ifs->SetNNormal(numberOfNormal);
	m_ifs->SetNColor(numberOfColor);
	m_ifs->SetNTexCoord(numberOfTexCoord);	
	m_ifs->SetNCoordIndex(numberOfCoordIndex);
	m_ifs->SetNNormalIndex(numberOfNormalIndex);
	m_ifs->SetNColorIndex(numberOfColorIndex);
	m_ifs->SetNTexCoordIndex(numberOfTexCoordIndex);
	m_ifs->AllocateMem();

	float fValue=0.0;
	int quantmode=0;
	if (numberOfCoord>0)
	{
		for(index = 0 ; index < 3; index++)
		{
			ReadFromStream(compressedStream, &fValue, sizeof(float));		
			m_Header.quantMinGeometry[index] = fValue;
		}
		for(index = 0 ; index < 3; index++)
		{
			ReadFromStream(compressedStream, &fValue, sizeof(float));
			m_Header.rangeGeometry[index ] = fValue;
		}
	}
	
	
	if(numberOfNormal > 0)
	{
		for(index = 0 ; index < 3; index++)
		{
			ReadFromStream(compressedStream, &fValue, sizeof(float));	
			m_Header.quantMinNormal[index] = fValue;
		}
		for(index = 0 ; index < 3; index++)
		{
			ReadFromStream(compressedStream, &fValue, sizeof(float));
			m_Header.rangeNormal[index ] = fValue;
		}
	}
	if (numberOfColor>0)
	{
		for(index = 0 ; index < 3; index++)
		{
			ReadFromStream(compressedStream, &fValue, sizeof(float));	
			m_Header.quantMinColor[index] = fValue;
		}
		for(index = 0 ; index < 3; index++)
		{
			ReadFromStream(compressedStream, &fValue, sizeof(float));
			m_Header.rangeColor[index ] = fValue;
		}
		
	}
	
	if (numberOfTexCoord>0)
	{
		for(index = 0 ; index < 2; index++)
		{
			ReadFromStream(compressedStream, &fValue, sizeof(float));	
			m_Header.quantMinTexCoord[index] = fValue;
		}
		for(index = 0 ; index < 2; index++)
		{
			ReadFromStream(compressedStream, &fValue, sizeof(float));
			m_Header.rangeTexCoord[index ] = fValue;
		}
		
		
	}
	if(m_Header.dimensionOfOtherAttribute > 0)
	{
		m_Header.quantMinOtherAttribute = new float [m_Header.dimensionOfOtherAttribute];
		if (numberOfOtherAttributes >0)
		{
			ReadFromStream(compressedStream, &quantmode, sizeof(unsigned char));	
			m_Header.quantmodeOtherAttribute = quantmode;
			for(index = 0 ; index < m_Header.dimensionOfOtherAttribute; index++)
			{
				ReadFromStream(compressedStream, &fValue, sizeof(float));	
				m_Header.quantMinOtherAttribute[index] = fValue;
			}
			if(m_Header.quantmodeTexCoord == 0)
			{
				ReadFromStream(compressedStream, &fValue, sizeof(float));
				m_Header.quantRangeOtherAttribute = fValue;
			}
			else
			{
				for(index = 0 ; index < m_Header.dimensionOfOtherAttribute; index++)
				{
					ReadFromStream(compressedStream, &fValue, sizeof(float));
					m_Header.rangeOtherAttribute[index ] = fValue;
				}
			}
			
		}
	}

	return true;
}

//~dmlab

void SC3DMCDecoder::DecodeDataBuffer(unsigned char * &compressedStream)
{
	
	if (m_encodingMode == QBCR) 
	{	
		QBCRDecoder qbcrdec;
		//dmlab
		qbcrdec.SetIFS(m_ifs);
		qbcrdec.SetHeader(&m_Header);
		//~dmlab
		qbcrdec.DecodeDataBuffer(compressedStream); 
	}
	
	else if (m_encodingMode == SVA) 
	{
	SVADecoder svadec;
		//dmlab
		svadec.SetIFS(m_ifs);
		svadec.SetHeader(&m_Header);
		//~dmlab
		svadec.DecodeDataBuffer(compressedStream); 
	}
	else if (m_encodingMode == TFAN)
	{	
		TFANDecoder tfandec;
		tfandec.SetIFS(m_ifs);
		tfandec.SetHeader(&m_Header);
		tfandec.DecodeTFANData(compressedStream);
	}
}