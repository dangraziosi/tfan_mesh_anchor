//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom, ETRI and Hanyang University 2008, 2009
// Licensed Materials, Program Property of Institut Telecom, ETRI and Hanyang University
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// ETRI and Hanyang University retain full right to use the code 
// for his/her own purpose, assign or donate the code to a third party and
// to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//    Marius Preda
//    Seung Wook Lee
//    Daiyong Kim
//    KyoungSoo Son
//
//
//    http://www-artemis.it-sudparis.eu/
//    http://www.etri.re.kr/
//    http://dmlab.hanyang.ac.kr/
//    
//  
//    Marius Preda       (marius.preda@int-evry.fr)
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//    Seung Wook Lee     (tajinet@etri.re.kr)  
//    Daiyong Kim        (dykim@dmlab.hanyang.ac.kr)    
//    KyoungSoo Son      (bonokensin@dmlab.hanyang.ac.kr)
//
//-------------------------------------------------------------------------



#ifndef _QBCRDECODER_H_
#define _QBCRDECODER_H_


#include "IndexedFaceSet.h"
#include "Arithmetic_Codec.h"
#include "SC3DMCHeader.h"
#include "utils.h"

class QBCRDecoder{
	SC3DMCHeader * m_header;
	IndexedFaceSet * m_ifs;	
	int m_vertexOrderPres;
	int m_triangleOrderPres;
	int m_trianglePermPres;
	unsigned char m_meshType;

	DecoderStreamInfo m_streamInfo;

	int m_binarizationMode;

	int m_coordPredMode; 
	int m_texCoordPredMode; 
	int m_normalPredMode; 
	int m_colorPredMode; 
	int m_otherAttributesPredMode; 
	
	int m_coordIndexPredMode; 
	int m_texCoordIndexPredMode; 
	int m_normalIndexPredMode; 
	int m_colorIndexPredMode; 
	int m_otherAttributesIndexPredMode;

public:
	inline void SetHeader(SC3DMCHeader * header) {m_header = header;}
	inline void SetIFS(IndexedFaceSet * ifs){ m_ifs = ifs;}
	inline IndexedFaceSet * GetIFS(){ return m_ifs;}

	void DecodeDataBuffer(unsigned char * &compressedStream);
	QBCRDecoder():m_ifs(NULL), m_meshType(0){};

	~QBCRDecoder(void){};
};

#endif //_QBCRDECODER_H_
