//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom, ETRI and Hanyang University 2008, 2009
// Licensed Materials, Program Property of Institut Telecom, ETRI and Hanyang University
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// ETRI and Hanyang University retain full right to use the code 
// for his/her own purpose, assign or donate the code to a third party and
// to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//    Marius Preda
//    Seung Wook Lee
//    Daiyong Kim
//    KyoungSoo Son
//
//
//    http://www-artemis.it-sudparis.eu/
//    http://www.etri.re.kr/
//    http://dmlab.hanyang.ac.kr/
//    
//  
//    Marius Preda       (marius.preda@int-evry.fr)
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//    Seung Wook Lee     (tajinet@etri.re.kr)  
//    Daiyong Kim        (dykim@dmlab.hanyang.ac.kr)    
//    KyoungSoo Son      (bonokensin@dmlab.hanyang.ac.kr)
//
//-------------------------------------------------------------------------



#ifndef _QBCRENCODER_H
#define _QBCRENCODER_H

#include "IndexedFaceSet.h"
#include "EncodingParameter.h"
#include "utils.h"

class QBCREncoder{
private:

	IndexedFaceSet * m_ifs;
	EncodingParams * m_params;
	EncoderStreamInfo m_streamInfo;

public:
	inline void SetIFS(IndexedFaceSet * ifs){ m_ifs = ifs;}
	inline void SetParams(EncodingParams * params){ m_params = params;}

	// This function is called before encode
	// It should return the numbre of bytes to be allocated for the compressed stream
	unsigned int GetEstimatedCompressedStreamSize() { return 50000000;};

	
	//	Writes the main stream
	void EncodeDataBuffer(unsigned char * compressedStream, unsigned int & compressedStreamSize);

	QBCREncoder(void):m_ifs(NULL), m_params(NULL){};
	~QBCREncoder(void){};
};


#endif //_QBCRENCODER

