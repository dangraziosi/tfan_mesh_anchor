//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom, ETRI and Hanyang University 2008, 2009
// Licensed Materials, Program Property of Institut Telecom, ETRI and Hanyang University
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// ETRI and Hanyang University retain full right to use the code 
// for his/her own purpose, assign or donate the code to a third party and
// to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//    Marius Preda
//    Seung Wook Lee
//    Daiyong Kim
//    KyoungSoo Son
//
//
//    http://www-artemis.it-sudparis.eu/
//    http://www.etri.re.kr/
//    http://dmlab.hanyang.ac.kr/
//    
//  
//    Marius Preda       (marius.preda@int-evry.fr)
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//    Seung Wook Lee     (tajinet@etri.re.kr)  
//    Daiyong Kim        (dykim@dmlab.hanyang.ac.kr)    
//    KyoungSoo Son      (bonokensin@dmlab.hanyang.ac.kr)
//
//-------------------------------------------------------------------------

#include "QBCR_Decoder.h"



void QBCRDecoder::DecodeDataBuffer(unsigned char * &compressedStream)
{
	m_streamInfo.m_compressedStream = compressedStream; 

	// decoding the coordIndex	
	DecodeIntArray(m_streamInfo, m_ifs->GetNCoord(), m_ifs->GetCoordIndex(), m_ifs->GetNCoordIndex() * 3, 1, m_coordIndexPredMode, 
					 m_binarizationMode, int(log((float)(m_ifs->GetNCoordIndex())) / log(2.0)) +1);
	// decoding the normalIndex	
	if (m_ifs->GetNNormalIndex() > 0)
	{
		if (m_ifs->GetNormalPerVertex())
		{
			DecodeIntArray(m_streamInfo, m_ifs->GetNNormal(), m_ifs->GetNormalIndex(), m_ifs->GetNNormalIndex() * 3, 1, m_normalIndexPredMode, 
					 m_binarizationMode, int(log((float)(m_ifs->GetNNormalIndex())) / log(2.0)) +1);
		}
		else
		{
			DecodeIntArray(m_streamInfo, m_ifs->GetNNormal(), m_ifs->GetNormalIndex(), m_ifs->GetNNormalIndex(), 1, m_normalIndexPredMode, 
					 m_binarizationMode, int(log((float)(m_ifs->GetNNormalIndex())) / log(2.0)) +1);
		}
	}
	// decoding the colorIndex	
	if (m_ifs->GetNColorIndex() > 0)
	{
		if (m_ifs->GetColorPerVertex())
		{
			DecodeIntArray(m_streamInfo, m_ifs->GetNColor(), m_ifs->GetColorIndex(), m_ifs->GetNColorIndex() * 3, 1, m_colorIndexPredMode, 
					 m_binarizationMode, int(log((float)(m_ifs->GetNColorIndex())) / log(2.0)) +1);
		}
		else
		{
			DecodeIntArray(m_streamInfo, m_ifs->GetNColor(), m_ifs->GetColorIndex(), m_ifs->GetNColorIndex(), 1, m_colorIndexPredMode, 
					 m_binarizationMode, int(log((float)(m_ifs->GetNColorIndex())) / log(2.0)) +1);
		}
	}
	// decoding the texCoordIndex	
	if (m_ifs->GetNTexCoordIndex() > 0)
		DecodeIntArray(m_streamInfo, m_ifs->GetNTexCoord(), m_ifs->GetTexCoordIndex(), m_ifs->GetNTexCoordIndex() * 3, 1, m_texCoordIndexPredMode, 
					 m_binarizationMode, int(log((float)(m_ifs->GetNTexCoordIndex())) / log(2.0)) +1);


	// decoding the coord
	int m_QuantizationParameter[3] ={0};

	if (m_ifs->GetNCoord() > 0)
	{
		m_QuantizationParameter[0] = m_header->QPforGeometry;
		DecodeFloatArray(m_streamInfo, m_ifs->GetCoord(), m_ifs->GetNCoord(), 3, m_coordPredMode, 
					 m_binarizationMode, 0, m_QuantizationParameter,
					 static_cast<float *> (m_header->quantMinGeometry), static_cast<float *> (m_header->rangeGeometry),
					 m_ifs->GetNCoordIndex(), m_ifs->GetCoordIndex(), NULL, NULL);
	}

	// decoding the normal
	if (m_ifs->GetNNormal() > 0)
	{
		if (m_ifs->GetNormalPerVertex())
		{
			m_QuantizationParameter[0] = m_header->QPforNormal;
			if (m_ifs->GetNNormalIndex()>0)
			{
				DecodeFloatArray(m_streamInfo, m_ifs->GetNormal(), m_ifs->GetNNormal(), 1, m_normalPredMode, 
					 m_binarizationMode, 1, m_QuantizationParameter,
					 static_cast<float *> (m_header->quantMinNormal), static_cast<float *> (m_header->rangeNormal),
					 m_ifs->GetNNormalIndex(), m_ifs->GetNormalIndex(), NULL, NULL);
			}
			else
			{
				DecodeFloatArray(m_streamInfo, m_ifs->GetNormal(), m_ifs->GetNNormal(), 1, m_normalPredMode, 
					 m_binarizationMode,  1, m_QuantizationParameter,
					 static_cast<float *> (m_header->quantMinNormal), static_cast<float *> (m_header->rangeNormal),
					 m_ifs->GetNCoordIndex(), m_ifs->GetCoordIndex(), NULL, NULL);
			}
		}
		else
		{
			DecodeFloatArray(m_streamInfo, m_ifs->GetNormal(), m_ifs->GetNNormal(), 1, m_normalPredMode, 
					 m_binarizationMode, 1, m_QuantizationParameter,
					 static_cast<float *> (m_header->quantMinNormal), static_cast<float *> (m_header->rangeNormal),0,0,NULL,NULL);
		}	
	}

	// decoding the color
	if (m_ifs->GetNColor() > 0)
	{
		m_QuantizationParameter[0] = m_header->QPforNormal;
		if (m_ifs->GetColorPerVertex())
		{
			
			if (m_ifs->GetNColorIndex()>0)
			{
				DecodeFloatArray(m_streamInfo, m_ifs->GetColor(), m_ifs->GetNColor(), 3, m_colorPredMode, 
					 m_binarizationMode, 0, m_QuantizationParameter, 
					 static_cast<float *> (m_header->quantMinColor), static_cast<float *> (m_header->rangeColor),
						 m_ifs->GetNColorIndex(), m_ifs->GetColorIndex(), NULL, NULL);
			}
			else
			{
				DecodeFloatArray(m_streamInfo, m_ifs->GetColor(), m_ifs->GetNColor(), 3, m_colorPredMode, 
					 m_binarizationMode, 0, m_QuantizationParameter,
					 static_cast<float *> (m_header->quantMinColor), static_cast<float *> (m_header->rangeColor),
						 m_ifs->GetNCoordIndex(), m_ifs->GetCoordIndex(), NULL, NULL);
			}
		}
		else
		{
				DecodeFloatArray(m_streamInfo, m_ifs->GetColor(), m_ifs->GetNColor(), 3, m_colorPredMode, 
					 m_binarizationMode, 0, m_QuantizationParameter,
					 static_cast<float *> (m_header->quantMinColor), static_cast<float *> (m_header->rangeColor),0,0,NULL,NULL);
		}	
	}	
	
	// decoding the texCoord
	if (m_ifs->GetNTexCoord() > 0)
	{
		m_QuantizationParameter[1] = m_header->TexCoordWidth;
		m_QuantizationParameter[2] = m_header->TexCoordHeight;
		if( m_header->TexCoordWidth >  m_header->TexCoordHeight)
			m_QuantizationParameter[0] = int(log((float)(m_header->TexCoordWidth)) / log(2.0))+1;
		else
			m_QuantizationParameter[0] = int(log((float)(m_header->TexCoordHeight)) /log(2.0))+1;

		if (m_ifs->GetNTexCoordIndex()>0)
		{
				DecodeFloatArray(m_streamInfo, m_ifs->GetTexCoord(), m_ifs->GetNTexCoord(), 2, m_texCoordPredMode, 
					 m_binarizationMode, 2,  m_QuantizationParameter,
					 static_cast<float *> (m_header->quantMinTexCoord), static_cast<float *> (m_header->rangeTexCoord),
					 m_ifs->GetNTexCoordIndex(), m_ifs->GetTexCoordIndex(), NULL, NULL);
		}
		else
		{
				DecodeFloatArray(m_streamInfo, m_ifs->GetTexCoord(), m_ifs->GetNTexCoord(), 2, m_texCoordPredMode, 
					 m_binarizationMode, 2,  m_QuantizationParameter,
					 static_cast<float *> (m_header->quantMinTexCoord), static_cast<float *> (m_header->rangeTexCoord),
					 m_ifs->GetNCoordIndex(), m_ifs->GetCoordIndex(), NULL, NULL);
		}
	}

	compressedStream = m_streamInfo.m_compressedStream;
}

