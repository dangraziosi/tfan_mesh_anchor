//-------------------------------------------------------------------------
// 
// Copyright ETRI, Hanyang University 2008, 2009
// Licensed Materials, Program Property of ETRI & Hanyang University
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.
// ETRI and Hanyang University retain full right to use the code 
// for his/her own purpose, assign or donate the code to a third party and
// to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Seung Wook Lee (ETRI)
//    Daiyong Kim (Hanyang University)
//    KyoungSoo Son (Hanyang University)
//
//    http://www.etri.re.kr/
//    http://dmlab.hanyang.ac.kr/
//  
//    Seung Wook Lee     (tajinet@etri.re.kr)  
//    Daiyong Kim        (dykim@dmlab.hanyang.ac.kr)    
//    KyoungSoo Son      (bonokensin@dmlab.hanyang.ac.kr)
//
//-------------------------------------------------------------------------
#pragma warning (disable : 4786)


#include "SVA_IndexDecoder.h"
#include "Arithmetic_Codec.h"
#include <cstdlib>

#define LEN_BIT		1

SVAIndexDecoder::SVAIndexDecoder()
{
}

SVAIndexDecoder::~SVAIndexDecoder()
{
}

void SVAIndexDecoder::Decode(DecoderStreamInfo &streamInfo, int *pIndex, int numberOfIndex, int numberOfData)
{
	index = pIndex;
	nIndex_ = numberOfIndex;
	nData = numberOfData;
	
	unsigned char mask = 0;	
	ReadFromStream(streamInfo.m_compressedStream, &mask, sizeof(unsigned char));
	int predictionMode = mask & 7;
	int binarizationMode = mask>>4;

	DecodeConnectivity(streamInfo,binarizationMode);
	Flush(streamInfo);
}

void SVAIndexDecoder::DecodeConnectivity(DecoderStreamInfo &streamInfo, int binarization)
{
	if(binarization == BP)
	{
		unsigned int prefixSize, prefixSize_nP, prefixSize_fd, prefixSize_nR, nBPL, nSign;
		int nLoop;
//		int difValue;
		int FDmode = 0;
		int FDvalue = 0;
		int * BPLTable = new int [32];
		BPLTable[0] = 0;
		BPLTable[1] = 1;
		BPLTable[2] = 2;
		for(int i = 3; i <32; i++)
		{
			BPLTable[i] = static_cast<int>(pow(2.0,(double)(i-1))-1);
		}
        //==================================================
        unsigned long long sizeofbs=0;
        unsigned int tCount, nCount;
        unsigned int a_counter=0;

        FDmode = (int)ReadBitsFromStorage(streamInfo,LEN_BIT);
        if(FDmode == 0)
		{
			FDvalue = (int)ReadBitsFromStorage(streamInfo,LEN_BIT);
		}
        Flush(streamInfo);

        ReadFromStream(streamInfo.m_compressedStream, &sizeofbs, 4);
		
        //Read nType header
        unsigned char * nType = new unsigned char [nIndex_]; 

        prefixSize = ReadBitsFromStorage(streamInfo,LEN_PREFIX);
		Flush(streamInfo);
		for(nLoop = 0 ; nLoop < (nIndex_ - 1); ++nLoop){
            nBPL = ReadBitsFromStorage(streamInfo,prefixSize);
			nType[nLoop] = BPLTable[nBPL];
			
            if(nBPL>2)
                nBPL = nBPL;
        }
        Flush(streamInfo);
        /*int temptemp=(a_counter%8)-1;
        for(nLoop = 0 ; nLoop < (a_counter%8); ++nLoop){
		    ReadBitsFromStorage(streamInfo,1);
        }*/
        sizeofbs=0;
        //Read nType into data array
        //==================================================
        ReadFromStream(streamInfo.m_compressedStream, &sizeofbs, 4);
        //Read bitstream header
        //==================================================
		//FDmode = (int)ReadBitsFromStorage(streamInfo,LEN_BIT);
		
		prefixSize = ReadBitsFromStorage(streamInfo,LEN_PREFIX);
		prefixSize_nP = ReadBitsFromStorage(streamInfo,LEN_PREFIX);
		prefixSize_fd = ReadBitsFromStorage(streamInfo,LEN_PREFIX);
		prefixSize_nR = ReadBitsFromStorage(streamInfo,LEN_PREFIX);
		Flush(streamInfo); 

		unsigned int /*nFaceDirection,*/ nCoordIndex, nPosition, nRotation;
		int nDifCoordIndex[3];
		
		unsigned int nIndex;
		unsigned int tCoordIndex[3];
		unsigned int tChange;

		for(nLoop = 0 ; nLoop < 3; nLoop++)
		{
			nBPL = ReadBitsFromStorage(streamInfo,prefixSize);
			nCoordIndex = BPLTable[nBPL];
			if(nBPL > 2)
			{
				nCoordIndex += ReadBitsFromStorage(streamInfo,nBPL - 1);
			}
			index[nLoop] = (nCoordIndex);
		}
		
		
		int temp[3];
			for(nLoop = 0 ; nLoop < (nIndex_ - 1); ++nLoop)
			{
				if (nLoop == nIndex_ - 2)
					nLoop = nLoop;
				
				switch(nType[nLoop])
				{				
				case 0:
					/*nPosition = ReadBitsFromStorage(streamInfo,1);
					if(nPosition == 1)
					{
						nPosition += ReadBitsFromStorage(streamInfo,1);
					}*/
					nBPL = ReadBitsFromStorage(streamInfo,prefixSize_nP);
					nPosition = BPLTable[nBPL];
				
                    if(FDmode == 1){
                    //    FDvalue = ReadBitsFromStorage(streamInfo,1);
						nBPL = ReadBitsFromStorage(streamInfo,prefixSize_fd);
						FDvalue = BPLTable[nBPL];
                    }
					//nFaceDirection = ReadBitsFromStorage(streamInfo,1);
					nBPL = ReadBitsFromStorage(streamInfo,prefixSize);
					nDifCoordIndex[0] = BPLTable[nBPL];
					if(nBPL > 2)
					{
						nDifCoordIndex[0] += ReadBitsFromStorage(streamInfo,nBPL - 1);
					}

					temp[0] = nDifCoordIndex[0];
					//nDifCoordIndex[0]+=1;

                    if (nDifCoordIndex[0]!=0){
					    nSign = ReadBitsFromStorage(streamInfo,1);
					    if(nSign == 1)
					    {
						    nDifCoordIndex[0] = -(nDifCoordIndex[0]);
					    }
                    }
					
					/*nRotation = ReadBitsFromStorage(streamInfo,1);
					if(nRotation == 1)
					{
						nRotation += ReadBitsFromStorage(streamInfo,1);
					}*/
					nBPL = ReadBitsFromStorage(streamInfo,prefixSize_nR);
						nRotation = BPLTable[nBPL];
					
					for(nIndex = 0; nIndex < 3; nIndex++)
					{
						if(nIndex == nPosition)
						{
							tCoordIndex[nIndex] = InverseCircularDifferenceForConnectivity(index[(3*nLoop)+nIndex],nDifCoordIndex[0]);                      
						}
						else
						{
							tCoordIndex[nIndex] = index[(3*nLoop)+nIndex];
						}
                        
					}
					//dmlab
					//tCoordIndex[(nPosition+1)%3] ^= tCoordIndex[(nPosition+2)%3] ^= tCoordIndex[(nPosition+1)%3] ^= tCoordIndex[(nPosition+2)%3];
					if(FDvalue == 1)
					{
						tChange = tCoordIndex[(nPosition+1)%3];
						tCoordIndex[(nPosition+1)%3] = tCoordIndex[(nPosition+2)%3] ;
						tCoordIndex[(nPosition+2)%3] = tChange;
					}
					
					InverseRotation(tCoordIndex,nRotation);

					for(nIndex = 0; nIndex < 3; nIndex++)
					{
						//coordIndex.push_back(tCoordIndex[nIndex]);
						index[((nLoop+1)*3)+nIndex] = tCoordIndex[nIndex];
						
                        if( tCoordIndex[nIndex] == 7525 )
                            nIndex = nIndex;
					}

					break;

				case 1:
					for(nIndex = 0; nIndex < 3; nIndex++)
					{
						nBPL = ReadBitsFromStorage(streamInfo,prefixSize);
						nDifCoordIndex[nIndex] = BPLTable[nBPL];
						if(nBPL > 2)
						{
							nDifCoordIndex[nIndex] += ReadBitsFromStorage(streamInfo,nBPL - 1);
						}
						//nDifCoordIndex[nIndex]+=1;
                        if (nDifCoordIndex[nIndex]!=0){
						    nSign = ReadBitsFromStorage(streamInfo,1);
						    if(nSign == 1)
						    {
							    nDifCoordIndex[nIndex] = -(nDifCoordIndex[nIndex]);
						    }
                        }
						//coordIndex.push_back(InverseCircularDifferenceForConnectivity(coordIndex[(3*nLoop)+nIndex],nDifCoordIndex[nIndex]));
						index[((nLoop+1)*3)+nIndex] = (InverseCircularDifferenceForConnectivity(index[(3*nLoop)+nIndex],nDifCoordIndex[nIndex]));
					}
					break;
				case 2:
					/*nPosition = ReadBitsFromStorage(streamInfo,1);
					if(nPosition == 1)
					{
						nPosition += ReadBitsFromStorage(streamInfo,1);
					}*/
					nBPL = ReadBitsFromStorage(streamInfo,prefixSize_nP);
						nPosition = BPLTable[nBPL];
					
					for(nIndex = 0; nIndex < 2; nIndex++)
					{
						nBPL = ReadBitsFromStorage(streamInfo,prefixSize);
						nDifCoordIndex[nIndex] = BPLTable[nBPL];
						if(nBPL > 2)
						{
							nDifCoordIndex[nIndex] += ReadBitsFromStorage(streamInfo,nBPL - 1);
						}
						//nDifCoordIndex[nIndex]+=1;
                        if (nDifCoordIndex[nIndex]!=0){
						    nSign = ReadBitsFromStorage(streamInfo,1);
						    if(nSign == 1)
						    {
							    nDifCoordIndex[nIndex] = -(nDifCoordIndex[nIndex]);
						    }
                        }
					}
					/*nRotation = ReadBitsFromStorage(streamInfo,1);
					if(nRotation == 1)
					{
						nRotation += ReadBitsFromStorage(streamInfo,1);
					}*/
					nBPL = ReadBitsFromStorage(streamInfo,prefixSize_nR);
						nRotation = BPLTable[nBPL];
					nCount = 0;
					tCoordIndex[nPosition] = index[(3*nLoop)+nPosition];
					for(nIndex = 1; nIndex < 3; nIndex++)
					{
						tCoordIndex[((nPosition+nIndex)%3)] = InverseCircularDifferenceForConnectivity(index[(3*nLoop)+((nPosition+nIndex)%3)],nDifCoordIndex[nCount++]);
					}

					InverseRotation(tCoordIndex,nRotation);
					
					for(nIndex = 0; nIndex < 3; nIndex++)
					{
						index[((nLoop+1)*3)+nIndex] = (tCoordIndex[nIndex]);
					}
					break;
				case 3:
					for(nIndex = 0; nIndex < 3; nIndex++)
					{
						nBPL = ReadBitsFromStorage(streamInfo,prefixSize);
						nDifCoordIndex[nIndex] = BPLTable[nBPL];
						if(nBPL > 2)
						{
							nDifCoordIndex[nIndex] += ReadBitsFromStorage(streamInfo,nBPL - 1);
						}
                        if (nDifCoordIndex[nIndex]!=0){
						    nSign = ReadBitsFromStorage(streamInfo,1);
						    if(nSign == 1)
						    {
							    nDifCoordIndex[nIndex] = -(nDifCoordIndex[nIndex]);
						    }
                        }
						index[((nLoop+1)*3)+nIndex] = (InverseCircularDifferenceForConnectivity(index[(3*nLoop)+nIndex],nDifCoordIndex[nIndex]));
					}
					break;
				case 4:
                    if(FDmode == 1){
                        //FDvalue = ReadBitsFromStorage(streamInfo,1);
						nBPL = ReadBitsFromStorage(streamInfo,prefixSize_fd);
						FDvalue = BPLTable[nBPL];
                    }
                    /*nRotation = ReadBitsFromStorage(streamInfo,1);
					if(nRotation == 1)
					{
						nRotation += ReadBitsFromStorage(streamInfo,1);
					}*/
					nBPL = ReadBitsFromStorage(streamInfo,prefixSize_nR);
						nRotation = BPLTable[nBPL];
					if(FDvalue == 1)
					{
						for(nIndex = 0; nIndex < 3; nIndex++)
						{
							tCoordIndex[nIndex] = index[(3*nLoop)+2-nIndex];	
						}
						InverseRotation(tCoordIndex,nRotation);
						for(nIndex = 0; nIndex < 3; nIndex++)
						{
							index[((nLoop+1)*3)+nIndex] = (tCoordIndex[nIndex]);
						}
					}
					else
					{
						for(nIndex = 0; nIndex < 3; nIndex++)
						{
							tCoordIndex[nIndex] = index[(3*nLoop)+nIndex];
						}
						InverseRotation(tCoordIndex,nRotation);
						for(nIndex = 0; nIndex < 3; nIndex++)
						{
							index[((nLoop+1)*3)+nIndex] = (tCoordIndex[nIndex]);
						}
					}
					break;
				}
			}

		delete [] BPLTable;
	}
	else if(binarization == AC)
	{
		unsigned int nSign;
		int nLoop;
		int difValue;
		unsigned int nFaceDirection, nPosition, nRotation;//nCoordIndex,
		int nDifCoordIndex[3];
		unsigned int nCount; //tCount,
		unsigned int nIndex;
		unsigned int tCoordIndex[3];
		unsigned int tChange;
        unsigned int length_of_ntype;
		unsigned int code_bytes = 0;
        //==================================================
        ReadFromStream(streamInfo.m_compressedStream,&length_of_ntype,4);
        unsigned char * nType = new unsigned char [nIndex_]; 
        unsigned char * nTypebuffer = new unsigned char [length_of_ntype]; 
		ReadFromStream(streamInfo.m_compressedStream, nTypebuffer, sizeof(unsigned char)*length_of_ntype);
        
        Adaptive_Data_Model mType (5);
		Arithmetic_Codec ntypeacd(length_of_ntype, nTypebuffer);
        ntypeacd.start_decoder();
 
        //Read nType into data array
        for (nLoop=0;nLoop<nIndex_;nLoop++){
            nType[nLoop] = ntypeacd.decode(mType);
        }
        //==================================================
        ReadFromStream(streamInfo.m_compressedStream, &code_bytes, sizeof(unsigned int));
		unsigned char * code_buffer = new unsigned char [code_bytes];    
		ReadFromStream(streamInfo.m_compressedStream, code_buffer, sizeof(unsigned char)*code_bytes);
        
		Adaptive_Data_Model mModel(1024);
		Adaptive_Data_Model mhasnext (2);
		Adaptive_Data_Model mSign (2);
		
		Adaptive_Data_Model mFD (2);
		Adaptive_Data_Model mPos (3);
		Adaptive_Data_Model mRotate (3);
		Arithmetic_Codec acd(code_bytes, code_buffer);
		acd.start_decoder();	// initialize decoder

		int data[4];
		data[0] = 1;
		data[1] = 1<<10;
		data[2] = 1<<20;
		data[3] = 1<<30;
		int count = 0;

		for(nLoop = 0 ; nLoop < 3; nLoop++)
		{		
			difValue = 0;
			while(1)
			{
				difValue += data[count] * acd.decode(mModel);
				count++;
				int temp = acd.decode(mhasnext);
                
				if( temp ==0)
					break;
                
			}
            int temp=acd.decode(mSign);
			count = 0;
			index[nLoop] = (difValue);       
		}
		for(nLoop = 0 ; nLoop < (nIndex_ - 1); nLoop++)
		{
			switch(nType[nLoop])
			{
			case 0:
				nPosition = acd.decode(mPos);
				nFaceDirection =acd.decode(mFD);
				
				difValue = 0;
				while(1)
				{
					difValue += data[count] * acd.decode(mModel);
					count++;
					int temp = acd.decode(mhasnext);

					if( temp ==0)
						break;
				}
				count = 0;
                if (difValue!=0){				    nSign = acd.decode(mSign);
				    if(nSign == 1)
				    {
					    difValue = -(difValue);
				    }
                }
				
				nRotation = acd.decode(mRotate);
				nDifCoordIndex[0] = difValue;
			
				for(nIndex = 0; nIndex < 3; nIndex++)
				{
					if(nIndex == nPosition)
					{
						tCoordIndex[nIndex] = InverseCircularDifferenceForConnectivity(index[(3*nLoop)+nIndex],nDifCoordIndex[0]);                   
					}
					else
					{
						tCoordIndex[nIndex] = index[(3*nLoop)+nIndex];
					}
				}
				//dmlab
				//tCoordIndex[(nPosition+1)%3] ^= tCoordIndex[(nPosition+2)%3] ^= tCoordIndex[(nPosition+1)%3] ^= tCoordIndex[(nPosition+2)%3];
				if(nFaceDirection == 1)
				{
					tChange = tCoordIndex[(nPosition+1)%3];
					tCoordIndex[(nPosition+1)%3] = tCoordIndex[(nPosition+2)%3] ;
					tCoordIndex[(nPosition+2)%3] = tChange;
				}
				InverseRotation(tCoordIndex,nRotation);
				for(nIndex = 0; nIndex < 3; nIndex++)
				{
					//coordIndex.push_back(tCoordIndex[nIndex]);
					index[((nLoop+1)*3)+nIndex] = tCoordIndex[nIndex];
				}
				break;

			case 1:
				for(nIndex = 0; nIndex < 3; nIndex++)
				{
					difValue = 0;
					while(1)
					{
						difValue += data[count] * acd.decode(mModel);
						count++;
						int temp = acd.decode(mhasnext);

						if( temp ==0)
							break;
					}
					count = 0;
                    if (difValue!=0){
					    nSign = acd.decode(mSign);
					    if(nSign == 1)
					    {
						    difValue = -(difValue);
					    }
                    }
					nDifCoordIndex[nIndex] = difValue;
			
					//coordIndex.push_back(InverseCircularDifferenceForConnectivity(coordIndex[(3*nLoop)+nIndex],nDifCoordIndex[nIndex]));
					index[((nLoop+1)*3)+nIndex] = (InverseCircularDifferenceForConnectivity(index[(3*nLoop)+nIndex],nDifCoordIndex[nIndex]));
				}
				break;
			case 2:
				nPosition = acd.decode(mPos);
				for(nIndex = 0; nIndex < 2; nIndex++)
				{
					difValue = 0;
					while(1)
					{
						difValue += data[count] * acd.decode(mModel);
						count++;
						int temp = acd.decode(mhasnext);

						if( temp ==0)
							break;
					}
					count = 0;
                    if (difValue!=0){
					    nSign = acd.decode(mSign);
					    if(nSign == 1)
					    {
						    difValue = -(difValue);
					    }
                    }
					nDifCoordIndex[nIndex] = difValue;					
				}
				nRotation = acd.decode(mRotate);
				nCount = 0;
				tCoordIndex[nPosition] = index[(3*nLoop)+nPosition];
				for(nIndex = 1; nIndex < 3; nIndex++)
				{
					tCoordIndex[((nPosition+nIndex)%3)] = InverseCircularDifferenceForConnectivity(index[(3*nLoop)+((nPosition+nIndex)%3)],nDifCoordIndex[nCount++]);
				}
				nCount = 0;
				InverseRotation(tCoordIndex,nRotation);
				
				for(nIndex = 0; nIndex < 3; nIndex++)
				{
					index[((nLoop+1)*3)+nIndex] = (tCoordIndex[nIndex]);
				}
				break;
			case 3:
				for(nIndex = 0; nIndex < 3; nIndex++)
				{
					difValue = 0;
					while(1)
					{
						difValue += data[count] * acd.decode(mModel);
						count++;
						int temp = acd.decode(mhasnext);

						if( temp ==0)
							break;
					}
					count = 0;
                    if (difValue!=0){
					    nSign = acd.decode(mSign);
					    if(nSign == 1)
					    {
						    difValue = -(difValue);
					    }
                    }
					nDifCoordIndex[nIndex] = difValue;

					index[((nLoop+1)*3)+nIndex] = (InverseCircularDifferenceForConnectivity(index[(3*nLoop)+nIndex],nDifCoordIndex[nIndex]));
				}
				break;
			case 4:
				nFaceDirection =  acd.decode(mFD);
				nRotation =  acd.decode(mRotate);
				if(nFaceDirection == 1)
				{
					for(nIndex = 0; nIndex < 3; nIndex++)
					{
						tCoordIndex[nIndex] = index[(3*nLoop)+2-nIndex];	
					}
					InverseRotation(tCoordIndex,nRotation);
					for(nIndex = 0; nIndex < 3; nIndex++)
					{
						index[((nLoop+1)*3)+nIndex] = (tCoordIndex[nIndex]);
					}
				}
				else
				{
					for(nIndex = 0; nIndex < 3; nIndex++)
					{
						tCoordIndex[nIndex] = index[(3*nLoop)+nIndex];
					}
					InverseRotation(tCoordIndex,nRotation);
					for(nIndex = 0; nIndex < 3; nIndex++)
					{
						index[((nLoop+1)*3)+nIndex] = (tCoordIndex[nIndex]);
					}
				}
				break;

			}
		}
   	}
	else
	{
		//printf("long parameter");
//		assert(false);
		exit(1);		
	}
}

unsigned int SVAIndexDecoder::InverseCircularDifferenceForConnectivity(unsigned int previous, int differential)
{
	int result = (int)previous+differential;
    
	if(result >= (int)nData)
	{
		result = result - nData;
	}
	else if(result < 0 )
	{
		result = nData + result;
	}
	return result;

}

void SVAIndexDecoder::InverseRotation(unsigned int *data, int size)
{
	int tData[3];
	int i;
	for(i=0; i<3; i++)
	{
		tData[i] = data[i];
	}
	for(i=0; i<3; i++)
	{
		data[i]= tData[(i-size+3)%3];
	}

}
