//-------------------------------------------------------------------------
// 
// Copyright ETRI, Hanyang University 2008, 2009
// Licensed Materials, Program Property of ETRI & Hanyang University
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.
// ETRI and Hanyang University retain full right to use the code 
// for his/her own purpose, assign or donate the code to a third party and
// to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Seung Wook Lee (ETRI)
//    Daiyong Kim (Hanyang University)
//    KyoungSoo Son (Hanyang University)
//
//    http://www.etri.re.kr/
//    http://dmlab.hanyang.ac.kr/
//  
//    Seung Wook Lee     (tajinet@etri.re.kr)  
//    Daiyong Kim        (dykim@dmlab.hanyang.ac.kr)    
//    KyoungSoo Son      (bonokensin@dmlab.hanyang.ac.kr)
//
//-------------------------------------------------------------------------
#pragma warning (disable : 4786)

#include "SVA_IndexEncoder.h"
#include "Arithmetic_Codec.h"
#include <cstdlib>


#define LEN_BIT		1

SVAIndexEncoder::SVAIndexEncoder()
{
	MaxDifferentialForConnectivity = 0;
}

SVAIndexEncoder::~SVAIndexEncoder()
{
}

void SVAIndexEncoder::Encode(EncoderStreamInfo &streamInfo, int *pIndex, int numberOfIndex, int numberOfData, int binarization)
{
	index = pIndex;
	nIndex = numberOfIndex;
	nData = numberOfData;

	int predictionMode = 4;
	unsigned char mask = predictionMode & 7;
	int binarizationMode =binarization;
	if(binarizationMode != AC)
		binarizationMode = BP;
	mask += (binarizationMode & 7)<<4;
	Write2Stream(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize, &mask, sizeof(unsigned char));

	ConnectivityAnalyze();
	
	Encode_Connectivity(streamInfo,binarizationMode);
   
	Flush(streamInfo);
}

void SVAIndexEncoder::Encode_Connectivity(EncoderStreamInfo &streamInfo, int binarization)
{
	if(binarization == BP)
	{
		int maxValue = MaxDifferentialForConnectivity+1;	 
		int * BPLTable = new int [maxValue];
		int * payloadTable = new int [maxValue];
		int i = 0;
		for(i = 0 ; i < 3; i++)
		{
			if(i == maxValue)
				break;
			BPLTable[i] = i;
			payloadTable[i] = -1;
		}
		
		int payloadLength = 4;
		int minValueInPresentBPL = 3;
		int presentBPL = 3;
		for( ; i<maxValue; i++)
		{
			if( i < 3)
			{
				BPLTable[i] = i;
				payloadTable[i] = -1;
			}
			if(i == payloadLength+minValueInPresentBPL)
			{
				minValueInPresentBPL += payloadLength;
				payloadLength *= 2;
				presentBPL++;
			}
			BPLTable[i] = presentBPL;
			payloadTable[i] = i - minValueInPresentBPL;
		}
		
		
		unsigned int nMaxCheckBPL = int(log((float)MaxDifferentialForConnectivity-1) / log((float)2)) +1; 
		unsigned int prefix_size = int(log((float)(4)) / log((float)2)) +1;	
		unsigned int prefix_size_nP = int(log((float)(2)) / log((float)2)) +1;	
		unsigned int prefix_size_fd = int(log((float)(2)) / log((float)2)) +1;	
		unsigned int prefix_size_nR = int(log((float)(2)) / log((float)2)) +1;	
		int nBPL, nPayload, nSign;
		unsigned int countForType =0;
		unsigned int countForFD =0;  // Not to support RunLength Coding
		unsigned int countForDifCI =0;
		unsigned int countForPos =0;
		unsigned int countForRotate =0;
		int nType, nPos, nFD, nDifCI, nRotate;
        //==================================================
        unsigned int encoded_length,a_encoded_length;
        unsigned int beforelength;
        
        InsertData(streamInfo,FDmode, LEN_BIT);//FDMODE
        if(FDmode == 0)
			InsertData(streamInfo,FDvalue, LEN_BIT);
        Flush(streamInfo);

        InsertData(streamInfo,0x0, 32);//nType header
        beforelength=streamInfo.m_compressedStreamSize;
        
        InsertData(streamInfo,prefix_size, LEN_PREFIX);
		Flush(streamInfo); 
        for( ; countForType < type.size(); countForType++)
		{
            nType = type[countForType];
            nBPL= BPLTable[nType];
			//nPayload = payloadTable[difindex[nType]];

			InsertData(streamInfo,nBPL,prefix_size);
			//InsertData(streamInfo,1, nType+1);*/
        }
        encoded_length=streamInfo.m_compressedStreamSize-beforelength;
        a_encoded_length=encoded_length;
        if (streamInfo.m_bitPostion != 0){
            a_encoded_length++;
        }
        memcpy( streamInfo.m_compressedStream - (encoded_length+4), &a_encoded_length, 4 );
        Flush(streamInfo);
        //nType size
        //==================================================
        
        InsertData(streamInfo,0x0, 32);
        beforelength=streamInfo.m_compressedStreamSize;
        //bitstream header
        //==================================================
		//InsertData(streamInfo,FDmode, LEN_BIT);

		prefix_size = int(log((float)(nMaxCheckBPL+1)) / log((float)2)) +1;
		InsertData(streamInfo,prefix_size, LEN_PREFIX);
		InsertData(streamInfo,prefix_size_nP, LEN_PREFIX);
		InsertData(streamInfo,prefix_size_fd, LEN_PREFIX);
		InsertData(streamInfo,prefix_size_nR, LEN_PREFIX);
		Flush(streamInfo);		
		for( ;countForDifCI<3;countForDifCI++)
		{
			nBPL= BPLTable[difindex[countForDifCI]];
			nPayload = payloadTable[difindex[countForDifCI]];
			InsertData(streamInfo,nBPL,prefix_size);
			if(nPayload != -1)
				InsertData(streamInfo,nPayload, (nBPL-1));
		}
        countForType =0;
		for( ; countForType < type.size(); countForType++)
		{
			nType = type[countForType];
			if (countForType == type.size() - 1)
				nType = nType;
			//InsertData(streamInfo,1, nType+1);
			switch(nType)
			{
			case 0:
               
				nPos= position[countForPos++];
				//nFD = faceDirection[countForFD++];
				
				nDifCI = difindex[countForDifCI++];
				/*if(nPos == 0 )
					InsertData(streamInfo,nPos, 1);
				else
					InsertData(streamInfo,(nPos+1), 2);*/

				nBPL= BPLTable[nPos];				
				InsertData(streamInfo,nBPL,prefix_size_nP);

				if(FDmode == 1){
                    nFD = faceDirection[countForFD++];
					nBPL= BPLTable[nFD];				
					InsertData(streamInfo,nBPL,prefix_size_fd);
                }
				if(nDifCI > 0)
					nSign = 0;
				else
				{
					nDifCI = -nDifCI;
					nSign = 1;
				}
				//nBPL= BPLTable[nDifCI-1];
				//nPayload = payloadTable[nDifCI-1];
                nBPL= BPLTable[nDifCI];
				nPayload = payloadTable[nDifCI];
				InsertData(streamInfo,nBPL,prefix_size);
				if(nPayload != -1)
					InsertData(streamInfo,nPayload, (nBPL-1));
                if (nDifCI!=0)
				  {
					  InsertData(streamInfo,nSign,1);					  
				}
				nRotate = rotate[countForRotate++];
	
				nBPL= BPLTable[nRotate];				
				InsertData(streamInfo,nBPL,prefix_size_nR);
				/*	InsertData(streamInfo,nRotate, 1);
				else
					InsertData(streamInfo,(nRotate+1), 2);*/

				break;
			case 1:
				for(i=0; i < 3; i++)
				{
					nDifCI = difindex[countForDifCI++];
					if(nDifCI > 0)
						nSign = 0;
					else
					{
						nDifCI = -nDifCI;
						nSign = 1;
					}
					//nBPL= BPLTable[nDifCI-1];
					//nPayload = payloadTable[nDifCI-1];
                    nBPL= BPLTable[nDifCI];
					nPayload = payloadTable[nDifCI];
					InsertData(streamInfo,nBPL,prefix_size);
					if(nPayload != -1)
						InsertData(streamInfo,nPayload, (nBPL-1));
                    if (nDifCI!=0)
					{
						InsertData(streamInfo,nSign,1);						
					}
				}
				break;
			case 2:
				nPos= position[countForPos++];
				//nRotate = rotate[countForRotate++];
				
				nBPL= BPLTable[nPos];				
				InsertData(streamInfo,nBPL,prefix_size_nP);
					/*InsertData(streamInfo,nPos, 1);
				else
					InsertData(streamInfo,(nPos+1), 2);*/
				//nsert_Data(nRotate, 2);
				for(i=0; i < 2; i++)
				{
					nDifCI = difindex[countForDifCI++];
					if(nDifCI > 0)
						nSign = 0;
					else
					{
						nDifCI = -nDifCI;
						nSign = 1;
					}
					//nBPL= BPLTable[nDifCI-1];
					//nPayload = payloadTable[nDifCI-1];
                    nBPL= BPLTable[nDifCI];
					nPayload = payloadTable[nDifCI];
					InsertData(streamInfo,nBPL,prefix_size);
					if(nPayload != -1)
						InsertData(streamInfo,nPayload, (nBPL-1));
                    if (nDifCI!=0)
					  {
						  InsertData(streamInfo,nSign,1);
					}
				}
				nRotate = rotate[countForRotate++];
	
				nBPL= BPLTable[nRotate];				
				InsertData(streamInfo,nBPL,prefix_size_nR);
				/*	InsertData(streamInfo,nRotate, 1);
				else
					InsertData(streamInfo,(nRotate+1), 2);*/
				break;
			case 3:
				for(i=0; i < 3; i++)
				{
					nDifCI = difindex[countForDifCI++];
					if(nDifCI > 0)
						nSign = 0;
					else
					{
						nDifCI = -nDifCI;
						nSign = 1;
					}
					nBPL= BPLTable[nDifCI];
					nPayload = payloadTable[nDifCI];
					InsertData(streamInfo,nBPL,prefix_size);
					if(nPayload != -1)
						InsertData(streamInfo,nPayload, (nBPL-1));
                    if (nDifCI!=0)
					  {
						  InsertData(streamInfo,nSign,1);
					}
				}
				break;
			case 4:
				//nFD = faceDirection[countForFD++];
				if(FDmode == 1){
                    nFD = faceDirection[countForFD++];
					nBPL= BPLTable[nFD];				
					InsertData(streamInfo,nBPL,prefix_size_fd);
					//InsertData(streamInfo,nFD,1);
                }
				nRotate = rotate[countForRotate++];

					nBPL= BPLTable[nRotate];				
					InsertData(streamInfo,nBPL,prefix_size_nR);
					/*InsertData(streamInfo,nRotate, 1);
				else
					InsertData(streamInfo,(nRotate+1), 2);*/
				
				break;
			}
			
		}
        //=========================
        //bitstream size
        encoded_length=streamInfo.m_compressedStreamSize-beforelength;
        a_encoded_length=encoded_length;
        if(streamInfo.m_bitPostion != 0)
            a_encoded_length++;
        memcpy( streamInfo.m_compressedStream - (encoded_length+4), &a_encoded_length, 4 );
        //=========================
        delete [] BPLTable;
		delete [] payloadTable;
	}
	else if(binarization == AC)
	{
		
		unsigned int i;
        unsigned int encoded_ntype_length=0;
        unsigned int mask = 0x80000000;
		unsigned int countForType =0;
		unsigned int countForFD =0;  // Not to support RunLength Coding
		unsigned int countForDifCI =0;
		unsigned int countForPos =0;
		unsigned int countForRotate =0;
		int nType, nPos, nFD, nDifCI, nRotate;

		unsigned int attr_num;
		int previousQuantValue[3];
		for(attr_num = 0; attr_num < 3; attr_num++)
		{
			previousQuantValue[attr_num] = 0;
		}

		int nSymbol, nSign; //, input;
		int lengthCoordIndex = int(log((double)nData-1)/log(2.0)) + 1;
		unsigned int NMAX = (int) nIndex*3 * lengthCoordIndex + 100;

		Adaptive_Data_Model mType (5);

        Arithmetic_Codec ntypeace(NMAX, NULL);
        ntypeace.start_encoder();

        //================================================
        //nType
        for( ; countForType < type.size(); countForType++)
		{
			nType = type[countForType];
			ntypeace.encode(nType,mType);
        }
        ntypeace.write_to_mem(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize);
        //================================================

        Adaptive_Data_Model mModel(1024);
		Adaptive_Data_Model mhasnext (2);
		Adaptive_Data_Model mSign (2);
		Adaptive_Data_Model mFD (2);
		Adaptive_Data_Model mPos (3);
		Adaptive_Data_Model mRotate (3);

        Arithmetic_Codec ace(NMAX, NULL);
		ace.start_encoder();
		for( ;countForDifCI<3;countForDifCI++)
		{
			nSymbol=difindex[countForDifCI];
			while(1)
			{
				ace.encode(nSymbol%1024, mModel);
				nSymbol/=1024;
				if(nSymbol)
					ace.encode(1, mhasnext);
				else
					break;
			}
			ace.encode(0, mhasnext);
            ace.encode(0, mSign);
		}
        countForType=0;
        //int tempnSymbol;
		for( ; countForType < type.size(); countForType++)
		{
			nType = type[countForType];
			//ace.encode(nType,mType);			
			switch(nType)
			{
			case 0:
				nPos= position[countForPos++];
				nFD = faceDirection[countForFD++];
				
				nDifCI = difindex[countForDifCI++];
				ace.encode(nPos, mPos);
				ace.encode(nFD, mFD);
				
				if(nDifCI > 0)
					nSign = 0;
				else
				{
					nDifCI = -nDifCI;
					nSign = 1;
				}
				nSymbol = nDifCI;
				while(1)
				{
					ace.encode(nSymbol%1024, mModel);
					nSymbol/=1024;
					if(nSymbol)
						ace.encode(1, mhasnext);
					else
						break;
				}
				ace.encode(0, mhasnext);
                if (nDifCI!=0)
				    ace.encode(nSign, mSign);

				nRotate = rotate[countForRotate++];
				ace.encode(nRotate, mRotate);

				break;
			case 1:
				for(i=0; i < 3; i++)
				{
					nDifCI = difindex[countForDifCI++];
					if(nDifCI > 0)
						nSign = 0;
					else
					{
						nDifCI = -nDifCI;
						nSign = 1;
					}
					nSymbol = nDifCI;
					while(1)
					{
						ace.encode(nSymbol%1024, mModel);
						nSymbol/=1024;
						if(nSymbol)
							ace.encode(1, mhasnext);
						else
							break;
					}
					ace.encode(0, mhasnext);
                    if (nDifCI!=0)
					    ace.encode(nSign, mSign);
				}
				break;
			case 2:
				nPos= position[countForPos++];
				ace.encode(nPos, mPos);
				for(i=0; i < 2; i++)
				{
					nDifCI = difindex[countForDifCI++];
					if(nDifCI > 0)
						nSign = 0;
					else
					{
						nDifCI = -nDifCI;
						nSign = 1;
					}
					nSymbol = nDifCI;
					while(1)
					{
						ace.encode(nSymbol%1024, mModel);
						nSymbol/=1024;
						if(nSymbol)
							ace.encode(1, mhasnext);
						else
							break;
					}
					ace.encode(0, mhasnext);
                    if (nDifCI!=0)
					    ace.encode(nSign, mSign);
				}
				nRotate = rotate[countForRotate++];
				ace.encode(nRotate, mRotate);
				break;
			case 3:
				for(i=0; i < 3; i++)
				{
					nDifCI = difindex[countForDifCI++];
					if(nDifCI > 0)
						nSign = 0;
					else
					{
						nDifCI = -nDifCI;
						nSign = 1;
					}
					nSymbol = nDifCI;
					while(1)
					{
						ace.encode(nSymbol%1024, mModel);
						nSymbol/=1024;
						if(nSymbol)
							ace.encode(1, mhasnext);
						else
							break;
					}
					ace.encode(0, mhasnext);
                    if (nDifCI!=0)
					    ace.encode(nSign, mSign);
				}
				break;
			case 4:
				nFD = faceDirection[countForFD++];
				ace.encode(nFD, mFD);
				nRotate = rotate[countForRotate++];
				ace.encode(nRotate, mRotate);
				break;
			}
		}
		ace.write_to_mem(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize);

	}


}

void SVAIndexEncoder::ConnectivityAnalyze()
{

	int symbol;
	int flip=0;
	int k, l;
	//unsigned int j;
	int DifferetialCI[2];
	int rotcount=0;
	int nRot;


	difindex.push_back(index[0]); 
	if(abs(index[0]) > MaxDifferentialForConnectivity)
	{
		MaxDifferentialForConnectivity = index[0];
	}
	difindex.push_back(index[1]);
	if(abs(index[1]) > MaxDifferentialForConnectivity)
	{
		MaxDifferentialForConnectivity = index[1];
	}
	difindex.push_back(index[2]); 
	if(abs(index[2]) > MaxDifferentialForConnectivity)
	{
		MaxDifferentialForConnectivity = index[2];
	}

	//From second face
	for(int j = 1; j < nIndex; j++)
	{

		//Count the overlapped points
		int sizeofOverlapped=0;
		int myType=0;
		int cheecktype4 =0;

		if(cheecktype4 == 1)
		{
			cheecktype4 =0;
			sizeofOverlapped = 4;

		}
		else 
		{
			for(k=0;k<3;k++)
			{
				for(l=k+1;l<3;l++)
				{
					if(index[j*3+k] == index[j*3+l])
					{
//						assert(cheecktype4 == 1);
						sizeofOverlapped = 4;
						break;
					}
				}
			}
			if(sizeofOverlapped != 4)
			{
				for(k=0;k<3;k++)
				{
					for(l=0;l<3;l++)
					{
						if(index[j*3+k] == index[(j-1)*3+l])
						{
							sizeofOverlapped++;
							break;
						}
					}
				}
			}
		}

		// Changing type information based on its frequency.
		// Type 2 ==> Type 0
		// Type 0 ==> Type 1
		// Type 1 ==> Type 2
		// Type 3 ==> Type 3

		

		
		switch(sizeofOverlapped)
		{
		case 0: //Type C encode 3 symbols
			myType = 1;
			
			symbol = CircularDifferenceForConnectivity(index[(j-1)*3], index[j*3]);
			difindex.push_back(symbol);
			if(abs(symbol) > MaxDifferentialForConnectivity)
			{
				MaxDifferentialForConnectivity = abs(symbol);
			}

			symbol = CircularDifferenceForConnectivity(index[(j-1)*3+1], index[j*3+1]);
			difindex.push_back(symbol);
			if(abs(symbol) > MaxDifferentialForConnectivity)
			{
				MaxDifferentialForConnectivity = abs(symbol);
			}
			
			symbol = CircularDifferenceForConnectivity(index[(j-1)*3+2], index[j*3+2]);
			difindex.push_back(symbol);
			if(abs(symbol) > MaxDifferentialForConnectivity)
			{
				MaxDifferentialForConnectivity = abs(symbol);
			}
			break;
		case 1: //Type B encode 2 symbols 
			myType = 2;

			for(k=0;k<3;k++)
			{
				for(l=0;l<3;l++)
				{
					if(index[j*3+k] == index[(j-1)*3+l])						
					{
						//Rotation
						nRot = Rotation((index+j*3), k-l);
						rotate.push_back(nRot);
						
						


						//encode point that indicates same symbol						;
						position.push_back(l);

						//encode 2 symbols						
						symbol = CircularDifferenceForConnectivity(index[(j-1)*3+(l+1)%3], index[j*3+(l+1)%3]);
						difindex.push_back(symbol);
						if(abs(symbol) > MaxDifferentialForConnectivity)
						{
							MaxDifferentialForConnectivity = abs(symbol);
						}
						symbol = CircularDifferenceForConnectivity(index[(j-1)*3+(l+2)%3], index[j*3+(l+2)%3]);
						difindex.push_back(symbol);
						if(abs(symbol) > MaxDifferentialForConnectivity)
						{
							MaxDifferentialForConnectivity = abs(symbol);
						}

						k=3;
						InverseRotation((index+j*3), nRot);
						break;
					}
				}
			}

			break;



		case 2: //Type A encode 1 symbol
			//DifferetialCI[0] indicates different symbol's position in the current face
			myType = 0;
			for(k=0;k<3;k++)
			{
				for(l=0;l<3;l++)
					if(index[j*3+k] == index[(j-1)*3+l])
						break;
				if(l==3)
					DifferetialCI[0]=k;
			}
			//DifferetialCI[1] indicates different symbol's position in the current face
			for(k=0;k<3;k++)
			{
				for(l=0;l<3;l++)
					if(index[j*3+l] == index[(j-1)*3+k])
						break;
				if(l==3)
					DifferetialCI[1]=k;
			}
			//rotate to match DifferetialCI[0] and DifferetialCI[1]
			nRot = Rotation((index+j*3), DifferetialCI[0]-DifferetialCI[1]);
			rotate.push_back(nRot);
		

			flip=1;
			
			for(k=0; k <3; k++)
			{
				if(index[j*3+k] == index[(j-1)*3+k])
				{
					flip=0;
					break;
				}
			}
			//coding flip flag
			faceDirection.push_back(flip);

			//encode pos that indicates different symbol
			position.push_back(DifferetialCI[1]);


			//encode 1 symbol
			symbol = CircularDifferenceForConnectivity(index[(j-1)*3+DifferetialCI[1]],index[j*3+DifferetialCI[1]]);
			difindex.push_back(symbol);
			if(abs(symbol) > MaxDifferentialForConnectivity)
			{
				MaxDifferentialForConnectivity = abs(symbol);
			}
			InverseRotation((index+j*3), nRot);
			
			break;

		
		case 3: //Type D encode only flip bit
			myType = 4;
			rotcount=0;
			while(index[j*3+1] != index[(j-1)*3+1])
			{
				Rotation((index+j*3), 1);
				rotcount++;
			}
			rotate.push_back(rotcount);
			
			

			if(	index[j*3] == index[(j-1)*3] )
				flip=0;
			else
				flip=1;
			faceDirection.push_back(flip);
			InverseRotation((index+j*3), rotcount);
			//coding flip flag			
			

			break;
		case 4:
			myType = 3;
			symbol = CircularDifferenceForConnectivity(index[(j-1)*3], index[j*3]);
			difindex.push_back(symbol);
			if(abs(symbol) > MaxDifferentialForConnectivity)
			{
				MaxDifferentialForConnectivity = abs(symbol);
			}

			symbol = CircularDifferenceForConnectivity(index[(j-1)*3+1], index[j*3+1]);
			difindex.push_back(symbol);
			if(abs(symbol) > MaxDifferentialForConnectivity)
			{
				MaxDifferentialForConnectivity = abs(symbol);
			}
			
			symbol = CircularDifferenceForConnectivity(index[(j-1)*3+2], index[j*3+2]);
			difindex.push_back(symbol);
			if(abs(symbol) > MaxDifferentialForConnectivity)
			{
				MaxDifferentialForConnectivity = abs(symbol);
			}

			break;

		}
		type.push_back(myType); //type information

	}

	//change
	if(faceDirection.size() == 0 )
	{
		FDmode = 1;
	}
	else
	{
		FDvalue = faceDirection[0];
		FDmode = 0;
		for(unsigned int j = 1 ; j< faceDirection.size(); j++)
		{
			if(FDvalue != faceDirection[j])
			{
				FDmode = 1;
				break;
			}
		}
	}
}


int SVAIndexEncoder::CircularDifferenceForConnectivity(int prev, int cur)
{
	int data;
	if(cur > prev)
	{
		data = (prev - 0 )+(nData - cur);

	}
	else
	{
		data = (cur - 0 )+(nData - prev);
	}

	if(abs(cur - prev) <= data)
		return cur-prev;
	else if(cur < prev)
	{
		return (data);
	}
	else
	{
		return (-data);
	}
}


int SVAIndexEncoder::Rotation(int* data, int move)
{
	move=move%3;
	if(move<0)
		move+=3;
	int i;
	int tempStorage[5];
	for(i = 0 ; i < 5 ; i++)
	{
		tempStorage [i] = data[(i%3)];
	}
	for(i = 0 ; i < 3 ; i++)
	{
		data [i] = tempStorage[i+move];
	}

	return move;

}

void SVAIndexEncoder::InverseRotation(int *data, int size)
{
	int tData[3];
	int i;
	for(i=0; i<3; i++)
	{
		tData[i] = data[i];
	}
	for(i=0; i<3; i++)
	{
		data[(i+size)%3]= tData[i];
	}

}
