//-------------------------------------------------------------------------
// 
// Copyright ETRI, Hanyang University 2008, 2009
// Licensed Materials, Program Property of ETRI & Hanyang University
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.
// ETRI and Hanyang University retain full right to use the code 
// for his/her own purpose, assign or donate the code to a third party and
// to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Seung Wook Lee (ETRI)
//    Daiyong Kim (Hanyang University)
//    KyoungSoo Son (Hanyang University)
//
//    http://www.etri.re.kr/
//    http://dmlab.hanyang.ac.kr/
//  
//    Seung Wook Lee     (tajinet@etri.re.kr)  
//    Daiyong Kim        (dykim@dmlab.hanyang.ac.kr)    
//    KyoungSoo Son      (bonokensin@dmlab.hanyang.ac.kr)
//
//-------------------------------------------------------------------------

#pragma warning (disable : 4786)

#include "SVA_Encoder.h"
#include "SVA_IndexEncoder.h"

void SVAEncoder::EncodeDataBuffer(unsigned char * compressedStream, unsigned int & compressedStreamSize)
{
	m_streamInfo.m_compressedStream = compressedStream; 
	m_streamInfo.m_compressedStreamSize = compressedStreamSize;
	
	// encoding the coordIndex
	if(m_ifs->GetCoordIndex() > 0)
	{
		SVAIndexEncoder coordIndex;
		coordIndex.Encode(m_streamInfo,m_ifs->GetCoordIndex(),m_ifs->GetNCoordIndex() , m_ifs->GetNCoord(), m_params->m_binarizationMode);

	}
	// encoding the normalIndex
	if (m_ifs->GetNNormalIndex() > 0)
	{
		SVAIndexEncoder normalIndex;
		normalIndex.Encode(m_streamInfo,m_ifs->GetNormalIndex(),m_ifs->GetNNormalIndex() , m_ifs->GetNNormal(), m_params->m_binarizationMode);
	}

	// encoding the colorIndex
	if (m_ifs->GetNColorIndex() > 0)
	{
		SVAIndexEncoder colorIndex;
		colorIndex.Encode(m_streamInfo,m_ifs->GetColorIndex(),m_ifs->GetNColorIndex() , m_ifs->GetNColor(), m_params->m_binarizationMode);
	}
	
	// encoding the texCoordIndex
	if (m_ifs->GetNTexCoordIndex() > 0)
	{
		SVAIndexEncoder texCoordIndex;
		texCoordIndex.Encode(m_streamInfo,m_ifs->GetTexCoordIndex(),m_ifs->GetNTexCoordIndex() , m_ifs->GetNTexCoord(), m_params->m_binarizationMode);
	}

	int m_QuantizationParameter[3] = {0,0,0};
	// encoding the coord
	
	if (m_ifs->GetNCoord() > 0)
	{
		m_QuantizationParameter[0] = m_params->m_nQCoordBits;
			
		EncodeFloatArray(m_streamInfo, m_ifs->GetCoord(), m_ifs->GetNCoord(), 3, m_params->m_coordPredMode, 
					 m_params->m_binarizationMode, UniformQuantization, m_QuantizationParameter,
					 static_cast<float *> (m_ifs->minCoord), static_cast<float *> (m_ifs->rangeCoord), 
					 m_ifs->GetNCoordIndex(), m_ifs->GetCoordIndex(), NULL, NULL);
	}

	// encoding the normal
	if (m_ifs->GetNNormal() > 0)
	{
		m_QuantizationParameter[0] = m_params->m_nQNormalBits;

		if (m_ifs->GetNormalPerVertex())
		{
			
			if (m_ifs->GetNNormalIndex()>0)
			{
				EncodeFloatArray(m_streamInfo, m_ifs->GetNormal(), m_ifs->GetNNormal(), 3, m_params->m_normalPredMode, 
					 m_params->m_binarizationMode, UniformQuantization, m_QuantizationParameter, 
					 static_cast<float *> (m_ifs->minNormal), static_cast<float *> (m_ifs->rangeNormal),
					 m_ifs->GetNNormalIndex(), m_ifs->GetNormalIndex(), NULL, NULL);
			}
			else
			{
				EncodeFloatArray(m_streamInfo, m_ifs->GetNormal(), m_ifs->GetNNormal(), 3, m_params->m_normalPredMode, 
					 m_params->m_binarizationMode, UniformQuantization, m_QuantizationParameter,  
					 static_cast<float *> (m_ifs->minNormal), static_cast<float *> (m_ifs->rangeNormal),
					 m_ifs->GetNCoordIndex(), m_ifs->GetCoordIndex(), NULL, NULL);
			}
		}
		else
		{
			EncodeFloatArray(m_streamInfo, m_ifs->GetNormal(), m_ifs->GetNNormal(), 1, 1, 
					 m_params->m_binarizationMode, UniformQuantization, m_QuantizationParameter, 
					 static_cast<float *> (m_ifs->minNormal), static_cast<float *> (m_ifs->rangeNormal));
		}	
	}


	// encoding the color
	if (m_ifs->GetNColor() > 0)
	{
		m_QuantizationParameter[0] = m_params->m_nQColorBits;
		if (m_ifs->GetColorPerVertex())
		{
			
			if (m_ifs->GetNColorIndex()>0)
			{
				EncodeFloatArray(m_streamInfo, m_ifs->GetColor(), m_ifs->GetNColor(), 3, m_params->m_colorPredMode, 
						 m_params->m_binarizationMode, UniformQuantization, m_QuantizationParameter,
						 static_cast<float *> (m_ifs->minColor), static_cast<float *> (m_ifs->rangeColor),
						 m_ifs->GetNColorIndex(), m_ifs->GetColorIndex(), NULL, NULL);
			}
			else
			{
				EncodeFloatArray(m_streamInfo, m_ifs->GetColor(), m_ifs->GetNColor(), 3, m_params->m_colorPredMode, 
						 m_params->m_binarizationMode, UniformQuantization, m_QuantizationParameter,
						 static_cast<float *> (m_ifs->minColor), static_cast<float *> (m_ifs->rangeColor),
						 m_ifs->GetNCoordIndex(), m_ifs->GetCoordIndex(), NULL, NULL);
			}
		}
		else
		{
			EncodeFloatArray(m_streamInfo, m_ifs->GetColor(), m_ifs->GetNColor(), 3, 1, 
						 m_params->m_binarizationMode, UniformQuantization, m_QuantizationParameter, 
						 static_cast<float *> (m_ifs->minColor), static_cast<float *> (m_ifs->rangeColor));
		}	
	}
	
	// encoding the texCoord
	if (m_ifs->GetNTexCoord() > 0)
	{
		if(m_params->m_nQTexCoordWidth > m_params->m_nQTexCoordHeight)
			m_QuantizationParameter[0] = int(log((float)(m_params->m_nQTexCoordWidth)) /log(2.0))+1;
		else
			m_QuantizationParameter[0] = int(log((float)(m_params->m_nQTexCoordHeight)) / log(2.0))+1;
		m_QuantizationParameter[1] = m_params->m_nQTexCoordWidth;
		m_QuantizationParameter[2] = m_params->m_nQTexCoordHeight;
		
		if (m_ifs->GetNTexCoordIndex()>0)
		{
			EncodeFloatArray(m_streamInfo, m_ifs->GetTexCoord(), m_ifs->GetNTexCoord(), 2, m_params->m_texCoordPredMode, 
				 m_params->m_binarizationMode,  UniformTextureQuantization, m_QuantizationParameter,
				 static_cast<float *> (m_ifs->minTexCoord), static_cast<float *> (m_ifs->rangeTexCoord),
				m_ifs->GetNTexCoordIndex(), m_ifs->GetTexCoordIndex(), NULL, NULL);
		}
		else
		{
			EncodeFloatArray(m_streamInfo, m_ifs->GetTexCoord(), m_ifs->GetNTexCoord(), 2, m_params->m_texCoordPredMode, 
				 m_params->m_binarizationMode,  UniformTextureQuantization, m_QuantizationParameter,
				 static_cast<float *> (m_ifs->minTexCoord), static_cast<float *> (m_ifs->rangeTexCoord),
				 m_ifs->GetNCoordIndex(), m_ifs->GetCoordIndex(), NULL, NULL);
		}
	}

	// we update the number of bytes written
	compressedStreamSize = m_streamInfo.m_compressedStreamSize;
}



