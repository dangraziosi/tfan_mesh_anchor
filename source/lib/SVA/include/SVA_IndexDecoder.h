//-------------------------------------------------------------------------
// 
// Copyright ETRI, Hanyang University 2008, 2009
// Licensed Materials, Program Property of ETRI & Hanyang University
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.
// ETRI and Hanyang University retain full right to use the code 
// for his/her own purpose, assign or donate the code to a third party and
// to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Seung Wook Lee (ETRI)
//    Daiyong Kim (Hanyang University)
//    KyoungSoo Son (Hanyang University)
//
//    http://www.etri.re.kr/
//    http://dmlab.hanyang.ac.kr/
//  
//    Seung Wook Lee     (tajinet@etri.re.kr)  
//    Daiyong Kim        (dykim@dmlab.hanyang.ac.kr)    
//    KyoungSoo Son      (bonokensin@dmlab.hanyang.ac.kr)
//
//-------------------------------------------------------------------------



#ifndef _SVAINDEXDECODER_H
#define _SVAINDEXDECODER_H

#include <IndexedFaceSet.h>
#include <utils.h>

class SVAIndexDecoder{

	//Add here your features and functions
	//...
private:
	int *index;
	int nIndex_;
	int nData;

public :

	// Encoding results

	void Decode(DecoderStreamInfo &streamInfo, int *pIndex, int numberOfIndex, int numberOfData);
	void DecodeConnectivity(DecoderStreamInfo &streamInfo ,int binarization);
	unsigned int InverseCircularDifferenceForConnectivity(unsigned int previous, int differential);
	void InverseRotation(unsigned int *data, int size);

	SVAIndexDecoder(void);
	~SVAIndexDecoder(void);



};

#endif //_SVAINDEXDecoder

