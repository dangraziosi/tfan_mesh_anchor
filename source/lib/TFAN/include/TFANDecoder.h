#pragma once

#include "utils.h"
#include "IndexedFaceSet.h"
#include "TFANConnectivityDecoder.h"
#include "SC3DMCHeader.h"

class TFANDecoder
{
	SC3DMCHeader * m_header;
	IndexedFaceSet * m_ifs;	
	int m_vertexOrderPres;
	int m_triangleOrderPres;
	int m_trianglePermPres;
	unsigned char m_meshType;
	unsigned int m_streamSize;

	DecoderStreamInfo m_streamInfo;

	int m_coordPredMode; 
	int m_texCoordPredMode; 
	int m_normalPredMode; 
	int m_colorPredMode; 
	int m_otherAttributesPredMode; 

	int m_binarizationMode;

public:
	inline void SetHeader(SC3DMCHeader * header) {m_header = header;}
	inline void SetIFS(IndexedFaceSet * ifs){ m_ifs = ifs;}
	inline IndexedFaceSet * GetIFS(){ return m_ifs;}
	inline unsigned int GetStreamSize(){ return m_streamSize;}
	
	
	void DecodeTFANData(unsigned char * &compressedStream);
	TFANDecoder(): m_vertexOrderPres(0), m_triangleOrderPres(0), m_trianglePermPres(0), m_ifs(NULL), m_meshType(0), m_streamSize(0){};

	~TFANDecoder(void){};
};
