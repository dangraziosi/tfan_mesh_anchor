#pragma once
#include <vector>
#include <map>
#include "MultiVectOpt.h"
#include "BaseConnectivityDecoder.h"
typedef std::vector<int> IntVect;
typedef std::map<int, IntVect> IntMultiVect;
typedef std::vector<IntVect> VectIntVect;



class TFANConnectivityDecoder : public BaseConnectivityDecoder
{
	MultiVectOpt * tfans_;
	MultiVectOpt * vertex2Triangle_;

	int m_vertexOrderPres;
	int m_triangleOrderPres;
	int m_trianglePermPres;

	int vertexCount_;
	int triangleCount_;
	
	int  * vertexMapR_;
	int  * triangleMapR_;
	int  * triangleMapPos_;

	int* nbrFans_;
	int* degree_;
	int* cases_;
	int* vertices_;
	int* ops_;

	int nNbrFans_;
	int nDegree_;
	int nCases_;
	int nVertices_;
	int nOps_;
	unsigned int maxV2T_;


	int DecodeNTFans(DecoderStreamInfo &streamInfo);
	int DecodeDegrees(DecoderStreamInfo &streamInfo);
	int DecodeCases(DecoderStreamInfo &streamInfo);
	int DecodeVerticesIndices(DecoderStreamInfo &streamInfo);
	int DecodeOps(DecoderStreamInfo &streamInfo);
	int DecodeVertexOrder(DecoderStreamInfo &streamInfo);
	int DecodeTriangleOrder(DecoderStreamInfo &streamInfo);
	int DecodeTrianglePermutation(DecoderStreamInfo &streamInfo);

	void FreeInt(int ** tab, int dim);

	inline int GetNCoordIndex(){ return m_nTriangles;};
	inline int GetNCoord(){ return m_nVertices;};
	bool GetCoordIndex(int pos, int * coordIndex);
	void AddNeighborVertex2Triangle(int v, int t);

public:
	void ReOrderTab(float * tab, int D, int nVertices);
	void ReOrderTab(int * tab, int D, int nTriangles);
	void ReOrderConnec(int * coordIndex, int nCoordIndex);
	void ReOrderPerm(int * coordIndex, int nCoordIndex);

	inline void SetTrianglePermPres(int trianglePermPres){ m_trianglePermPres = trianglePermPres;};	

	inline void SetVertexOrderPres(int vertexOrderPres){ m_vertexOrderPres = vertexOrderPres;};
	inline void SetTriangleOrderPres(int triangleOrderPres){ m_triangleOrderPres = triangleOrderPres;};
	void ReadStream(IntVect & nbrFans, IntVect & degree, IntVect & cases, 
					IntVect & vertices, IntVect & ops,
					int * coordIndex, int nCoordIndex, int nCoord, unsigned int maxV2T);

	inline MultiVectOpt * GetTFans(){return tfans_;};
	inline MultiVectOpt * GetV2T(){return vertex2Triangle_;};
	
	void Decode();

	int Decode(DecoderStreamInfo &streamInfo);

	TFANConnectivityDecoder(void);
	~TFANConnectivityDecoder(void);
};
