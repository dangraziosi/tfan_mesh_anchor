#include <vector>
#include <map>
#include "BinaryIO.h"
#include "BaseConnectivityEncoder.h"
#include "utils.h"

typedef std::vector<int> IntVect;
typedef std::map<int, IntVect> IntMultiVect;
typedef std::vector<IntVect> VectIntVect;

class TFANConnectivityEncoder : public BaseConnectivityEncoder
{
	VectIntVect vertex2Vertex_;
	VectIntVect vertex2Triangle_;
	VectIntVect triangle2Triangle_;

	int m_vertexOrderPres;
	int m_triangleOrderPres;
	int m_trianglePermPres;

	int  * tagsV_;
	int  * tagsT_;

	int vertexCount_;
	int  * vertexMap_;

	int triangleCount_;
	int  * triangleMap_;

	int * triangleMapPos_;

	unsigned int maxV2T_;
	void Init();
	void Free();




	inline int GetNCoordIndex(){ return m_nTriangles;};
	bool GetCoordIndex(int pos, int * coordIndex);

	void AddNeighborVertex2Vertex(int v1, int v2);
	void ComputeVertex2Vertex();
	void AddNeighborVertex2Triangle(int v, int t);
	void ComputeVertex2Triangle();
	void AddNeighborTriangle2Triangle(int t1, int t2);
	void ComputeTriangle2Triangle();
	void AddTriangles(int focusVertex, std::vector<int> &VL);

	bool IsCase0(int degree, std::vector<int> &ops, std::vector<int> &vertices);
	bool IsCase1(int degree, std::vector<int> &ops, std::vector<int> &vertices);
	bool IsCase2(int degree, std::vector<int> &ops, std::vector<int> &vertices);
	bool IsCase3(int degree, std::vector<int> &ops, std::vector<int> &vertices);
	bool IsCase4(int degree, std::vector<int> &ops, std::vector<int> &vertices);
	bool IsCase5(int degree, std::vector<int> &ops, std::vector<int> &vertices);
	bool IsCase6(int degree, std::vector<int> &ops, std::vector<int> &vertices);
	bool IsCase7(int degree, std::vector<int> &ops, std::vector<int> &vertices);
	bool IsCase8(int degree, std::vector<int> &ops, std::vector<int> &vertices);

	int EncodeNTFans(EncoderStreamInfo &streamInfo);
	int EncodeDegrees(EncoderStreamInfo &streamInfo);
	int EncodeCases(EncoderStreamInfo &streamInfo);
	int EncodeVerticesIndices(EncoderStreamInfo &streamInfo);
	int EncodeOps(EncoderStreamInfo &streamInfo);


	
	int EncodeVertexOrder(EncoderStreamInfo &streamInfo);
	void GetEncodeVertexOrderParameters(int * vertexMapR, unsigned char & K);
	int EncodeVertexOrder(int * vertexMapR, unsigned char K);

	int EncodeTriangleOrder(EncoderStreamInfo &streamInfo);
	void GetEncodeTriangleOrderParameters(int * triangleMapR, unsigned char & K);
	int EncodeTriangleOrder(int * triangleMapR, unsigned char K);

	int EncodeTrianglePermutation(EncoderStreamInfo &streamInfo);
	// arithmetic encoding

public:
// output
	IntVect nbrFans_;
	IntVect degree_;
	IntVect cases_;
	IntVect vertices_;
	IntVect ops_;

	void ReOrderTab(float * tab, int D, int nVertices);
	void ReOrderTab(int * tab, int D, int nTriangles);
	void ReOrderConnec(int * coordIndex, int nCoordIndex);
	void ReOrderPerm(int * coordIndex, int nCoordIndex);

	inline unsigned int GetMaxV2T(){ return maxV2T_;};

	inline void SetVertexOrderPres(int vertexOrderPres){ m_vertexOrderPres = vertexOrderPres;};
	inline void SetTriangleOrderPres(int triangleOrderPres){ m_triangleOrderPres = triangleOrderPres;};
	inline void SetTrianglePermPres(int trianglePermPres){ m_trianglePermPres = trianglePermPres;};	

	// This function is called before encode
	// It should return the nombre of bytes to be allocated for the compressed stream
	unsigned int GetEstimatedCompressedStreamSize() { return 0;};

	// This function encodes the list of triangles
	// It returns the compressed stream as a char * and the size of the memory space used to store the compressed stream
	// Rq.: no memory allocation is done inside this function. The user should allocate and free the memory. 
	int Encode(EncoderStreamInfo &streamInfo);


	TFANConnectivityEncoder(void);
	~TFANConnectivityEncoder(void);
};
