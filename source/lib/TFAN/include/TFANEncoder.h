#pragma once
#include "IndexedFaceSet.h"
#include "TFANConnectivityEncoder.h"
#include "TFANConnectivityDecoder.h"
#include "EncodingParameter.h"
#include "BinaryIO.h"
#include "utils.h"

class TFANEncoder
{
	IndexedFaceSet * m_ifs;
	EncodingParams * m_params;
	EncoderStreamInfo m_streamInfo;

public:
	inline void SetIFS(IndexedFaceSet * ifs){ m_ifs = ifs;}
	inline void SetParams(EncodingParams * params){ m_params = params;}

	// This function is called before encode
	// It should return the nombre of bytes to be allocated for the compressed stream
	unsigned int GetEstimatedCompressedStreamSize() { return 50000000;};

	
	//	Writes the main stream
	void EncodeDataBuffer(unsigned char * compressedStream, unsigned int & compressedStreamSize);


	TFANEncoder(void):m_ifs(NULL){};
	~TFANEncoder(void){};
};
