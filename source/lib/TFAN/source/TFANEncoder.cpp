//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom 2008, 2009
// Licensed Materials, Program Property of Institut Telecom
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// retains full right to use the code for his/her own purpose, assign or donate 
// the code to a third party and to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//
//
//    http://www-artemis.it-sudparis.eu/
//    
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//
//-------------------------------------------------------------------------
#include "TFANEncoder.h"
#include "TFANDecoder.h"
//#include "QBCR_Encoder.h"



void TFANEncoder::EncodeDataBuffer(unsigned char * compressedStream, unsigned int & compressedStreamSize)
{
	m_streamInfo.m_compressedStream = compressedStream; 
	m_streamInfo.m_compressedStreamSize = compressedStreamSize;

	unsigned char compressionMask = 0;
	if (m_params->m_vertexOrderPres == 1) compressionMask += 1;
	if (m_params->m_triangleOrderPres == 1) compressionMask += 2;
	if (m_params->m_trianglePermPres == 1) compressionMask += 4;
	Write2Stream(m_streamInfo.m_compressedStream, m_streamInfo.m_compressedStreamSize, &compressionMask, sizeof(unsigned char)); 

	if (m_ifs->GetNCoordIndex() >0) 
	{
		TFANConnectivityEncoder coordIndexEncoder;
		coordIndexEncoder.SetTriangleList(m_ifs->GetCoordIndex());
		coordIndexEncoder.SetNTriangles(m_ifs->GetNCoordIndex());
		coordIndexEncoder.SetNVertices(m_ifs->GetNCoord());
		coordIndexEncoder.SetVertexOrderPres(m_params->m_vertexOrderPres);
		coordIndexEncoder.SetTriangleOrderPres(m_params->m_triangleOrderPres);
		coordIndexEncoder.SetTrianglePermPres(m_params->m_trianglePermPres);
		int connectivitySize = coordIndexEncoder.Encode(m_streamInfo);
		printf("VERTEX CONNECTIVITY   %i (bytes)\n", connectivitySize);

		// compute the decoder connectivity
		TFANConnectivityDecoder coordIndexDecoder;
		coordIndexDecoder.ReadStream(coordIndexEncoder.nbrFans_, coordIndexEncoder.degree_, 
								 coordIndexEncoder.cases_, coordIndexEncoder.vertices_, coordIndexEncoder.ops_,
								 m_ifs->GetCoordIndex(), m_ifs->GetNCoordIndex(), m_ifs->GetNCoord(),
								 coordIndexEncoder.GetMaxV2T());
		coordIndexDecoder.Decode();
		//===========================================

		TFANConnectivityEncoder normalIndexEncoder;
		TFANConnectivityDecoder normalIndexDecoder;
		if ((m_ifs->GetNNormalIndex()>0))
		{
			if (m_ifs->GetNormalPerVertex())
			{
				// reorder the normalIndex
				coordIndexEncoder.ReOrderTab(m_ifs->GetNormalIndex(), 3, m_ifs->GetNNormalIndex());
				coordIndexEncoder.ReOrderPerm(m_ifs->GetNormalIndex(), m_ifs->GetNNormalIndex());
				// encode the normalIndex
				normalIndexEncoder.SetTriangleList(m_ifs->GetNormalIndex());
				normalIndexEncoder.SetNTriangles(m_ifs->GetNNormalIndex());
				normalIndexEncoder.SetNVertices(m_ifs->GetNNormal());
				normalIndexEncoder.SetVertexOrderPres(m_params->m_vertexOrderPres);
				normalIndexEncoder.SetTriangleOrderPres(1);
				normalIndexEncoder.SetTrianglePermPres(1);
				int normalSize = normalIndexEncoder.Encode(m_streamInfo);
				printf("NORMAL CONNECTIVITY   %i (bytes)\n", normalSize);
				// compute the decoder connectivity
				normalIndexDecoder.ReadStream(normalIndexEncoder.nbrFans_, normalIndexEncoder.degree_, 
									 normalIndexEncoder.cases_, normalIndexEncoder.vertices_, normalIndexEncoder.ops_,
									 m_ifs->GetNormalIndex(), m_ifs->GetNNormalIndex(), m_ifs->GetNNormal(),
									 normalIndexEncoder.GetMaxV2T());
				normalIndexDecoder.Decode();
				//===========================================
			}
			else
			{
				int normalSize = m_streamInfo.m_compressedStreamSize;
				EncodeIntArray(m_streamInfo, m_ifs->GetNormalIndex(), m_ifs->GetNNormalIndex(), 1, 3, 4, int(log((float)(m_ifs->GetNNormalIndex())) / log(2.0)) +1, m_ifs->GetNNormalIndex());
				printf("NORMAL CONNECTIVITY   %i (bytes)\n", m_streamInfo.m_compressedStreamSize - normalSize);
			}
		}
		
		TFANConnectivityEncoder colorIndexEncoder;
		TFANConnectivityDecoder colorIndexDecoder;
		if (m_ifs->GetNColorIndex()>0)
		{
			if (m_ifs->GetColorPerVertex())
			{
				// reorder the colorIndex
				coordIndexEncoder.ReOrderTab(m_ifs->GetColorIndex(), 3, m_ifs->GetNColorIndex());
				coordIndexEncoder.ReOrderPerm(m_ifs->GetColorIndex(), m_ifs->GetNColorIndex());
				// encode the colorIndex
				colorIndexEncoder.SetTriangleList(m_ifs->GetColorIndex());
				colorIndexEncoder.SetNTriangles(m_ifs->GetNColorIndex());
				colorIndexEncoder.SetNVertices(m_ifs->GetNColor());
				colorIndexEncoder.SetVertexOrderPres(m_params->m_vertexOrderPres);
				colorIndexEncoder.SetTriangleOrderPres(1);
				colorIndexEncoder.SetTrianglePermPres(1);
				int colorSize = colorIndexEncoder.Encode(m_streamInfo);
				printf("COLOR CONNECTIVITY    %i (bytes)\n", colorSize);

				// compute the decoder connectivity
				colorIndexDecoder.ReadStream(colorIndexEncoder.nbrFans_, colorIndexEncoder.degree_, 
									 colorIndexEncoder.cases_, colorIndexEncoder.vertices_, colorIndexEncoder.ops_,
									 m_ifs->GetColorIndex(), m_ifs->GetNColorIndex(), m_ifs->GetNColor(),
									 colorIndexEncoder.GetMaxV2T());
				colorIndexDecoder.Decode();
				//===========================================
			}
			else
			{
				int colorSize = m_streamInfo.m_compressedStreamSize;
				EncodeIntArray(m_streamInfo, m_ifs->GetColorIndex(), m_ifs->GetNColorIndex(), 1, 3, 4, int(log((float)(m_ifs->GetNColorIndex())) / log(2.0)) +1, m_ifs->GetNColorIndex());
				printf("COLOR CONNECTIVITY    %i (bytes)\n", m_streamInfo.m_compressedStreamSize - colorSize);
			}
		}
		
		TFANConnectivityEncoder texCoordIndexEncoder;
		TFANConnectivityDecoder texCoordIndexDecoder;
		if (m_ifs->GetNTexCoordIndex()>0)
		{
			// reorder the texCoordIndex
			coordIndexEncoder.ReOrderTab(m_ifs->GetTexCoordIndex(), 3, m_ifs->GetNTexCoordIndex());
			coordIndexEncoder.ReOrderPerm(m_ifs->GetTexCoordIndex(), m_ifs->GetNTexCoordIndex());
			// encode the texCoordIndex
			texCoordIndexEncoder.SetTriangleList(m_ifs->GetTexCoordIndex());
			texCoordIndexEncoder.SetNTriangles(m_ifs->GetNTexCoordIndex());
			texCoordIndexEncoder.SetNVertices(m_ifs->GetNTexCoord());
			texCoordIndexEncoder.SetVertexOrderPres(m_params->m_vertexOrderPres);
			texCoordIndexEncoder.SetTriangleOrderPres(1);
			texCoordIndexEncoder.SetTrianglePermPres(1);
			int texCoordSize = texCoordIndexEncoder.Encode(m_streamInfo);
			printf("TEXTURE CONNECTIVITY  %i (bytes)\n", texCoordSize);

			// compute the decoder connectivity
			texCoordIndexDecoder.ReadStream(texCoordIndexEncoder.nbrFans_, texCoordIndexEncoder.degree_, 
								 texCoordIndexEncoder.cases_, texCoordIndexEncoder.vertices_, texCoordIndexEncoder.ops_,
								 m_ifs->GetTexCoordIndex(), m_ifs->GetNTexCoordIndex(), m_ifs->GetNTexCoord(),
								 texCoordIndexEncoder.GetMaxV2T());
			texCoordIndexDecoder.Decode();
			//===========================================
		}

		int QuantizationParameter[3] = {0, 0, 0};
		if (m_ifs->GetNCoord() >0)
		{
			int vertexSize = m_streamInfo.m_compressedStreamSize;
			QuantizationParameter[0] = m_params->m_nQCoordBits;
			// reorder the coord array
			coordIndexEncoder.ReOrderTab(m_ifs->GetCoord(), 3, m_ifs->GetNCoord());
			EncodeFloatArray(m_streamInfo, m_ifs->GetCoord(), m_ifs->GetNCoord(), 3, m_params->m_coordPredMode, 
					 m_params->m_binarizationMode, 0, QuantizationParameter, 
					 static_cast<float *> (m_ifs->minCoord), static_cast<float *> (m_ifs->rangeCoord), 
					 m_ifs->GetNCoordIndex(), m_ifs->GetCoordIndex(), coordIndexDecoder.GetTFans(), coordIndexDecoder.GetV2T());
			printf("VERTEX POSITION       %i (bytes)\n", m_streamInfo.m_compressedStreamSize - vertexSize);
		}
		if (m_ifs->GetNNormal() >0)
		{
			QuantizationParameter[0] = m_params->m_nQNormalBits;
			int normalSize = m_streamInfo.m_compressedStreamSize;
			if (m_ifs->GetNormalPerVertex())
			{
				if (m_ifs->GetNNormalIndex()>0)
				{
					// reorder the normal array
					normalIndexEncoder.ReOrderTab(m_ifs->GetNormal(), 3, m_ifs->GetNNormal());
					EncodeFloatArray(m_streamInfo, m_ifs->GetNormal(), m_ifs->GetNNormal(), 3, m_params->m_normalPredMode, 
					 m_params->m_binarizationMode, 1, QuantizationParameter, 
					 static_cast<float *> (m_ifs->minNormal), static_cast<float *> (m_ifs->rangeNormal),
					 m_ifs->GetNNormalIndex(), m_ifs->GetNormalIndex(), normalIndexDecoder.GetTFans(), normalIndexDecoder.GetV2T());
					printf("VERTEX NORMAL(IDX)    %i (bytes)\n", m_streamInfo.m_compressedStreamSize - normalSize);
				}
				else
				{
					// reorder the normal array
					coordIndexEncoder.ReOrderTab(m_ifs->GetNormal(), 3, m_ifs->GetNNormal());		
					EncodeFloatArray(m_streamInfo, m_ifs->GetNormal(), m_ifs->GetNNormal(), 3, m_params->m_normalPredMode, 
					 m_params->m_binarizationMode, 1, QuantizationParameter, 
					 static_cast<float *> (m_ifs->minNormal), static_cast<float *> (m_ifs->rangeNormal),
					 m_ifs->GetNCoordIndex(), m_ifs->GetCoordIndex(), coordIndexDecoder.GetTFans(), coordIndexDecoder.GetV2T());
					printf("VERTEX NORMAL         %i (bytes)\n", m_streamInfo.m_compressedStreamSize - normalSize);
				}
			}
			else
			{
				EncodeFloatArray(m_streamInfo, m_ifs->GetNormal(), m_ifs->GetNNormal(), 3, 1, 
					 4, 1, QuantizationParameter, static_cast<float *> (m_ifs->minNormal), static_cast<float *> (m_ifs->rangeNormal));
				printf("NORMAL ARRAY          %i (bytes)\n", m_streamInfo.m_compressedStreamSize - normalSize);
			}
		}

		if (m_ifs->GetNColor() >0)
		{
			int colorSize = m_streamInfo.m_compressedStreamSize;
			QuantizationParameter[0] = m_params->m_nQColorBits;
			if (m_ifs->GetColorPerVertex())
			{
				if (m_ifs->GetNColorIndex()>0)
				{
					// reorder the color array
					colorIndexEncoder.ReOrderTab(m_ifs->GetColor(), 3, m_ifs->GetNColor());		
					EncodeFloatArray(m_streamInfo, m_ifs->GetColor(), m_ifs->GetNColor(), 3, m_params->m_colorPredMode, 
						 m_params->m_binarizationMode, 0, QuantizationParameter, 
						 static_cast<float *> (m_ifs->minColor), static_cast<float *> (m_ifs->rangeColor),
						 m_ifs->GetNColorIndex(), m_ifs->GetColorIndex(), colorIndexDecoder.GetTFans(), colorIndexDecoder.GetV2T());
					printf("VERTEX COLOR(IDX)\t\t%i (bytes)\n", m_streamInfo.m_compressedStreamSize - colorSize);
				}
				else
				{
					// reorder the color array
					coordIndexEncoder.ReOrderTab(m_ifs->GetColor(), 3, m_ifs->GetNColor());		
					EncodeFloatArray(m_streamInfo, m_ifs->GetColor(), m_ifs->GetNColor(), 3, m_params->m_colorPredMode, 
						 m_params->m_binarizationMode, 0, QuantizationParameter, 
						 static_cast<float *> (m_ifs->minColor), static_cast<float *> (m_ifs->rangeColor),
						 m_ifs->GetNCoordIndex(), m_ifs->GetCoordIndex(), coordIndexDecoder.GetTFans(), coordIndexDecoder.GetV2T());
					printf("VERTEX COLOR\t\t%i (bytes)\n", m_streamInfo.m_compressedStreamSize - colorSize);
				}
			}
			else
			{
				EncodeFloatArray(m_streamInfo, m_ifs->GetColor(), m_ifs->GetNColor(), 3, 1, 4, 0, QuantizationParameter, 
					 static_cast<float *> (m_ifs->minColor), static_cast<float *> (m_ifs->rangeColor));
				printf("COLOR ARRAY\t\t%i (bytes)\n", m_streamInfo.m_compressedStreamSize - colorSize);
			}
		}

		if (m_ifs->GetNTexCoord() >0)
		{
			int texCoordSize = m_streamInfo.m_compressedStreamSize;
			if(m_params->m_nQTexCoordWidth > m_params->m_nQTexCoordHeight)
				QuantizationParameter[0] = int(log((float)(m_params->m_nQTexCoordWidth)) /log(2.0))+1;
			else
				QuantizationParameter[0] = int(log((float)(m_params->m_nQTexCoordHeight)) / log(2.0))+1;
			QuantizationParameter[1] = m_params->m_nQTexCoordWidth;
			QuantizationParameter[2] = m_params->m_nQTexCoordHeight;
			if (m_ifs->GetNTexCoordIndex()>0)
			{
				// reorder the texCoord array
				texCoordIndexEncoder.ReOrderTab(m_ifs->GetTexCoord(), 2, m_ifs->GetNTexCoord());		
				EncodeFloatArray(m_streamInfo, m_ifs->GetTexCoord(), m_ifs->GetNTexCoord(), 2, m_params->m_texCoordPredMode, 
					m_params->m_binarizationMode, 2, QuantizationParameter, 
					static_cast<float *> (m_ifs->minTexCoord), static_cast<float *> (m_ifs->rangeTexCoord),
					m_ifs->GetNTexCoordIndex(), m_ifs->GetTexCoordIndex(), texCoordIndexDecoder.GetTFans(), texCoordIndexDecoder.GetV2T());
				printf("TEXTURE COORD(IDX)    %i (bytes)\n", m_streamInfo.m_compressedStreamSize - texCoordSize);
			}
			else
			{
				// reorder the texCoord array
				coordIndexEncoder.ReOrderTab(m_ifs->GetTexCoord(), 2, m_ifs->GetNTexCoord());		
				EncodeFloatArray(m_streamInfo, m_ifs->GetTexCoord(), m_ifs->GetNTexCoord(), 2, m_params->m_texCoordPredMode, 
					 m_params->m_binarizationMode, 2, QuantizationParameter, 
					static_cast<float *> (m_ifs->minTexCoord), static_cast<float *> (m_ifs->rangeTexCoord),
					m_ifs->GetNCoordIndex(), m_ifs->GetCoordIndex(), coordIndexDecoder.GetTFans(), coordIndexDecoder.GetV2T());
				printf("TEXTURE COORD         %i (bytes)\n", m_streamInfo.m_compressedStreamSize - texCoordSize);
			}
		}
	}

	// we update the number of bytes written
	compressedStreamSize = m_streamInfo.m_compressedStreamSize;
	printf("TOTAL SIZE            %i (bytes)\n", compressedStreamSize);
}