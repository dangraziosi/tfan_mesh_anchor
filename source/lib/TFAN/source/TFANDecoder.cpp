//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom 2008, 2009
// Licensed Materials, Program Property of Institut Telecom
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// retains full right to use the code for his/her own purpose, assign or donate 
// the code to a third party and to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//
//
//    http://www-artemis.it-sudparis.eu/
//    
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//
//-------------------------------------------------------------------------
#include "TFANDecoder.h"
//#include "QBCR_Decoder.h"

void TFANDecoder::DecodeTFANData(unsigned char * &compressedStream)
{
	m_streamInfo.m_compressedStream = compressedStream;

	unsigned char compressionMask = 0;
	ReadFromStream(m_streamInfo.m_compressedStream, &compressionMask, sizeof(unsigned char));
	if (compressionMask & 1) m_vertexOrderPres = 1;
	if (compressionMask & 2) m_triangleOrderPres = 1;
	if (compressionMask & 4) m_trianglePermPres = 1;

		if (m_ifs->GetNCoordIndex() >0) 
		{
			TFANConnectivityDecoder coordIndexDecoder;
			coordIndexDecoder.SetTriangleList(m_ifs->GetCoordIndex());
			coordIndexDecoder.SetNTriangles(m_ifs->GetNCoordIndex());
			coordIndexDecoder.SetNVertices(m_ifs->GetNCoord());
			coordIndexDecoder.SetVertexOrderPres(m_vertexOrderPres);
			coordIndexDecoder.SetTriangleOrderPres(m_triangleOrderPres);
			coordIndexDecoder.SetTrianglePermPres(m_trianglePermPres);
			coordIndexDecoder.Decode(m_streamInfo);

			TFANConnectivityDecoder normalIndexDecoder;
			if (m_ifs->GetNNormalIndex() >0) 
			{
				if (m_ifs->GetNormalPerVertex())
				{
					normalIndexDecoder.SetTriangleList(m_ifs->GetNormalIndex());
					normalIndexDecoder.SetNTriangles(m_ifs->GetNNormalIndex());
					normalIndexDecoder.SetNVertices(m_ifs->GetNNormal());
					normalIndexDecoder.SetVertexOrderPres(m_vertexOrderPres);
					normalIndexDecoder.SetTriangleOrderPres(1);
					normalIndexDecoder.SetTrianglePermPres(1);
					normalIndexDecoder.Decode(m_streamInfo);
				}
				else
				{
					int predModeNormalIndex = -1;
					int binarizationModeNormalIndex = -1;
					DecodeIntArray(m_streamInfo, m_ifs->GetNNormalIndex(), m_ifs->GetNormalIndex(), m_ifs->GetNNormalIndex(), 1, predModeNormalIndex, binarizationModeNormalIndex, int(log((float)(m_ifs->GetNNormalIndex())) / log(2.0)) +1);
				}
			}

			TFANConnectivityDecoder colorIndexDecoder;
			if (m_ifs->GetNColorIndex() >0) 
			{
				if (m_ifs->GetColorPerVertex())
				{
					colorIndexDecoder.SetTriangleList(m_ifs->GetColorIndex());
					colorIndexDecoder.SetNTriangles(m_ifs->GetNColorIndex());
					colorIndexDecoder.SetNVertices(m_ifs->GetNColor());
					colorIndexDecoder.SetVertexOrderPres(m_vertexOrderPres);
					colorIndexDecoder.SetTriangleOrderPres(1);
					colorIndexDecoder.SetTrianglePermPres(1);
					colorIndexDecoder.Decode(m_streamInfo);
				}
				else
				{
					int predModeColorIndex = -1;
					int binarizationModeColorIndex = -1;
					DecodeIntArray(m_streamInfo, m_ifs->GetNColorIndex(), m_ifs->GetColorIndex(), m_ifs->GetNColorIndex(), 1, predModeColorIndex, binarizationModeColorIndex, int(log((float)(m_ifs->GetNColorIndex())) / log(2.0)) +1);
				}
			}

			TFANConnectivityDecoder texCoordIndexDecoder;
			if (m_ifs->GetNTexCoordIndex() >0) 
			{
				texCoordIndexDecoder.SetTriangleList(m_ifs->GetTexCoordIndex());
				texCoordIndexDecoder.SetNTriangles(m_ifs->GetNTexCoordIndex());
				texCoordIndexDecoder.SetNVertices(m_ifs->GetNTexCoord());
				texCoordIndexDecoder.SetVertexOrderPres(m_vertexOrderPres);
				texCoordIndexDecoder.SetTriangleOrderPres(1);
				texCoordIndexDecoder.SetTrianglePermPres(1);
				texCoordIndexDecoder.Decode(m_streamInfo);
			}

			int QuantizationParameter[3] ={0, 0, 0};
			if (m_ifs->GetNCoord() >0)
			{
				QuantizationParameter[0] = m_header->QPforGeometry;
				DecodeFloatArray(m_streamInfo, m_ifs->GetCoord(), m_ifs->GetNCoord(), 3, m_coordPredMode, 
					 m_binarizationMode, 0, QuantizationParameter, 
					 static_cast<float *> (m_header->quantMinGeometry), static_cast<float *> (m_header->rangeGeometry),
					 m_ifs->GetNCoordIndex(), m_ifs->GetCoordIndex(), coordIndexDecoder.GetTFans(), coordIndexDecoder.GetV2T());
				if (m_vertexOrderPres == 1) coordIndexDecoder.ReOrderTab(m_ifs->GetCoord(), 3, m_ifs->GetNCoord());
			}

			if (m_ifs->GetNNormal() >0)
			{
				QuantizationParameter[0] = m_header->QPforNormal;
				if (m_ifs->GetNormalPerVertex())
				{
					if (m_ifs->GetNNormalIndex()>0)
					{
						DecodeFloatArray(m_streamInfo, m_ifs->GetNormal(), m_ifs->GetNNormal(), 3, m_normalPredMode, 
							 m_binarizationMode, 1, QuantizationParameter, 
							 static_cast<float *> (m_header->quantMinNormal), static_cast<float *> (m_header->rangeNormal),
							 m_ifs->GetNNormalIndex(), m_ifs->GetNormalIndex(), normalIndexDecoder.GetTFans(), normalIndexDecoder.GetV2T());
						if (m_vertexOrderPres == 1) normalIndexDecoder.ReOrderTab(m_ifs->GetNormal(), 3, m_ifs->GetNNormal());
					}
					else
					{
						DecodeFloatArray(m_streamInfo, m_ifs->GetNormal(), m_ifs->GetNNormal(), 3, m_normalPredMode, 
								 m_binarizationMode, 1, QuantizationParameter, 
								 static_cast<float *> (m_header->quantMinNormal), static_cast<float *> (m_header->rangeNormal),
								 m_ifs->GetNCoordIndex(), m_ifs->GetCoordIndex(), coordIndexDecoder.GetTFans(), coordIndexDecoder.GetV2T());
						if (m_vertexOrderPres == 1) coordIndexDecoder.ReOrderTab(m_ifs->GetNormal(), 3, m_ifs->GetNNormal());
					}
				}
				else
				{
					DecodeFloatArray(m_streamInfo, m_ifs->GetNormal(), m_ifs->GetNNormal(), 3, m_normalPredMode, 
						 m_binarizationMode, 1, QuantizationParameter, static_cast<float *>(m_header->quantMinNormal),  static_cast<float *> (m_header->rangeNormal));
				}
			}


			if (m_ifs->GetNColor() >0)
			{
				QuantizationParameter[0] = m_header->QPforColor;
				if (m_ifs->GetColorPerVertex())
				{
					if (m_ifs->GetNColorIndex()>0)
					{
						DecodeFloatArray(m_streamInfo, m_ifs->GetColor(), m_ifs->GetNColor(), 3, m_colorPredMode, 
							 m_binarizationMode, 0, QuantizationParameter, 
							 static_cast<float *> (m_header->quantMinColor), static_cast<float *> (m_header->rangeColor),
							 m_ifs->GetNColorIndex(), m_ifs->GetColorIndex(), colorIndexDecoder.GetTFans(), colorIndexDecoder.GetV2T());
						if (m_vertexOrderPres == 1) colorIndexDecoder.ReOrderTab(m_ifs->GetColor(), 3, m_ifs->GetNColor());
					}
					else
					{
						DecodeFloatArray(m_streamInfo, m_ifs->GetColor(), m_ifs->GetNColor(), 3, m_colorPredMode, 
							 m_binarizationMode, 0, QuantizationParameter, 
							 static_cast<float *> (m_header->quantMinColor), static_cast<float *> (m_header->rangeColor),
							 m_ifs->GetNCoordIndex(), m_ifs->GetCoordIndex(), coordIndexDecoder.GetTFans(), coordIndexDecoder.GetV2T());
						if (m_vertexOrderPres == 1) coordIndexDecoder.ReOrderTab(m_ifs->GetColor(), 3, m_ifs->GetNColor());
					}
				}
				else
				{
					DecodeFloatArray(m_streamInfo, m_ifs->GetColor(), m_ifs->GetNColor(), 3, m_colorPredMode, 
						m_binarizationMode, 0, QuantizationParameter, 
						static_cast<float *> (m_header->quantMinColor), static_cast<float *> (m_header->rangeColor));				
				}
			}


			if (m_ifs->GetNTexCoord() >0)
			{
				QuantizationParameter[1] = m_header->TexCoordWidth;
				QuantizationParameter[2] = m_header->TexCoordHeight;
				if( m_header->TexCoordWidth >  m_header->TexCoordHeight)
					QuantizationParameter[0] = int(log((float)(m_header->TexCoordWidth)) / log(2.0))+1;
				else
					QuantizationParameter[0] = int(log((float)(m_header->TexCoordHeight)) /log(2.0))+1;

				if (m_ifs->GetNTexCoordIndex()>0)
				{
					DecodeFloatArray(m_streamInfo, m_ifs->GetTexCoord(), m_ifs->GetNTexCoord(), 2, m_texCoordPredMode, 
						m_binarizationMode, 2, QuantizationParameter, 
						static_cast<float *> (m_header->quantMinTexCoord), static_cast<float *> (m_header->rangeTexCoord),
						m_ifs->GetNTexCoordIndex(), m_ifs->GetTexCoordIndex(), texCoordIndexDecoder.GetTFans(), texCoordIndexDecoder.GetV2T());
					if (m_vertexOrderPres == 1) texCoordIndexDecoder.ReOrderTab(m_ifs->GetTexCoord(), 2, m_ifs->GetNTexCoord());
				}
				else
				{
					DecodeFloatArray(m_streamInfo, m_ifs->GetTexCoord(), m_ifs->GetNTexCoord(), 2, m_texCoordPredMode, 
						 m_binarizationMode, 2, QuantizationParameter, 
						 static_cast<float *> (m_header->quantMinTexCoord), static_cast<float *> (m_header->rangeTexCoord),
						 m_ifs->GetNCoordIndex(), m_ifs->GetCoordIndex(), coordIndexDecoder.GetTFans(), coordIndexDecoder.GetV2T());
					if (m_vertexOrderPres == 1) coordIndexDecoder.ReOrderTab(m_ifs->GetTexCoord(), 2, m_ifs->GetNTexCoord());
				}
			}


			if (m_ifs->GetNNormalIndex() >0) 
			{
				if (m_vertexOrderPres == 1) normalIndexDecoder.ReOrderConnec(m_ifs->GetNormalIndex(), m_ifs->GetNNormalIndex());
				normalIndexDecoder.ReOrderPerm(m_ifs->GetNormalIndex(), m_ifs->GetNNormalIndex());
				normalIndexDecoder.ReOrderTab(m_ifs->GetNormalIndex(), 3, m_ifs->GetNNormalIndex());
				if (m_trianglePermPres == 1) coordIndexDecoder.ReOrderPerm(m_ifs->GetNormalIndex(), m_ifs->GetNNormalIndex());
				if (m_triangleOrderPres == 1) coordIndexDecoder.ReOrderTab(m_ifs->GetNormalIndex(), 3, m_ifs->GetNNormalIndex());
			}
			if (m_ifs->GetNColorIndex() >0) 
			{
				if (m_vertexOrderPres == 1) colorIndexDecoder.ReOrderConnec(m_ifs->GetColorIndex(), m_ifs->GetNColorIndex());
				colorIndexDecoder.ReOrderPerm(m_ifs->GetColorIndex(), m_ifs->GetNColorIndex());
				colorIndexDecoder.ReOrderTab(m_ifs->GetColorIndex(), 3, m_ifs->GetNColorIndex());
				if (m_trianglePermPres == 1) coordIndexDecoder.ReOrderPerm(m_ifs->GetColorIndex(), m_ifs->GetNColorIndex());
				if (m_triangleOrderPres == 1) coordIndexDecoder.ReOrderTab(m_ifs->GetColorIndex(), 3, m_ifs->GetNColorIndex());
			}
			if (m_ifs->GetNTexCoordIndex() >0) 
			{
				if (m_vertexOrderPres == 1) texCoordIndexDecoder.ReOrderConnec(m_ifs->GetTexCoordIndex(), m_ifs->GetNTexCoordIndex());
				texCoordIndexDecoder.ReOrderPerm(m_ifs->GetTexCoordIndex(), m_ifs->GetNTexCoordIndex());
				texCoordIndexDecoder.ReOrderTab(m_ifs->GetTexCoordIndex(), 3, m_ifs->GetNTexCoordIndex());
				if (m_trianglePermPres == 1) coordIndexDecoder.ReOrderPerm(m_ifs->GetTexCoordIndex(), m_ifs->GetNTexCoordIndex());
				if (m_triangleOrderPres == 1) coordIndexDecoder.ReOrderTab(m_ifs->GetTexCoordIndex(), 3, m_ifs->GetNTexCoordIndex());
			}

			// reorder the coord array
			if (m_vertexOrderPres == 1) {
				coordIndexDecoder.ReOrderConnec(m_ifs->GetCoordIndex(), m_ifs->GetNCoordIndex());
			}
			// reorder the coordIndex
			if (m_trianglePermPres == 1) {
				coordIndexDecoder.ReOrderPerm(m_ifs->GetCoordIndex(), m_ifs->GetNCoordIndex());
			}
			if (m_triangleOrderPres == 1) {
				coordIndexDecoder.ReOrderTab(m_ifs->GetCoordIndex(), 3, m_ifs->GetNCoordIndex());
			}

		}
		compressedStream = m_streamInfo.m_compressedStream;
	}
