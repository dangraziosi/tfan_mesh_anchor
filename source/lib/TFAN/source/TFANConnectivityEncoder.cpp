//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom 2008, 2009
// Licensed Materials, Program Property of Institut Telecom
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// retains full right to use the code for his/her own purpose, assign or donate 
// the code to a third party and to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//
//
//    http://www-artemis.it-sudparis.eu/
//    
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//
//-------------------------------------------------------------------------
#include "TFANConnectivityEncoder.h"
#include "Arithmetic_Codec.h"
#include "BinaryIO.h"
#include "MathTools.h"
//#include "QBCR_Encoder.h"
#include <time.h>

TFANConnectivityEncoder::TFANConnectivityEncoder(void)
{	
	tagsV_ = NULL;
	tagsT_ = NULL;
	vertexMap_ = NULL;
	triangleCount_ = 0;
	triangleMap_ = NULL;
	triangleMapPos_ = NULL;
	vertexCount_ = 0;
	maxV2T_ = 0;
	m_trianglePermPres = 0;
	m_vertexOrderPres = 0;
	m_triangleOrderPres = 0;
	m_trianglePermPres = 0;
}

TFANConnectivityEncoder::~TFANConnectivityEncoder(void)
{
	Free();
}


bool TFANConnectivityEncoder::GetCoordIndex(int pos, int * coordIndex) {
	if (pos < m_nTriangles) {
		for(int h = 0; h < 3; h++) {
			coordIndex[h] = m_triangles[pos*3+h];
		}
		return true;
	}
	return false;
}
void TFANConnectivityEncoder::AddNeighborVertex2Vertex(int v1, int v2)
{
	int found = 0;	
	for (IntVect::iterator posc = vertex2Vertex_[v1].begin(); posc != vertex2Vertex_[v1].end(); ++posc) {
		if ((*posc) == v2) {
			found = 1;
			break;
		}
	}
	if (found == 0)
		vertex2Vertex_[v1].push_back(v2);
}

void TFANConnectivityEncoder::ComputeVertex2Vertex(){
	vertex2Vertex_.resize(m_nVertices);
	for (int v = 0; v < m_nVertices; v++)
	{
		vertex2Vertex_[v].reserve(20);
	}
	for (int f = 0; f < m_nTriangles; f++) {
//		printf("%i \t %i \t %i\n", m_triangles[f*3+0], m_triangles[f*3+1], m_triangles[f*3+2]);
		AddNeighborVertex2Vertex(m_triangles[f*3+0], m_triangles[f*3+1]);
		AddNeighborVertex2Vertex(m_triangles[f*3+0], m_triangles[f*3+2]);

		AddNeighborVertex2Vertex(m_triangles[f*3+1], m_triangles[f*3+0]);
		AddNeighborVertex2Vertex(m_triangles[f*3+1], m_triangles[f*3+2]);

		AddNeighborVertex2Vertex(m_triangles[f*3+2], m_triangles[f*3+0]);
		AddNeighborVertex2Vertex(m_triangles[f*3+2], m_triangles[f*3+1]);
	}
}
void TFANConnectivityEncoder::AddNeighborVertex2Triangle(int v, int t)
{
	int found = 0;	
	for (IntVect::iterator posc = vertex2Triangle_[v].begin(); posc != vertex2Triangle_[v].end(); ++posc) {
		if ((*posc) == t) {
			found = 1;
			break;
		}
	}
	if (found == 0)
		vertex2Triangle_[v].push_back(t);
}

void TFANConnectivityEncoder::ComputeVertex2Triangle(){
	vertex2Triangle_.resize(m_nVertices);
	for (int v = 0; v < m_nVertices; v++)
	{
		vertex2Triangle_[v].reserve(20);
	}
	for (int f = 0; f < m_nTriangles; f++) {
		AddNeighborVertex2Triangle(m_triangles[f*3+0], f);
		AddNeighborVertex2Triangle(m_triangles[f*3+1], f);
		AddNeighborVertex2Triangle(m_triangles[f*3+2], f);
	}
}
void TFANConnectivityEncoder::AddNeighborTriangle2Triangle(int t1, int t2)
{
	int found = 0;	
	for (IntVect::iterator posc = triangle2Triangle_[t1].begin(); posc != triangle2Triangle_[t1].end(); ++posc) {
		if ((*posc) == t2) {
			found = 1;
			break;
		}
	}
	if (found == 0)
		triangle2Triangle_[t1].push_back(t2);
}

void TFANConnectivityEncoder::ComputeTriangle2Triangle(){
	triangle2Triangle_.resize(m_nTriangles);
	for (int t = 0; t < m_nTriangles; t++)
	{
		triangle2Triangle_[t].reserve(20);
	}
	int coordIndex[3] = {-1, -1, -1};
	for (int t = 0; t < m_nTriangles; t++) {
		GetCoordIndex(t, coordIndex);
		for (int k = 0; k < 3; k++) {
			for (IntVect::iterator pt0 = vertex2Triangle_[coordIndex[k]].begin(); pt0 != vertex2Triangle_[coordIndex[k]].end(); ++pt0){
				if ((*pt0) != t){
					int tr0 = (*pt0);
					for (int i=0; i <  (int) vertex2Triangle_[coordIndex[(k+1)%3]].size();i++){
						int tr1 = vertex2Triangle_[coordIndex[(k+1)%3]][i];
						if (tr1 == tr0){
							AddNeighborTriangle2Triangle(t, tr1);
						}
					}
				}
			}
		}
	}
}
void TFANConnectivityEncoder::Init(){
	tagsT_= new int[m_nTriangles];
	triangleMap_ = new int[m_nTriangles];
	for (int t = 0; t < m_nTriangles; t++) {
		tagsT_[t] = 0;
		triangleMap_[t] = -1;
	}
	vertexMap_ = new int[m_nVertices];
	tagsV_ = new int[m_nVertices];
	for (int t = 0; t < m_nVertices; t++) {
		tagsV_[t] = 0;
		vertexMap_[t] = -1;
	}
	triangleMapPos_ = new int[m_nTriangles];
	for (int t = 0; t < m_nTriangles; t++) {
		triangleMapPos_[t] = -1;
	}
}
void TFANConnectivityEncoder::Free(){
	if (tagsV_ != NULL) delete [] tagsV_;
	if (tagsT_ != NULL) delete [] tagsT_;
	if (vertexMap_ != NULL) delete [] vertexMap_;
	if (triangleMap_ != NULL) delete [] triangleMap_;
	if (triangleMapPos_ != NULL) delete [] triangleMapPos_;	
	tagsV_ = NULL;
	tagsT_ = NULL;
	vertexMap_ = NULL;
}
bool TFANConnectivityEncoder::IsCase0(int degree, std::vector<int> &ops, std::vector<int> &vertices){
	// ops: 1000001 vertices: -1 -2
	if ((vertices.size() != 2) || (degree < 2)) {
		return false;
	}
	if ((vertices[0] != -1) ||(vertices[1] != -2) || 
		(ops[0] != 1)       ||(ops[degree-1] != 1)  ) return false;
	for (int u = 1; u < degree-1; u++) {
		if (ops[u] != 0) return false;
	}
	return true;
}
bool TFANConnectivityEncoder::IsCase1(int degree, std::vector<int> &ops, std::vector<int> &vertices){
	// ops: 1xxxxxx1 vertices: -1 x x x x x -2
	if ((degree < 2) || (vertices.size()< 1)) {
		return false;
	}
	if ((vertices[0] != -1) ||(vertices[vertices.size()-1] != -2) || 
		(ops[0] != 1)       ||(ops[degree-1] != 1)  ) return false;
	return true;
}
bool TFANConnectivityEncoder::IsCase2(int degree, std::vector<int> &ops, std::vector<int> &vertices){
	// ops: 00000001 vertices: -1
	if ((degree < 2) || (vertices.size() != 1)) {
		return false;
	}
	if ((vertices[0] != -1) || (ops[degree-1] != 1)  ) return false;
	for (int u = 0; u < degree-1; u++) {
		if (ops[u] != 0) return false;
	}
	return true;
}
bool TFANConnectivityEncoder::IsCase3(int degree, std::vector<int> &ops, std::vector<int> &vertices){
	// ops: 00000001 vertices: -2
	if ((degree < 2) || (vertices.size() != 1)) {
		return false;
	}
	if ((vertices[0] != -2) || (ops[degree-1] != 1)  ) return false;
	for (int u = 0; u < degree-1; u++) {
		if (ops[u] != 0) return false;
	}
	return true;
}
bool TFANConnectivityEncoder::IsCase4(int degree, std::vector<int> &ops, std::vector<int> &vertices){
	// ops: 10000000 vertices: -1
	if ((degree < 2) || (vertices.size() != 1)) {
		return false;
	}
	if ((vertices[0] != -1) || (ops[0] != 1)  ) return false;
	for (int u = 1; u < degree; u++) {
		if (ops[u] != 0) return false;
	}
	return true;
}
bool TFANConnectivityEncoder::IsCase5(int degree, std::vector<int> &ops, std::vector<int> &vertices){
	// ops: 10000000 vertices: -2
	if ((degree < 2) || (vertices.size() != 1)) {
		return false;
	}
	if ((vertices[0] != -2) || (ops[0] != 1)  ) return false;
	for (int u = 1; u < degree; u++) {
		if (ops[u] != 0) return false;
	}
	return true;
}
bool TFANConnectivityEncoder::IsCase6(int degree, std::vector<int> &ops, std::vector<int> &vertices){
	// ops: 0000000 vertices: 
	if (vertices.size() != 0) {
		return false;
	}
	for (int u = 0; u < degree; u++) {
		if (ops[u] != 0) return false;
	}
	return true;
}
bool TFANConnectivityEncoder::IsCase7(int degree, std::vector<int> &ops, std::vector<int> &vertices){
	// ops: 1000001 vertices: -2 -1
	if ((vertices.size() != 2) || (degree < 2)) {
		return false;
	}
	if ((vertices[0] != -2) ||(vertices[1] != -1) || 
		(ops[0] != 1)       ||(ops[degree-1] != 1)  ) return false;
	for (int u = 1; u < degree-1; u++) {
		if (ops[u] != 0) return false;
	}
	return true;
}
bool TFANConnectivityEncoder::IsCase8(int degree, std::vector<int> &ops, std::vector<int> &vertices){
	// ops: 1xxxxxx1 vertices: -1 x x x x x -2
	if ((degree < 2) || (vertices.size()< 1)) {
		return false;
	}
	if ((vertices[0] != -2) ||(vertices[vertices.size()-1] != -1) || 
		(ops[0] != 1)       ||(ops[degree-1] != 1)  ) return false;
	return true;
}

//FILE * encoderLog = fopen("c:\\temp\\enc.txt", "w");

void TFANConnectivityEncoder::AddTriangles(int focusVertex, std::vector<int> &VL){
	int v = -1;
	int t = -1;
	int v0 = -1;

	// we get all free/conquested triangles adjacent to focusVertex
	// we get all conquested vertices adjacent to focusVertex
	std::map<int, int> conquestedVL;
	int connec[3] = {-1, -1, -1};
	std::vector<int> TL;
	for( int j =0; j != vertex2Triangle_[focusVertex].size(); j++){
		t = vertex2Triangle_[focusVertex][j];
		if ( tagsT_[t] == 0) {
			TL.push_back(t);
		}
		else {
			GetCoordIndex(t, connec);
			for (int k = 0; k != 3; k++){
				v = vertexMap_[connec[k]];
				if ((connec[k] != focusVertex) && (v > vertexMap_[focusVertex])) {// we get only the visited vertices
					conquestedVL[v] = connec[k];
				}
			}
		}
	}

	// sort the vertices by an incresing order of traversal
	IntVect sortedConquestedVL;
	for(std::map<int, int>::iterator it = conquestedVL.begin(); it!= conquestedVL.end(); it++){
		sortedConquestedVL.push_back(it->first);
	}

	//----------------------------------

	VectIntVect triangleFans;
	if (TL.size()!= 0) {
		// Compute trinagles adjacency
		IntMultiVect triangle2Triangle;
		int vertexNT[3] = {-1, -1, -1};
		int vertexT[3] = {-1, -1, -1};
		// triangle with minimum of connectivity
		for (int i = 0; i < (int) TL.size(); i++) {
			GetCoordIndex(TL[i], vertexT);
			for (int j = 0; j < (int) TL.size(); j++) {
				if (i!=j) {
					GetCoordIndex(TL[j], vertexNT);
					int found = 0;
					for (int k1 = 0; k1 < 3; k1++) {
						for (int k2 = 0; k2 < 3; k2++) {
							if (vertexNT[k1] == vertexT[k2]) found++;
						}
					}
					if ((found == 2)||(found == 3)) {
						int test = 0;
						for (int h = 0; h < (int) triangle2Triangle[TL[i]].size(); h++){
							if (triangle2Triangle[TL[i]][h] == TL[j]) test = 1;
						}
						if (test == 0) triangle2Triangle[TL[i]].push_back(TL[j]);
					}
				}
			}
		}
		
		// handle degenerated faces
		for (int i = 0; i < (int) TL.size(); i++) 
		{
			if (tagsT_[TL[i]] == 0) 
			{
				GetCoordIndex(TL[i], vertexT);
				if (vertexT[0] == vertexT[1] ||
					vertexT[1] == vertexT[2] ||
					vertexT[2] == vertexT[0] )
				{
					GetCoordIndex(TL[i], vertexT);
					tagsT_[TL[i]] = 1;
					IntVect vfan;
					int posFocusVertex = -1;
					for (int k = 0; k < 3; k++) {
						if (vertexT[k] == focusVertex) 
						{
							posFocusVertex = k;
							break;
						}
					}
					for (int k = 0; k < 3; k++) {
						if (k != posFocusVertex) vfan.push_back(vertexT[k]);
					}
					triangleFans.push_back(vfan);
					triangleMap_[TL[i]] = triangleCount_++;				
					triangleMapPos_[triangleMap_[TL[i]]] = posFocusVertex;
				}
			}
		}



		int tMin = 0;
		int connecMin = 0;
		int nextT = 0;
		int nextTOld = 0;
		while (tMin!=-1){
			tMin = -1;
			connecMin = 100000;
			for (int i = 0; i < (int) TL.size(); i++) {
				if (tagsT_[TL[i]] == 0) {
					if ( connecMin >  (int) triangle2Triangle[TL[i]].size()){
						connecMin = (int) triangle2Triangle[TL[i]].size();
						tMin = TL[i];
					}
				}
			}

			if (tMin != -1) {
				IntVect vfan;
				IntVect tfan;
				GetCoordIndex(tMin, vertexT);
				for (int k = 0; k < 3; k++) {
					if (vertexT[k] != focusVertex) vfan.push_back(vertexT[k]);
				}

				nextT = tMin;
				tagsT_[tMin] = 1; tfan.push_back(tMin);
				int nbrT = 0;
				int a = -1;
				int b = -1;
				do {
					nextTOld = nextT;
					nextT = -1;
					for (int h = 0; h < (int) triangle2Triangle[nextTOld].size(); h++) {
						if ( tagsT_[triangle2Triangle[nextTOld][h]] == 0) {
							GetCoordIndex(triangle2Triangle[nextTOld][h], vertexNT);
							if (nbrT == 0) {
								nextT = triangle2Triangle[nextTOld][h];
								tagsT_[nextT] = 1; tfan.push_back(nextT);
								for (int k = 0; k < 3; k++) {
									if (vertexNT[k] == vfan[0]) {
										a = vfan[1];
										b = vfan[0];
										break;
									}
									if (vertexNT[k] == vfan[1]) {
										b = vfan[1];
										a = vfan[0];
										break;
									}				
								}
								vfan.clear();
								vfan.push_back(a);
								vfan.push_back(b);
								nbrT++;
								int newV = -1;
								for (int k = 0; k < 3; k++) {
									if ((vertexNT[k] != b) && (vertexNT[k] != focusVertex)) {
										newV = vertexNT[k];
										break;
									}
								}
								a = b;
								b = newV;
								vfan.push_back(b);
								break;
							}
							else {
								int found = 0;
								for (int k = 0; k < 3; k++) {
									if (vertexNT[k] == b) {
										found = 1;
										break;
									}
								}
								if (found == 1) {
									nextT = triangle2Triangle[nextTOld][h];
									tagsT_[nextT] = 1;  tfan.push_back(nextT);
									int newV = -1;
									for (int k = 0; k < 3; k++) {
										if ((vertexNT[k] != b) && (vertexNT[k] != focusVertex)) {
											newV = vertexNT[k];
											break;
										}
									}
									a = b;
									b = newV;
									vfan.push_back(b);
								}
							}
						}
					}
				} while (nextT!=-1);
				if ( vfan.size() > 1) {

					int orientation = 1;
					for (int u=0; u!=3; u++) {
						if ((m_triangles[tfan[0]*3+u] == focusVertex) &&
							(m_triangles[tfan[0]*3+(u+1)%3] == vfan[0]) &&
							(m_triangles[tfan[0]*3+(u+2)%3] == vfan[1]))
						{
							orientation = 0;
						}
					}
					if (orientation == 0){
						triangleFans.push_back(vfan);
						for(int u = 0; u != tfan.size(); u++) {
							triangleMap_[tfan[u]] = triangleCount_++;				
							for (int x=0; x!=3; x++){
								if (m_triangles[tfan[u]*3+x] == focusVertex) 
									triangleMapPos_[triangleMap_[tfan[u]]] = x;
							}
						}
					}
					else {
						for(int u = static_cast<int>(tfan.size())-1; u != -1; u--) {
							triangleMap_[tfan[u]] = triangleCount_++;
							for (int x=0; x!=3; x++){
								if (m_triangles[tfan[u]*3+x] == focusVertex) 
									triangleMapPos_[triangleMap_[tfan[u]]] = x;
							}
						}
						IntVect vfanR;
						for(int i = (int) vfan.size()-1; i >=0 ; i--) {
							vfanR.push_back(vfan[i]);
						}
						triangleFans.push_back(vfanR);
					}
				}
			}
		}
	}

	nbrFans_.push_back((int) triangleFans.size());
	
//	printf("#fans %i\t", (int) triangleFans.size());
	
	if (triangleFans.size()!=0) {
		for(int f = 0; f != triangleFans.size(); f++) {
			degree_.push_back((int) triangleFans[f].size());
			IntVect vertices;
			IntVect ops;

			for(int k = 0; k != triangleFans[f].size(); k++) {
				v0 = triangleFans[f][k];
				if (tagsV_[v0] == 0) {
					ops.push_back(0);
					vertexMap_[v0] = vertexCount_++;
					VL.push_back(v0);
					sortedConquestedVL.push_back(vertexMap_[v0]);
					tagsV_[v0] = 1;
				}
				else {
					ops.push_back(1);
					int pos = 0;
					int found = 0;
					for(int u=0; u!= sortedConquestedVL.size(); u++){
						pos++;
						if (sortedConquestedVL[u] == vertexMap_[v0]) {
							found = 1;
							break;
						}
					}	
					if (found == 1){
						vertices.push_back(-pos);
					}
					else {
						vertices.push_back(vertexMap_[v0]-vertexMap_[focusVertex]);
					}
				}
			}

			//-----------------------------------------------
			int degree = degree_[degree_.size()-1];
			if (IsCase0(degree, ops, vertices)) { 
				// ops: 1000001 vertices: -1 -2
				cases_.push_back(0);
//				printf("Case 0\t");
			}
			else if (IsCase1(degree, ops, vertices)) {
				// ops: 1xxxxxx1 vertices: -1 x x x x x -2
				for(int u =1; u != degree-1; u++){
					ops_.push_back(ops[u]);
				}
				for(int u =1; u != vertices.size()-1; u++){
					vertices_.push_back(vertices[u]);
				}
				cases_.push_back(1);
//				printf("Case 1\t");
			}
			else if (IsCase2(degree, ops, vertices)) {
				// ops: 00000001 vertices: -1
				cases_.push_back(2);
//				printf("Case 2\t");
			}
			else if (IsCase3(degree, ops, vertices)) {
				// ops: 00000001 vertices: -2
				cases_.push_back(3);
//				printf("Case 3\t");
			}			
			else if (IsCase4(degree, ops, vertices)) {
				// ops: 10000000 vertices: -1
				cases_.push_back(4);
//				printf("Case 4\t");
			}
			else if (IsCase5(degree, ops, vertices)) {
				// ops: 10000000 vertices: -2
				cases_.push_back(5);
//				printf("Case 5\t");
			}			
			else if (IsCase6(degree, ops, vertices)) {
				// ops: 00000000 vertices:
				cases_.push_back(6);
//				printf("Case 6\t");
			}
			else if (IsCase7(degree, ops, vertices)) {
				// ops: 1000001 vertices: -1 -2
				cases_.push_back(7);
//				printf("Case 7\t");
			}
			else if (IsCase8(degree, ops, vertices)) {
				// ops: 1xxxxxx1 vertices: -2 x x x x x -1
				for(int u =1; u != degree-1; u++){
					ops_.push_back(ops[u]);
				}
				for(int u =1; u != vertices.size()-1; u++){
					vertices_.push_back(vertices[u]);
				}
				cases_.push_back(8);
//				printf("Case 8\t");
			}
			else {
				for(int u =0; u != degree; u++){
					ops_.push_back(ops[u]);
				}
				for(int u =0; u != vertices.size(); u++){
					vertices_.push_back(vertices[u]);
				}
				cases_.push_back(9);
//				printf("Case 9\t");
			}
/*
			fprintf(encoderLog, "v(%i)\t%i\t(", vertexMap_[focusVertex], degree_[degree_.size()-1]);
			for (int y = 0; y != ops.size(); y++){
				fprintf(encoderLog, "%i", ops[y]);
			}
			fprintf(encoderLog, ")\t(");

			for (int y = 0; y != vertices.size(); y++){
				fprintf(encoderLog, "%i, ", vertices[y]);
			}
			fprintf(encoderLog, ")\t(");
			for(int k = 0; k != triangleFans[f].size(); k++) {
				v0 = triangleFans[f][k];
				fprintf(encoderLog, "%i, ", vertexMap_[v0]);
			}
			fprintf(encoderLog, ")\n");
			fflush(encoderLog);
*/
		}
	}
	else {
//			printf("v(%i)\t%i\n", vertexMap_[focusVertex], 0);
	}
}


int TFANConnectivityEncoder::Encode(EncoderStreamInfo &streamInfo)
{
	ComputeVertex2Vertex();
	ComputeVertex2Triangle();
	ComputeTriangle2Triangle();

	maxV2T_ = 0;
	for (int v = 0; v != m_nVertices; v++){
		if (maxV2T_ < vertex2Triangle_[v].size()) maxV2T_ = (unsigned int) vertex2Triangle_[v].size();
	}
	Write2Stream(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize, &maxV2T_, sizeof(unsigned int));

	Init();
	vertexCount_ = 0;
	triangleCount_ = 0;
	for (int v = 0; v != m_nVertices; v++){
		if (tagsV_[v] == 0) {
			int v0 = v; 
			std::vector<int> VL; 
			VL.push_back(v0); tagsV_[v0] = 1; vertexMap_[v0] = vertexCount_++;
			while (VL.size() != 0) {
				v0 = VL[0];
				VL.erase(VL.begin());
				AddTriangles(v0, VL);
			}
		}
	}




	int size = 0;
	size += EncodeNTFans(streamInfo);
	size += EncodeDegrees(streamInfo);
	size += EncodeCases(streamInfo);
	size += EncodeVerticesIndices(streamInfo);
	size += EncodeOps(streamInfo);
	if (m_vertexOrderPres == 1) {
		size += EncodeVertexOrder(streamInfo);
	}
	if (m_triangleOrderPres == 1) {
		size += EncodeTriangleOrder(streamInfo);
	}
	if (m_trianglePermPres == 1) {
		size += EncodeTrianglePermutation(streamInfo);
	}

	return size+4;
}
int TFANConnectivityEncoder::EncodeNTFans(EncoderStreamInfo &streamInfo){
/*
	FILE * fp = fopen("NTFans.txt", "w");
	for (int v = 0; v < (int) nbrFans_.size(); v++) {
		fprintf(fp, "%i\n", nbrFans_[v]);
	}
	fclose(fp);
*/

	unsigned int NMAX = (int) nbrFans_.size()*8+100;

	unsigned char maxV = 0;
	for (int v = 0; v < (int) nbrFans_.size(); v++) {
		if (maxV < nbrFans_[v]) maxV = nbrFans_[v];
	}
	maxV++;
	
	Write2Stream(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize, &maxV, sizeof(unsigned char));

	Adaptive_Data_Model mModel(maxV);
	Arithmetic_Codec ace(NMAX, NULL);
	ace.start_encoder();

	for (int v = 0; v < (int) nbrFans_.size(); v++) {
		ace.encode(nbrFans_[v], mModel);
	}
	unsigned int size = ace.write_to_mem(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize)+1;
	return size;
}

int TFANConnectivityEncoder::EncodeDegrees(EncoderStreamInfo &streamInfo){
/*
	FILE * fp = fopen("Degrees.txt", "w");
	for (int v = 0; v < (int) degree_.size(); v++) {
		fprintf(fp, "%i\n", degree_[v]);
	}
	fclose(fp);
*/

	unsigned int nbins = (unsigned int) degree_.size();
	Write2Stream(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize, &nbins, sizeof(unsigned int));

	unsigned int NMAX = (int) degree_.size() * 8+100;

	unsigned int maxV = 0;
	for (int v = 0; v < (int) degree_.size(); v++) {
		if (maxV < (unsigned int) degree_[v]) maxV = degree_[v];
	}
	maxV++;
	Write2Stream(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize, &maxV, sizeof(unsigned int));

//	printf("%i \n", maxV);

	Adaptive_Data_Model mModel(maxV);
	Arithmetic_Codec ace(NMAX, NULL);
	ace.start_encoder();

	for (int v = 0; v < (int) degree_.size(); v++) {
		ace.encode(degree_[v], mModel);
	}
	unsigned int size = ace.write_to_mem(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize) + 8;
	return size;
}

int TFANConnectivityEncoder::EncodeCases(EncoderStreamInfo &streamInfo){
	unsigned int NMAX = (int) cases_.size() * 8+100;
	Adaptive_Data_Model mModel(10);
	Arithmetic_Codec ace(NMAX, NULL);
	ace.start_encoder();
	for (int v = 0; v < (int) cases_.size(); v++) {
		ace.encode(cases_[v], mModel);
	}
	unsigned int size = ace.write_to_mem(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize);
	return size;
}

int TFANConnectivityEncoder::EncodeVerticesIndices(EncoderStreamInfo &streamInfo){
	unsigned int size = streamInfo.m_compressedStreamSize;
	unsigned int nVertices = static_cast<unsigned int>(vertices_.size());
	Write2Stream(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize, &nVertices, sizeof(unsigned int));
	if (nVertices > 0)
	{
		EncodeIntArray(streamInfo, static_cast<int *>(&vertices_[0]), static_cast<int>(vertices_.size()), 1, 1, 4,0, static_cast<int>(vertices_.size()));
	}
	return (static_cast<int>(streamInfo.m_compressedStreamSize - size));
}

int TFANConnectivityEncoder::EncodeOps(EncoderStreamInfo &streamInfo){
	unsigned int size = 0;
	unsigned int nbins = (unsigned int) ops_.size();
	Write2Stream(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize, &nbins, sizeof(unsigned int));

	size += 4;

	if (nbins > 0) {
		unsigned int NMAX = (int) ops_.size()+100;
		Adaptive_Bit_Model bModel;
		Arithmetic_Codec ace(NMAX, NULL);
		ace.start_encoder();
		for (int v = 0; v < (int) ops_.size(); v++) {
			ace.encode( ops_[v], bModel);
		}
		size += ace.write_to_mem(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize);
	}
	return size;
}


int TFANConnectivityEncoder::EncodeTriangleOrder(EncoderStreamInfo &streamInfo){
	int * triangleMapR = new int[m_nTriangles];
	for (int v = 0; v < m_nTriangles; v++) {
		triangleMapR[triangleMap_[v]] = v;
	}
	unsigned int size = streamInfo.m_compressedStreamSize;
	EncodeIntArray(streamInfo, triangleMapR, m_nTriangles, 1, 1, 4, 0, m_nTriangles);
	delete [] triangleMapR;
	return (static_cast<int>(streamInfo.m_compressedStreamSize - size));
}
int TFANConnectivityEncoder::EncodeVertexOrder(EncoderStreamInfo &streamInfo){
	int * vertexMapR = new int[m_nVertices];
	for (int v = 0; v < m_nVertices; v++) {
		vertexMapR[vertexMap_[v]] = v;
	}
	unsigned int size = streamInfo.m_compressedStreamSize;
	EncodeIntArray(streamInfo, vertexMapR, m_nVertices, 1, 1, 4, 0, m_nVertices);
	delete [] vertexMapR;
	return (static_cast<int>(streamInfo.m_compressedStreamSize - size));
}
int TFANConnectivityEncoder::EncodeTrianglePermutation(EncoderStreamInfo &streamInfo){
	unsigned int NMAX = (int) cases_.size() * 8+100;
	Adaptive_Data_Model mModel(3);
	Arithmetic_Codec ace(NMAX, NULL);
	ace.start_encoder();
	for (int v = 0; v < m_nTriangles; v++) {
		ace.encode(triangleMapPos_[v], mModel);
	}
	unsigned int size = ace.write_to_mem(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize);
	return size;
}

void TFANConnectivityEncoder::ReOrderTab(float * tab, int D, int nVertices){
	float * tabR = new float [D * nVertices];
	for (int v = 0; v != nVertices; v++){
		for (int d = 0; d != D; d++) {
			tabR[vertexMap_[v]*D+d] = tab[v*D+d];
		}
	}
	for (int v = 0; v != nVertices; v++){
		for (int d = 0; d != D; d++) {
			tab[v*D+d] = tabR[v*D+d];
		}
	}
	delete [] tabR;
}
void TFANConnectivityEncoder::ReOrderTab(int * tab, int D, int nTriangles){
	int * tabR = new int [D * nTriangles];
	for (int v = 0; v != nTriangles; v++){
		for (int d = 0; d != D; d++) {
			tabR[triangleMap_[v]*D+d] = tab[v*D+d];
		}
	}
	for (int v = 0; v != nTriangles; v++){
		for (int d = 0; d != D; d++) {
			tab[v*D+d] = tabR[v*D+d];
		}
	}
	delete [] tabR;
}
void TFANConnectivityEncoder::ReOrderConnec(int * coordIndex, int nCoordIndex){
	for (int t = 0; t != nCoordIndex; t++){
		for (int i = 0; i != 3; i++) {
			coordIndex[t*3+i] = vertexMap_[coordIndex[t*3+i]];
		}
	}
}
void TFANConnectivityEncoder::ReOrderPerm(int * coordIndex, int nCoordIndex){
	int coord[3];
	for (int t = 0; t != nCoordIndex; t++){
		for (int i = 0; i != 3; i++) {
			coord[i] = coordIndex[t*3+(i + triangleMapPos_[t]) % 3];
		}
		for (int i = 0; i != 3; i++) {
			coordIndex[t*3+i] = coord[i];
		}
	}
}