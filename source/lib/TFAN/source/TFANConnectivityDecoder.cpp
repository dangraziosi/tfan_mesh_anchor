//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom 2008, 2009
// Licensed Materials, Program Property of Institut Telecom
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// retains full right to use the code for his/her own purpose, assign or donate 
// the code to a third party and to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//
//
//    http://www-artemis.it-sudparis.eu/
//    
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//
//-------------------------------------------------------------------------

#include "TFANConnectivityDecoder.h"
#include "Arithmetic_Codec.h"
#include "BinaryIO.h"
#include "MathTools.h"
//#include "QBCR_Decoder.h"

TFANConnectivityDecoder::TFANConnectivityDecoder(void)
{
	m_triangles = NULL;
	m_nTriangles = 0;
	m_nVertices = 0;
	vertexCount_ = 0;
	vertexMapR_ = NULL;
	triangleMapR_ = NULL;
	triangleMapPos_ = NULL;
	nbrFans_ = NULL;
	degree_  = NULL;
	cases_ = NULL;
	vertices_ = NULL;
	ops_ = NULL;
	nNbrFans_=0;
	nDegree_=0;
	nCases_=0;
	nVertices_=0;
	nOps_=0;

	tfans_ = NULL;
	vertex2Triangle_ = NULL;
	maxV2T_ = 0;
	m_vertexOrderPres = 0;
	m_triangleOrderPres = 0;
	m_trianglePermPres = 0;
}

TFANConnectivityDecoder::~TFANConnectivityDecoder(void)
{
	if (vertexMapR_!=NULL) delete [] vertexMapR_;
	if (triangleMapR_!=NULL) delete [] triangleMapR_;
	if (triangleMapPos_ != NULL) delete [] triangleMapPos_;	
	if (nbrFans_!=NULL) delete [] nbrFans_;
	if (degree_!=NULL) delete [] degree_;
	if (cases_!=NULL) delete [] cases_;
	if (vertices_!=NULL) delete [] vertices_;
	if (ops_!=NULL) delete [] ops_;
	if (tfans_!=NULL) delete tfans_;
	if (vertex2Triangle_!=NULL) delete vertex2Triangle_;
}
void TFANConnectivityDecoder::FreeInt(int ** tab, int dim)
{
	if (tab) {
		for (int i = 0; i < dim; i++) {
			delete [] tab[i];
		}
		delete [] tab;
	}
}
bool TFANConnectivityDecoder::GetCoordIndex(int pos, int * coordIndex) {
	if (pos < m_nTriangles) {
		for(int h = 0; h < 3; h++) {
			coordIndex[h] = m_triangles[pos*3+h];
		}
		return true;
	}
	return false;
}

int TFANConnectivityDecoder::Decode(DecoderStreamInfo &streamInfo){
	triangleCount_ = 0;
	vertexCount_ = 0;
	ReadFromStream(streamInfo.m_compressedStream, &maxV2T_, sizeof(unsigned int));
	DecodeNTFans(streamInfo);
	DecodeDegrees(streamInfo);
	DecodeCases(streamInfo);
	DecodeVerticesIndices(streamInfo);
	DecodeOps(streamInfo);
	if (m_vertexOrderPres == 1) {
		DecodeVertexOrder(streamInfo);
	}
	if (m_triangleOrderPres == 1) {
		DecodeTriangleOrder(streamInfo);
	}
	if (m_trianglePermPres == 1) {
		DecodeTrianglePermutation(streamInfo);
	}	
	Decode();
	return 0;
}
void TFANConnectivityDecoder::ReadStream(IntVect & nbrFans, IntVect & degree, IntVect & cases, 
									 IntVect & vertices, IntVect & ops,
									 int * coordIndex, int nCoordIndex, int nCoord,
									 unsigned int maxV2T)
{
	maxV2T_ = maxV2T;
	m_triangles = coordIndex;
	m_nTriangles = nCoordIndex;
	m_nVertices = nCoord;

	triangleCount_ = 0;
	vertexCount_ = 0;
	
	nNbrFans_ = (int) nbrFans.size();
	nbrFans_ = new int [nNbrFans_];
	for (int v = 0; v < nNbrFans_; v++) {
		nbrFans_[v] = nbrFans[v];
	}

	nDegree_ = (int) degree.size();
	degree_ = new int [nDegree_];
	for (int v = 0; v < nDegree_; v++) {
		degree_[v] = degree[v];
	}

	nCases_ = (int) cases.size();
	cases_ = new int [nCases_];
	for (int v = 0; v < nCases_; v++) {
		cases_[v] = cases[v];
	}

	nVertices_ = (int) vertices.size();
	vertices_ = new int [nVertices_];
	for (int v = 0; v < nVertices_; v++) {
		vertices_[v] = vertices[v];
	}

	nOps_ = (int) ops.size();
	ops_ = new int [nOps_];
	for (int v = 0; v < nOps_; v++) {
		ops_[v] = ops[v];
	}
}
/*
FILE * decoderLog = fopen("c:\\temp\\dec.txt", "w");
*/
void TFANConnectivityDecoder::Decode(){
//	logger.write_2_log("Decoded\n");

	int iNbrFans=0;
	int iDegree=0;
	int iCases=0;
	int iVertices=0;
	int iOps=0;


	int degree = 0;
	int cas = 0;
	int u =0;
	int nfans = 0;
	int j = 0;
	int v = 0;
	int k = 0;
	int f = 0;
	int p = 0;
	int iV = 0;

	int L[1000];
	int VL[1000];

	int nL=0;
	int nVL=0;

	int i = 0;
	int value = 0;
	int nbr = 0;
	int Number_FANS = 0;
	int MAX_TFANS_DIM=0;
	int pos=0;
	for (i = 0; i < nNbrFans_; i++) {
		Number_FANS += nbrFans_[i];
		for(f = 0; f < nbrFans_[i]; f++){	
			if (MAX_TFANS_DIM < degree_[pos]+1) MAX_TFANS_DIM = degree_[pos]+1;
			pos++;
		}
	}

	tfans_ = new MultiVectOpt(Number_FANS, MAX_TFANS_DIM);
	vertex2Triangle_ = new MultiVectOpt(m_nVertices, maxV2T_);

//	logger.write_2_log("\nMAX_TFANS_DIM %i\n", MAX_TFANS_DIM);
//	logger.write_2_log("MAX_V2T_DIM %i\n", maxV2T_);

	int iFans = 0;
	int rootT = 0;

	int b = 0;
	int c = 0;

//	logger.write_2_log("Decoder\n");
	for(int focusVertex = 0; focusVertex!= m_nVertices; focusVertex++){
		
		if (focusVertex == vertexCount_) vertexCount_++; // insert focusVertex
		nfans = nbrFans_[iNbrFans++];

		// we get all conquested vertices adjacent to focusVertex
		nL = 0;
		for(j =0; j != vertex2Triangle_->GetSize(focusVertex); j++){
			int t = vertex2Triangle_->GetElement(focusVertex,j);
			for (k = 0; k != 3; k++){
				v = m_triangles[t*3+k];
				if ((v != focusVertex)&& (v > focusVertex)) {
					L[nL] = v;
					nL++;
				}
			}
		}
		//-----------Sorting L---------------------
		for(i = 1; i < nL; i++) {
			value = L[i];
			j = i-1;
			while ((j >= 0) && (L[j] > value)) {
				L[j + 1] = L[j];
                j = j-1;
                L[j+1] = value;
			}
		}
		// we eliminate the repetition
		nVL = 0;	
		if (nL > 0) {
			VL[nVL] = L[0];
			L[nL] = -1;
			for(i=1; i<=nL; i++){	
				if (L[i]!=VL[nVL]){
					nVL++;
					VL[nVL] = L[i];
				}
			}
		}
		//----------------------------------
//		logger.write_2_log("%i \t nfans=%i \t (", focusVertex, nfans);
//		for (int x = 0; x < nVL; x++) {
//			logger.write_2_log("%i, ", VL[x]);
//		}
//		logger.write_2_log(")\n");
		//----------------------------------
		
		for(f = 0; f != nfans; f++){	
			degree = degree_[iDegree++];
			cas = cases_[iCases++];
			tfans_->Push(iFans, focusVertex);
				
			if (cas == 0) { // ops: 1000001 vertices: -1 -2
				tfans_->Push(iFans, VL[0]);
				for(u =1; u != degree-1; u++){
					VL[nVL++] = vertexCount_;
					tfans_->Push(iFans, vertexCount_++);
				}
				tfans_->Push(iFans, VL[1]);
			}
			else if (cas == 1) { // ops: 1xxxxxx1 vertices: -1 x x x x x -2
				tfans_->Push(iFans, VL[0]);
				for(u =1; u != degree-1; u++){
					if ( ops_[iOps++] == 1) {
						v = vertices_[iVertices++];
						if ( v < 0) {
							tfans_->Push(iFans, VL[-v-1]);
						}
						else {
							tfans_->Push(iFans, v + focusVertex);
						}
					}
					else {
						VL[nVL++] = vertexCount_;
						tfans_->Push(iFans, vertexCount_++);
					}
				}
				tfans_->Push(iFans, VL[1]);
			}
			else if (cas == 2) { // ops: 00000001 vertices: -1
				for(u =0; u != degree-1; u++){
					VL[nVL++] = vertexCount_;
					tfans_->Push(iFans, vertexCount_++);
				}
				tfans_->Push(iFans, VL[0]);			
			}
			else if (cas == 3) { // ops: 00000001 vertices: -2
				for(u =0; u != degree-1; u++){
					VL[nVL++] = vertexCount_;
					tfans_->Push(iFans, vertexCount_++);
				}
				tfans_->Push(iFans, VL[1]);		
			}			
			else if (cas == 4) {// ops: 10000000 vertices: -1
				tfans_->Push(iFans, VL[0]);
				for(u =1; u != degree; u++){
					VL[nVL++] = vertexCount_;
					tfans_->Push(iFans, vertexCount_++);
				}
			}
			else if (cas == 5) {// ops: 10000000 vertices: -2
				tfans_->Push(iFans, VL[1]);
				for(u =1; u != degree; u++){
					VL[nVL++] = vertexCount_;
					tfans_->Push(iFans, vertexCount_++);
				}
			}			
			else if (cas == 6) {// ops: 00000000 vertices:
				for(u =0; u != degree; u++){
					VL[nVL++] = vertexCount_;
					tfans_->Push(iFans, vertexCount_++);
				}
			}
			if (cas == 7) { // ops: 1000001 vertices: -2 -1
				tfans_->Push(iFans, VL[1]);
				for(u =1; u != degree-1; u++){
					VL[nVL++] = vertexCount_;
					tfans_->Push(iFans, vertexCount_++);
				}
				tfans_->Push(iFans, VL[0]);
			}
			else if (cas == 8) { // ops: 1xxxxxx1 vertices: -2 x x x x x -1
				tfans_->Push(iFans, VL[1]);
				for(u =1; u != degree-1; u++){
					if ( ops_[iOps++] == 1) {
						v = vertices_[iVertices++];
						if ( v < 0) {
							tfans_->Push(iFans, VL[-v-1]);
						}
						else {
							tfans_->Push(iFans, v + focusVertex);
						}
					}
					else {
						VL[nVL++] = vertexCount_;
						tfans_->Push(iFans, vertexCount_++);
					}
				}
				tfans_->Push(iFans, VL[0]);
			}
			else if (cas == 9) {// general case
				for(u =0; u != degree; u++){
					if ( ops_[iOps++] == 1) {
						v = vertices_[iVertices++];
						if ( v < 0) {
							tfans_->Push(iFans, VL[-v-1]);
						}
						else {
							tfans_->Push(iFans, v + focusVertex);
						}
					}
					else {
						VL[nVL++] = vertexCount_;
						tfans_->Push(iFans, vertexCount_++);
					}
				}
			}
			//logger.write_2_log("\t degree=%i \t cas = %i\n", degree, cas);

/*
			fprintf(decoderLog, "v(%i)\t%i\t(", focusVertex, degree);
			for(int x = 1; x != tfans_->GetSize(iFans); x++) {
				int v0 = tfans_->GetElement(iFans, x);
				fprintf(decoderLog, "%i, ", v0);
			}
			fprintf(decoderLog, ")\n");
			fflush(decoderLog);
*/

			b = tfans_->GetElement(iFans, 1);
			for (k = 2; k<tfans_->GetSize(iFans); k++){
				c = tfans_->GetElement(iFans, k);
				rootT = triangleCount_*3;
				 
				m_triangles[rootT++] = focusVertex;
				m_triangles[rootT++] = b;
				m_triangles[rootT] = c;
/*
				printf("%i\t(%i\t%i\t%i) \n", triangleCount_,
					m_triangles[triangleCount_*3], 
					m_triangles[triangleCount_*3+1], 
					m_triangles[triangleCount_*3+2]); 
*/
				vertex2Triangle_->Push(focusVertex,triangleCount_);
				if (b != focusVertex) vertex2Triangle_->Push(b,triangleCount_);
				if (c != focusVertex && c != b) vertex2Triangle_->Push(c,triangleCount_);
				b=c;
				triangleCount_++;
			}
			iFans++;
		}
	}
	m_nTriangles = triangleCount_;
}
int TFANConnectivityDecoder::DecodeNTFans(DecoderStreamInfo &streamInfo){
	unsigned char maxV = 0;
	ReadFromStream(streamInfo.m_compressedStream, &maxV, sizeof(unsigned char));

	unsigned int code_bytes = 0;
	ReadFromStream(streamInfo.m_compressedStream, &code_bytes, sizeof(unsigned int));

	unsigned char * code_buffer = new unsigned char [code_bytes];                                                   
	ReadFromStream(streamInfo.m_compressedStream, code_buffer, sizeof(unsigned char)*code_bytes);

	Adaptive_Data_Model mModel(maxV);
	Arithmetic_Codec acd(code_bytes, code_buffer);
	acd.start_decoder();	// initialize decoder

	nNbrFans_ = m_nVertices;
	nbrFans_ = new int [nNbrFans_];
	for (int v = 0; v < m_nVertices; v++) {
		nbrFans_[v] = acd.decode(mModel);
	}
	delete [] code_buffer;
	return 0;
}
int TFANConnectivityDecoder::DecodeDegrees(DecoderStreamInfo &streamInfo){
	unsigned int nbins = 0;
	ReadFromStream(streamInfo.m_compressedStream, &nbins, sizeof(unsigned int));
	
	unsigned int maxV = 0;
	ReadFromStream(streamInfo.m_compressedStream, &maxV, sizeof(unsigned int));

	unsigned int code_bytes = 0;
	ReadFromStream(streamInfo.m_compressedStream, &code_bytes, sizeof(unsigned int));

	unsigned char * code_buffer = new unsigned char [code_bytes];                                                   
	ReadFromStream(streamInfo.m_compressedStream, code_buffer, sizeof(unsigned char)*code_bytes);
	

	Adaptive_Data_Model mModel(maxV);
	Arithmetic_Codec acd(code_bytes, code_buffer);
	acd.start_decoder();	// initialize decoder

	nDegree_ = nbins;
	degree_= new int [nDegree_];

	for (int v = 0; v < (int) nbins; v++) {
		degree_[v]=acd.decode(mModel);
	}
	delete [] code_buffer;
	return 0;
}
int TFANConnectivityDecoder::DecodeCases(DecoderStreamInfo &streamInfo){
	unsigned int code_bytes = 0;
	ReadFromStream(streamInfo.m_compressedStream, &code_bytes, sizeof(unsigned int));

	unsigned char * code_buffer = new unsigned char [code_bytes];                                                   
	ReadFromStream(streamInfo.m_compressedStream, code_buffer, sizeof(unsigned char)*code_bytes);
	

	Adaptive_Data_Model mModel(10);
	Arithmetic_Codec acd(code_bytes, code_buffer);
	acd.start_decoder();	// initialize decoder

	nCases_ = nDegree_;
	cases_ = new int [nCases_];

	for (int v = 0; v < nDegree_; v++) {
		cases_[v]=acd.decode(mModel);
	}
	delete [] code_buffer;
	return 0;
}
int TFANConnectivityDecoder::DecodeVerticesIndices(DecoderStreamInfo &streamInfo){
	unsigned int nVertices = 0;
	ReadFromStream(streamInfo.m_compressedStream, &nVertices, sizeof(unsigned int));
	nVertices_ = nVertices;
	vertices_ = new int [nVertices_];
	int predModeQBCR = -1;
	int binarizationModeQBCR = -1;
	if (nVertices > 0)
	{
		DecodeIntArray(streamInfo, nVertices_, vertices_, nVertices_, 1, predModeQBCR, binarizationModeQBCR, 0);
	}
	return 0;
}

int TFANConnectivityDecoder::DecodeOps(DecoderStreamInfo &streamInfo){
	unsigned int nbins = 0;
	ReadFromStream(streamInfo.m_compressedStream, &nbins, sizeof(unsigned int));
	if (nbins > 0) {
		unsigned int code_bytes = 0;
		ReadFromStream(streamInfo.m_compressedStream, &code_bytes, sizeof(unsigned int));

		unsigned char * code_buffer = new unsigned char [code_bytes];                                                   
		ReadFromStream(streamInfo.m_compressedStream, code_buffer, sizeof(unsigned char)*code_bytes);
	
		Adaptive_Bit_Model bModel;
		Arithmetic_Codec acd(code_bytes, code_buffer);
		acd.start_decoder();	// initialize decoder

		nOps_ = (int) nbins;
		ops_ = new int [nOps_];
	
		for (int v = 0; v < (int) nbins; v++) {
			ops_[v]=acd.decode(bModel);
		}
		delete [] code_buffer;
	}
	return 0;
}

int TFANConnectivityDecoder::DecodeVertexOrder(DecoderStreamInfo &streamInfo){
	vertexMapR_ = new int [m_nVertices];
	int predModeQBCR = -1;
	int binarizationModeQBCR = -1;
	DecodeIntArray(streamInfo, m_nVertices, vertexMapR_, m_nVertices, 1, predModeQBCR, binarizationModeQBCR, 0);
	return 0;
}

int TFANConnectivityDecoder::DecodeTriangleOrder(DecoderStreamInfo &streamInfo){
	triangleMapR_ = new int [m_nTriangles];
	int predModeQBCR = -1;
	int binarizationModeQBCR = -1;
	DecodeIntArray(streamInfo, m_nTriangles, triangleMapR_, m_nTriangles, 1, predModeQBCR, binarizationModeQBCR, 0);
	return 0;
}

int TFANConnectivityDecoder::DecodeTrianglePermutation(DecoderStreamInfo &streamInfo){
	triangleMapPos_ = new int [m_nTriangles];
	unsigned int code_bytes = 0;
	ReadFromStream(streamInfo.m_compressedStream, &code_bytes, sizeof(unsigned int));

	unsigned char * code_buffer = new unsigned char [code_bytes];                                                   
	ReadFromStream(streamInfo.m_compressedStream, code_buffer, sizeof(unsigned char)*code_bytes);
	

	Adaptive_Data_Model mModel(3);
	Arithmetic_Codec acd(code_bytes, code_buffer);
	acd.start_decoder();	// initialize decoder

	for (int v = 0; v < m_nTriangles; v++) {
		triangleMapPos_[v]=acd.decode(mModel);
	}
	delete [] code_buffer;
	return 0;
}

void TFANConnectivityDecoder::ReOrderTab(float * tab, int D, int nVertices){
	float * tabR = new float [D * nVertices];
	for (int v = 0; v != nVertices; v++){
		for (int d = 0; d != D; d++) {
			tabR[vertexMapR_[v]*D+d] = tab[v*D+d];
		}
	}
	for (int v = 0; v != nVertices; v++){
		for (int d = 0; d != D; d++) {
			tab[v*D+d] = tabR[v*D+d];
		}
	}
	delete [] tabR;
}
void TFANConnectivityDecoder::ReOrderTab(int * tab, int D, int nTriangles){
	int * tabR = new int [D * nTriangles];
	for (int v = 0; v != nTriangles; v++){
		for (int d = 0; d != D; d++) {
			tabR[triangleMapR_[v]*D+d] = tab[v*D+d];
		}
	}
	for (int v = 0; v != nTriangles; v++){
		for (int d = 0; d != D; d++) {
			tab[v*D+d] = tabR[v*D+d];
		}
	}
	delete [] tabR;
}
void TFANConnectivityDecoder::ReOrderConnec(int * coordIndex, int nCoordIndex){
	for (int t = 0; t != nCoordIndex; t++){
		for (int i = 0; i != 3; i++) {
			coordIndex[t*3+i] = vertexMapR_[coordIndex[t*3+i]];
		}
	}
}
void TFANConnectivityDecoder::ReOrderPerm(int * coordIndex, int nCoordIndex){
	int coord[3];
	for (int t = 0; t != nCoordIndex; t++){
		for (int i = 0; i != 3; i++) {
			coord[i] = coordIndex[t*3+(i - triangleMapPos_[t] + 3) % 3];
		}
		for (int i = 0; i != 3; i++) {
			coordIndex[t*3+i] = coord[i];
		}
	}
}
