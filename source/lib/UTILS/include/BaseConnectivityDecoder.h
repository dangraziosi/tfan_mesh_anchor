#pragma once
#include "utils.h"
class BaseConnectivityDecoder
{
protected:
	int m_nTriangles;
	int m_nVertices;
	int * m_triangles;


public:

	// This funtion reads the header an initializes m_nTriangles and m_nVertices
	inline void SetTriangleList(int * triangles){ m_triangles = triangles;}
	inline void SetNTriangles(int nTriangles){ m_nTriangles = nTriangles;}
	inline void SetNVertices(int nVertices){ m_nVertices = nVertices;}



	// This function decodes the list of triangles
	// It returns the list as int * 
	// Rq.: no memory allocation is done inside this function. The user should allocate and free the memory. 
	virtual int Decode(DecoderStreamInfo &streamInfo) = 0;

	BaseConnectivityDecoder(void):m_nTriangles(0), m_nVertices(0), m_triangles(NULL){};
	~BaseConnectivityDecoder(void){};
};

