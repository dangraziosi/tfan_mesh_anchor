#ifndef AC_HEADER
#define AC_HEADER

#include <stdio.h>
#include <vector>
#include <string.h>


class ac_encoder 
{
public:
	//FILE *fp;
	std::vector<int> * fp_buffer;
	long low;
	long high;
	long fbits;
	int buffer;
	int bits_to_go;
	long total_bits;
	int symbol;
};

class ac_decoder 
{
public:
	FILE *fp;
	long value;  //뽑아낸다..
	long low;
	long high;
	int buffer;
	int bits_to_go;
	int garbage_bits;
};

class ac_model
{
public: 
	int nsym;  //심볼수.....
	int *freq;  //빈도..
	int *cfreq; //토탈 빈도
	int adapt;//????
};

void ac_encoder_init (ac_encoder *, std::vector<int> * );
void ac_encoder_done (ac_encoder *);
void ac_decoder_init (ac_decoder *, const char *);
void ac_decoder_init (ac_decoder *, std::vector<int> *);
void ac_decoder_done (ac_decoder *);
void ac_model_init (ac_model *, int, int *, int);
void ac_model_done (ac_model *);
long ac_encoder_bits (ac_encoder *);
void ac_encode_symbol (ac_encoder *, ac_model *, int);
int ac_decode_symbol (ac_decoder *, ac_model *);
/*static */ void update_model (ac_model *, int);

//ory DMLAB 
//value값을 length의 길이만큼 비트를 생성한 뒤에 비트스트림에 써주는 함수
int ac_encode_bits(ac_encoder *ace, ac_model *acm, int value, int length);
int ac_decode_bits(ac_decoder *acd, ac_model *acm, int length);
//~ory DMLAB
#endif
