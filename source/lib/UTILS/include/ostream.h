// ostream.h: interface for the ostream class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(_OSTREAM_H_)
#define _OSTREAM_H_

//#include <stdlib.h>
//#include <stdio.h>
//#include <string.h>
//#include <e32base.h>
//#include <e32std.h>
//#include <e32cons.h> 
//#include <malloc.h>
//#include <memory.h>

namespace BIFS
{

	class ostream  
	{
	protected:
		char* _buffer;
		unsigned int _buffSize;
		unsigned int _pos;
		unsigned int _size;
		unsigned int _delta;

		void EnlargeBuffer();

	public:
		ostream();
		
		void GetData(char** buff,int &size);
		virtual ~ostream();

		void write(const char* buff,unsigned int size);
		void write( int i, unsigned int size );
		void write( float f );
		int readInt(unsigned int size);
		float readFloat(unsigned int size);
		void moveForward(unsigned int size);
		void startBuffer();
		unsigned int tellp(){ return _pos;};
		void seekp(unsigned int position);		
	};

}

#endif // !defined(_OSTREAM_H_)
