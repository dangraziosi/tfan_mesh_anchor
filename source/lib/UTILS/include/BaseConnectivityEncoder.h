#pragma once
#include "utils.h"

class BaseConnectivityEncoder
{
protected:
	int * m_triangles;
	int m_nTriangles;
	int m_nVertices;
public:
	inline void SetTriangleList(int * triangles){ m_triangles = triangles;}
	inline void SetNTriangles(int nTriangles){ m_nTriangles = nTriangles;}
	inline void SetNVertices(int nVertices){ m_nVertices = nVertices;}

	// This function is called before encode
	// It should return the nombre of bytes to be allocated for the compressed stream
	virtual unsigned int GetEstimatedCompressedStreamSize() = 0;

	// This function encodes the list of triangles
	// It returns the compressed stream as a char * and the size of the memory space used to store the compressed stream
	// Rq.: no memory allocation is done inside this function. The user should allocate and free the memory. 
	virtual int Encode(EncoderStreamInfo &streamInfo) = 0;

	BaseConnectivityEncoder(void):m_triangles(NULL), m_nTriangles(0), m_nVertices(0){};
	~BaseConnectivityEncoder(void){};
};
