#pragma once
#include "utils.h"
class BaseGeometryEncoder
{
protected:
	int m_nVertices;
	float * m_geometry;
	int m_dim;
	int m_nQBits;
public:
	inline void SetGeometryList(float * geometry){ m_geometry = geometry;}
	inline void SetNVertices(int nVertices){ m_nVertices = nVertices;}
	inline void SetNQuantizationBits(int nQBits){ m_nQBits = nQBits;}
	inline void SetDim(int dim){ m_dim = dim;}

	// This function is called before encode
	// It should return the nombre of bytes to be allocated for the compressed stream
	virtual unsigned int GetEstimatedCompressedStreamSize() = 0;

	// This function encodes the geometry represented as m_dim times m_nVertices array 
	// It returns the compressed stream as a char * and the size of the memory space used to store the compressed stream
	// Rq.: no memory allocation is done inside this function. The user should allocate and free the memory. 
	virtual int Encode(EncoderStreamInfo &streamInfo) = 0;

	BaseGeometryEncoder(void):m_geometry(NULL), m_nVertices(0), m_dim(3), m_nQBits(12){};
	~BaseGeometryEncoder(void){};
};
