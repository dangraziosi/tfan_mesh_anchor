#pragma once

class MultiVectOpt{
public:

	int * tab_;
	int * size_;
	int V_;
	int D_;

	inline int GetV() { return V_; };
	inline int GetD() { return D_; };

	inline int GetSize(int v) {return size_[v];};

	inline int GetElement(int v, int k) { return tab_[v*D_+k]; };

	inline void Push (int v, int v2) { tab_[v * D_ + size_[v]]= v2; size_[v]++;};
	inline int Find (int v, int v2) {
		int r = v*D_;
		for (int k = 0; k < size_[v]; k++) if (tab_[r+k] == v2) return k;
		return -1;
	};
	inline void PushNew (int v, int v2) { if (Find(v, v2) == -1) Push(v, v2); };
	

	MultiVectOpt(int V, int D);
	~MultiVectOpt(void);
};
