#pragma once
#include "utils.h"
class BaseGeometryDecoder
{
protected:
	int m_nVertices;
	int m_dim;
	int m_nQBits;
	float * m_geometry;
public:

	inline void SetGeometryList(float * geometry){ m_geometry = geometry;}
	inline void SetNVertices(int nVertices){ m_nVertices = nVertices;}
	inline void SetDim(int dim){ m_dim = dim;}


	// This function decodes the the geometry represented as m_dim times m_nVertices array
	// Rq.: no memory allocation is done inside this function. The user should allocate and free the memory. 
	virtual int Decode(DecoderStreamInfo &streamInfo) = 0;

	BaseGeometryDecoder(void):m_nVertices(0), m_dim(3), m_nQBits(12){};
	~BaseGeometryDecoder(void){};
};
