//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom 2008, 2009
// Licensed Materials, Program Property of Institut Telecom
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// retains full right to use the code for his/her own purpose, assign or donate 
// the code to a third party and to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//
//
//    http://www-artemis.it-sudparis.eu/
//    
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//
//-------------------------------------------------------------------------
#pragma once
#include <vector>
#include <stdlib.h>

class BinaryIO
{
	std::vector<int> stream_;
	int currentposition_;

public:
	inline int Size() {return (int) stream_.size();};
	inline void WriteBin(int val) {
		if (val == 0) stream_.push_back(0);
		else stream_.push_back(1);
		currentposition_++;
	};
	inline void Rewind() {
		currentposition_ = 0;
	}
	inline void Clear() {
		stream_.clear();
		currentposition_ = 0;
	}

	inline int ReadBin() {
		return stream_[currentposition_++];
	};

	void WriteBinary(int val, int nbits);
	int ReadBinary (int nbits);


	void WriteSignedBinary(int val, int nbits);
	int ReadSignedBinary(int nbits);

	void ExpGolombEncode(unsigned int symbol, int k);
	void UnaryExpGolombEncode(unsigned int symbol, unsigned int exp_start);

	unsigned int ExpGolombDecode(int k);
	unsigned int UnaryExpGolombDecode(unsigned int exp_start);

	inline int nBinaryBits(unsigned int m){
		int n=0;
		while (m!=0) {
			m = (m>>1);
			n++;
		}
		return n;
	};


	BinaryIO(void);
	~BinaryIO(void);
};
