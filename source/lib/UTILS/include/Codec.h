#pragma once

#include "ac.h"

extern ac_encoder ace;
extern ac_model acm_header;
extern ac_model acm_coord;
extern ac_model acm_type;
extern ac_model acm_position;
extern ac_model acm_faceDirection;
extern ac_model acm_coordIndex;
extern ac_model acm_hasNext;
extern ac_model acm_sign;

extern ac_model ACM_Bin;

extern ac_decoder acd;

