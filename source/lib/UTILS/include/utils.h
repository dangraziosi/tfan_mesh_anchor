#ifndef _UTILS_H
#define _UTILS_H

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <vector>
#include "MultiVectOpt.h"



typedef std::vector<int> IntVect;
typedef std::vector<IntVect> VectIntVect;

#define EXTRA_MEM_ALLOCATED 1024

#define SC3DMCStartCode 0x00001F1
#define	DEFAULTQP 10
#define	DEFAULTPRED	3
#define LEN_PREFIX	5
#define MAX_VALUE 1000000
#define MIN_VALUE -1000000
#define INIT_UCHAR 0x00
#define LEN_BYTE	 8
#define LEN_QP		5
#define COORD_DIVISION	3
#define LEN_BPLMAX	32
#define QUANT_SIZE 10
#define LEN_4BITS	4

#define	FALSE	0
#define	TRUE	1

#define WRITE_TEMP if(lowHigh == 0)buffer[code_bytes]|=temp;else{buffer[code_bytes++]|=temp<<4;buffer[code_bytes]=0;}lowHigh=(lowHigh+1)%2
#define READ_TEMP if(lowHigh == 0) temp = (*(streamInfo.m_compressedStream))& 15;else temp=((*(streamInfo.m_compressedStream)++)& 240)>>4;lowHigh=(lowHigh+1)%2



enum EncodingMode {
	QBCR, 
	SVA,
	TFAN,
	PTFAN
};

class DecoderStreamInfo
{
public :
	unsigned char *m_compressedStream;
	unsigned int m_unCurrentBitPosition;
	unsigned int m_unCurrentBufferPosition;

	DecoderStreamInfo(void):m_compressedStream(NULL), m_unCurrentBitPosition(7), m_unCurrentBufferPosition(0){};
	~DecoderStreamInfo(void){};
};

class EncoderStreamInfo
{
public :
	unsigned char *m_compressedStream;
	unsigned int m_compressedStreamSize;

	unsigned char m_dumpBit;							// byte size data to represent the bit before dump
	int m_bitPostion;									// indicate present bit position in dumpBit	

	EncoderStreamInfo(void):m_compressedStream(NULL), m_compressedStreamSize(0), m_dumpBit(INIT_UCHAR), m_bitPostion(0){};
	~EncoderStreamInfo(void){};
};


void Write2Stream(unsigned char * & stream, unsigned int & compressedStreamSize, const void * source, unsigned int num);
void WriteStream2File(const char * fileName, const unsigned char * stream, unsigned int compressedStreamSize);
unsigned char * ReadStream2File(const char * fileName,  unsigned int & compressedStreamSize);
void ReadFromStream(unsigned char * & stream, void * source, unsigned int num);
void Insert_Data(unsigned char * & stream, unsigned int & compressedStreamSize, const void * source, unsigned int num);




enum BinarizationMode
{
	FL = 0,					// Fixed Length
	BP = 1,					// BPC
	FC = 2,					// 4 bits Coding
	AC = 3,					// Arithmetic Coding
	AC_EGC = 4				// AC/EGk coding
};

enum PredictionMode
{
	NoPrediction = 0, //0
	DifferentialPrediction = 1,//1
	XORPrediction = 2,//2
	AdaptiveDifferentialPrediction = 3,//3
	CircularDifferentialPrediction = 4,//4s
	TFANParallelogramPrediction = 5//5
};

enum QuantizationMode
{
	UniformQuantization = 0,	//
	NormalQuantization = 1,		//
	UniformTextureQuantization = 2		// 
};



class ConnectivityBasedPredictor
{
protected:
	int m_nVertices;
	int m_dim;
	int * m_triangles;
	int m_nTriangles;
	int m_predMode;

	int * m_intArray;	//qTable_
	int * m_intArrayPredicted;//errTable_
	int * m_tagVertex;
	int * m_tagTriangle;

	MultiVectOpt  * m_tfans;
	MultiVectOpt * m_V2T;
	VectIntVect m_vertex2Vertex;
	VectIntVect m_vertex2Triangle;
	VectIntVect m_triangle2Triangle;

	bool GetCoordIndex(int pos, int * coordIndex);
	void AddNeighborVertex2Vertex(int v1, int v2);
	void ComputeVertex2Vertex();
	void AddNeighborVertex2Triangle(int v, int t);
	void ComputeVertex2Triangle();
	void AddNeighborTriangle2Triangle(int t1, int t2);
	void ComputeTriangle2Triangle();
	int Round(double a);

	int DirectFastParaPred();

	int InvFastParaPred();


	void Init();

public:
	int Predict();
	int InvPredict();


	ConnectivityBasedPredictor(int * intArray, int numberOfdata, int dim, int nTriangles, int * triangles, int predMode, int * intArrayPredicted, MultiVectOpt * tfans=NULL, MultiVectOpt * V2T=NULL)
	{
		m_intArrayPredicted = intArrayPredicted;
		m_intArray = intArray;
		m_nVertices = numberOfdata;
		m_dim = dim;
		m_triangles = triangles;
		m_nTriangles = nTriangles;
		m_predMode = predMode;
		m_tfans = tfans;
		m_V2T = V2T;
		m_tagVertex = NULL;
		m_tagTriangle = NULL;
	}
	~ConnectivityBasedPredictor(void)
	{
		if (m_tagVertex != NULL) delete [] m_tagVertex;
		if (m_tagTriangle != NULL) delete [] m_tagTriangle;
	}
};


	 void InsertData(EncoderStreamInfo &streamInfo, int symbol, int length);			// to insert data in buffer limited in length bit.
	 void Flush(EncoderStreamInfo &streamInfo);
	 void BinarizeIntArray(EncoderStreamInfo &streamInfo, int * intArray, int * preds, int numberOfdata, int nQBits, int dim, int predictionMode, int binarizationMode, unsigned int M = 254);
	 void PredictIntArray(EncoderStreamInfo &streamInfo, int * intArray, int numberOfdata, int dim, int predictionMode, int * intArrayPredicted, int * preds, int nTriangles, int * triangles, MultiVectOpt * tfans, MultiVectOpt * V2T, int sizeOfData);
	 void EncodeIntArray(EncoderStreamInfo &streamInfo, int * intArray, int numberOfdata, int dim, int predictionMode, int binarizationMode, int nQBits, int sizeOfData, int nTriangles = 0, int * triangles = NULL, MultiVectOpt * tfans=NULL, MultiVectOpt * V2T=NULL);
	 void EncodeFloatArray(EncoderStreamInfo &streamInfo, float * floatArray, int numberOfdata, int dim, int predictionMode, int binarizationMode, int quantizationMode, int* nQBits, float *quantMin, float *quantRange, int nTriangles = 0, int * triangles = NULL, MultiVectOpt * tfans=NULL, MultiVectOpt * V2T=NULL);
	 void QuantizeFloatArray(float * floatArray, int numberOfdata, int dim, int* nQBits, int * intArray);
	 void QuantizeFloatArray(float * floatArray, int numberOfdata, int nQBits, int * intArray);  //add
	 void QuantizeFloatArray(float * floatArray, int numberOfdata, int dim, int nQBits, float *quantMin, float *quantRange, int * intArray);

	 unsigned int ReadBitsFromStorage(DecoderStreamInfo &streamInfo, unsigned int unReadLength);
	 void Flush(DecoderStreamInfo &streamInfo);
	 void InverseBinarizeIntArray(DecoderStreamInfo &streamInfo, int * intArray, int * preds, int numberOfdata, int dim, int &predictionMode, int &binarizationMode);
	 void InversePredictIntArray(DecoderStreamInfo &streamInfo, int * intArray, int numberOfdata, int dim, int predictionMode, int sizeOfData, int * preds,int nTriangles, int * triangles, MultiVectOpt * tfans, MultiVectOpt * V2T);
	 void DecodeFloatArray(DecoderStreamInfo &streamInfo, float * floatArray, int numberOfdata, int dim, int &predictionMode, int &binarizationMode, int quantizationMode, int* nQBits, float *quantMin, float *quantRange, int nTriangles = 0, int * triangles = NULL, MultiVectOpt * tfans=NULL, MultiVectOpt * V2T=NULL);
	 void DecodeIntArray(DecoderStreamInfo &streamInfo, int sizeOfDat, int * intArray, int numberOfdata, int dim, int &predictionMode, int &binarizationModea, int nTriangles = 0, int * triangles = NULL, MultiVectOpt * tfans=NULL, MultiVectOpt * V2T=NULL);
	 void InverseQuantizeFloatArray(float * floatArray, int numberOfdata, int dim, int* nQBits, int * intArray);
	 void InverseQuantizeFloatArray(float * floatArray, int numberOfdata, int nQBits, int * intArray);  //add
	 void InverseQuantizeFloatArray(float * floatArray, int numberOfdata, int dim, int nQBits, float *quantMin, float *quantRange, int * intArray);


	 //dmlab
	 void _code2Normal(const unsigned long code, float *normal, unsigned int subdivision); //add
	 unsigned long _normal2code(const float *normal,unsigned int subdivision); //add
	 //~dmlab

#endif // _UTILS_H