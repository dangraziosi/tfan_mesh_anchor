#include "BinaryIO.h"

BinaryIO::BinaryIO(void)
{
	currentposition_ = 0;
}

BinaryIO::~BinaryIO(void)
{
}

void BinaryIO::WriteBinary(int val, int nbits){
	int m = -1;
	for (int k = 0; k < nbits; k++) {
		m = 1 << k;
		if (val & m) WriteBin(1);
		else WriteBin(0);
	}
}
void BinaryIO::WriteSignedBinary(int val, int nbits){
	if (val >= 0) WriteBin(0);
	else WriteBin(1);
	val = abs(val);
	WriteBinary(val, nbits);
}


int BinaryIO::ReadBinary(int nbits){
	int val = 0;
	for (int k = 0; k < nbits; k++) {
		val = val + (ReadBin() << k);
	}
	return val;
}
int BinaryIO::ReadSignedBinary(int nbits){
	int val = 0;
	int sign = ReadBin();
	val = ReadBinary(nbits);
	if (sign) val = -val;
	return val;
}


/*
/*!
 ************************************************************************
 * \brief
 *    Exp Golomb binarization and encoding
 ************************************************************************
 */
void BinaryIO::ExpGolombEncode(unsigned int symbol, int k)
{
  while(1){
    if (symbol >= (unsigned int)(1<<k)){
		WriteBin(1);
	    symbol = symbol - (1<<k);
        k++;
    }
    else{
		WriteBin(0); //now terminated zero of unary part
	    while (k--)                               //next binary part
	        WriteBin((signed short)((symbol>>k)&1));
	      break;
    }
  }
  return;
}
/*
 ************************************************************************
 * \brief
 *    Exp-Golomb Encoding
*
************************************************************************/
void BinaryIO::UnaryExpGolombEncode(unsigned int symbol, unsigned int exp_start)

{
  unsigned int l,k;
  if (symbol==0){
    WriteBin(0);
    return;
  }
  else{
    WriteBin(1);
    l=symbol;
    k=1;
    while (((--l)>0) && (++k <= exp_start))
      WriteBin(1);

    if (symbol < exp_start) WriteBin(0);
    else ExpGolombEncode(symbol-exp_start,0);
  }
  return;
}

/*!
 ************************************************************************
 * \brief
 *    Exp Golomb binarization and decoding of a symbol
 *    with prob. of 0.5
 ************************************************************************
 */

unsigned int BinaryIO::ExpGolombDecode(int k)
{
  unsigned int l;
  int symbol = 0;
  int binary_symbol = 0;
  do
  {
    l=ReadBin();
    if (l==1)
    {
      symbol += (1<<k);
      k++;
    }
  }
  while (l!=0);

  while (k--)                             //next binary part
    if (ReadBin()==1)
      binary_symbol |= (1<<k);
  return (unsigned int) (symbol+binary_symbol);

}
/*!
 ************************************************************************
 * \brief
 *    Unary Exp-Golomb decoding
 ***********************************************************************
 */
unsigned int BinaryIO::UnaryExpGolombDecode(unsigned int exp_start)
{
  unsigned int l,k;
  unsigned int symbol;
  symbol = ReadBin();
  if (symbol==0)
    return 0;
  else
  {
    symbol=0;
    k=1;
    do
    {
      l=ReadBin();
      symbol++;
      k++;
    }
    while((l!=0) && (k!=exp_start));
    if (l!=0)
      symbol += ExpGolombDecode(0)+1;
    return symbol;
  }
}

