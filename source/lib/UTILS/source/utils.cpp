//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom 2008, 2009
// Licensed Materials, Program Property of Institut Telecom
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// retains full right to use the code for his/her own purpose, assign or donate 
// the code to a third party and to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//
//
//    http://www-artemis.it-sudparis.eu/
//    
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//
//-------------------------------------------------------------------------

//#pragma warning (disable : 4786)

#include <Arithmetic_Codec.h>
#include <utils.h>

#include <cmath>
#include <map>
#include <cstdlib>


#ifndef WIN32
#define __int64 int64_t
#endif
//#include "CConsole.h"
//extern CConsole* con; 


void Write2Stream(unsigned char * & stream, unsigned int & compressedStreamSize, const void * source, unsigned int num)
{
	memcpy(stream, source, num);
	compressedStreamSize += num;
	stream += num;
}
void ReadFromStream(unsigned char * & stream, void * dest, unsigned int num)
{
	memcpy(dest, stream, num);
	stream += num;
}
void WriteStream2File(const char * fileName, const unsigned char * stream, unsigned int compressedStreamSize)
{
	FILE * out = fopen(fileName, "wb");
	fwrite((const char *) &compressedStreamSize, 1, sizeof(unsigned int), out);
	fwrite((const char *) stream, compressedStreamSize, sizeof(char), out);
	fclose(out);
}
unsigned char * ReadStream2File(const char * fileName,  unsigned int & compressedStreamSize)
{
	FILE * in = fopen(fileName, "rb");
	if (in == NULL)
	{
		printf("\nCould not open (FILENAME) %s\n", fileName);
		exit(-1);
	}
	fread((char *) &compressedStreamSize, 1, sizeof(unsigned int), in);
	unsigned char * stream = new unsigned char[compressedStreamSize];
	fread((char *) stream, compressedStreamSize, sizeof(char), in);
	fclose(in);
	return stream;
}

void ConnectivityBasedPredictor::Init()
{
	m_tagVertex = new int [m_nVertices];
	for(int v = 0; v < m_nVertices; v++) {
		m_tagVertex[v] = 0;
	}
	m_tagTriangle = new int [m_nTriangles];
	for(int t = 0; t < m_nTriangles; t++) {
		m_tagTriangle[t] = 0;
	}
	m_vertex2Triangle.resize(m_nVertices);
	m_vertex2Vertex.resize(m_nVertices);
	m_triangle2Triangle.resize(m_nTriangles);
}	

void ConnectivityBasedPredictor::AddNeighborVertex2Vertex(int v1, int v2)
{
	int found = 0;	
	for (IntVect::iterator posc = m_vertex2Vertex[v1].begin(); posc != m_vertex2Vertex[v1].end(); ++posc) {
		if ((*posc) == v2) {
			found = 1;
			break;
		}
	}
	if (found == 0)
		m_vertex2Vertex[v1].push_back(v2);
}

void ConnectivityBasedPredictor::ComputeVertex2Vertex(){
	for (int f = 0; f < m_nTriangles; f++) {
		AddNeighborVertex2Vertex(m_triangles[f*3+0], m_triangles[f*3+1]);
		AddNeighborVertex2Vertex(m_triangles[f*3+0], m_triangles[f*3+2]);

		AddNeighborVertex2Vertex(m_triangles[f*3+1], m_triangles[f*3+0]);
		AddNeighborVertex2Vertex(m_triangles[f*3+1], m_triangles[f*3+2]);

		AddNeighborVertex2Vertex(m_triangles[f*3+2], m_triangles[f*3+0]);
		AddNeighborVertex2Vertex(m_triangles[f*3+2], m_triangles[f*3+1]);
	}
}
void ConnectivityBasedPredictor::AddNeighborVertex2Triangle(int v, int t)
{
	int found = 0;	
	for (IntVect::iterator posc = m_vertex2Triangle[v].begin(); posc != m_vertex2Triangle[v].end(); ++posc) {
		if ((*posc) == t) {
			found = 1;
			break;
		}
	}
	if (found == 0)
		m_vertex2Triangle[v].push_back(t);
}

void ConnectivityBasedPredictor::ComputeVertex2Triangle(){
	for (int f = 0; f < m_nTriangles; f++) {
		AddNeighborVertex2Triangle(m_triangles[f*3+0], f);
		AddNeighborVertex2Triangle(m_triangles[f*3+1], f);
		AddNeighborVertex2Triangle(m_triangles[f*3+2], f);
	}
}
void ConnectivityBasedPredictor::AddNeighborTriangle2Triangle(int t1, int t2)
{
	int found = 0;	
	for (IntVect::iterator posc = m_triangle2Triangle[t1].begin(); posc != m_triangle2Triangle[t1].end(); ++posc) {
		if ((*posc) == t2) {
			found = 1;
			break;
		}
	}
	if (found == 0)
		m_triangle2Triangle[t1].push_back(t2);
}

void ConnectivityBasedPredictor::ComputeTriangle2Triangle(){
	int coordIndex[3] = {-1, -1, -1};

	for (int t = 0; t < m_nTriangles; t++) {
		GetCoordIndex(t, coordIndex);
		for (int k = 0; k < 3; k++) {
			for (IntVect::iterator pt0 = m_vertex2Triangle[coordIndex[k]].begin(); pt0 != m_vertex2Triangle[coordIndex[k]].end(); ++pt0){
				if ((*pt0) != t){
					int tr0 = (*pt0);
					for (int i=0; i <  (int) m_vertex2Triangle[coordIndex[(k+1)%3]].size();i++){
						int tr1 = m_vertex2Triangle[coordIndex[(k+1)%3]][i];
						if (tr1 == tr0){
							AddNeighborTriangle2Triangle(t, tr1);
						}
					}
				}
			}
		}
	}
}

int ConnectivityBasedPredictor::Round(double a){
      if (a >= 0.0) return (int) (a+0.5);
      return -(int) (-a+0.5);
}

bool ConnectivityBasedPredictor::GetCoordIndex(int pos, int * coordIndex) {
	if (pos < m_nTriangles) {
		for(int h = 0; h < 3; h++) {
			coordIndex[h] = m_triangles[pos*3+h];
		}
		return true;
	}
	return false;
}




//===================================================================================
int ConnectivityBasedPredictor::DirectFastParaPred(){
	int a = 0;
	int b = 0;
	int c = 0;
	int d = 0;
	int e = -1;
	int ta = -1;
	int x = -1;
	int y = -1;
	int z = -1;
	int v = 0;
	for(v = 0; v < m_tfans->GetV(); v++) {
		if(m_tfans->GetSize(v)>2){
			// first vertex
			a = m_tfans->GetElement(v,0);
			if(m_tagVertex[a]==0){
				for (int i = 0; i < m_dim; i++) {
					m_intArrayPredicted[a*m_dim+i] = m_intArray[a*m_dim+i];
				}
				m_tagVertex[a]=2;
			}

			// second vertex
			b = m_tfans->GetElement(v,1);
			if(m_tagVertex[b]==0){
				for (int i = 0; i < m_dim; i++) {
					m_intArrayPredicted[b*m_dim+i] = m_intArray[b*m_dim+i] - m_intArray[a*m_dim+i];
				}
				m_tagVertex[b]=1;
			}

			//third vertex
			c = m_tfans->GetElement(v,2);
			if(m_tagVertex[c]==0){
				e = -1;
				ta = -1;
				x = -1;
				y = -1;
				z = -1;

				for (int u = 0; u < m_V2T->GetSize(a); u++) {
					ta = m_V2T->GetElement(a, u);
					x = m_triangles[ta*3];
					y = m_triangles[ta*3+1];
					z = m_triangles[ta*3+2];
					if ( (x==a) && (y==b) && (z!=c) && (m_tagVertex[z]!=0) ) { e = z; break;}
					if ( (x==a) && (z==b) && (y!=c) && (m_tagVertex[y]!=0) ) { e = y; break;}

					if ( (y==a) && (x==b) && (z!=c) && (m_tagVertex[z]!=0) ) { e = z; break;}
					if ( (y==a) && (z==b) && (x!=c) && (m_tagVertex[x]!=0) ) { e = x; break;}

					if ( (z==a) && (x==b) && (y!=c) && (m_tagVertex[y]!=0) ) { e = y; break;}
					if ( (z==a) && (y==b) && (x!=c) && (m_tagVertex[x]!=0) ) { e = x; break;}
				}
				if (e==-1) {
					for (int i = 0; i < m_dim; i++) {
						m_intArrayPredicted[c*m_dim+i] = m_intArray[c*m_dim+i] - m_intArray[b*m_dim+i];
					}
					m_tagVertex[c]=1;
				}
				else {
					for (int i = 0; i < m_dim; i++) {
						m_intArrayPredicted[c*m_dim+i] = m_intArray[c*m_dim+i] - m_intArray[b*m_dim+i] + m_intArray[e*m_dim+i] - m_intArray[a*m_dim+i];
					}
					m_tagVertex[c]=1;
				}
			}

			// remaining vertices
			for(int k = 3; k < m_tfans->GetSize(v); k++){
				d = m_tfans->GetElement(v,k);
				if(m_tagVertex[d]==0){
					for (int i = 0; i < m_dim; i++) {
						m_intArrayPredicted[d*m_dim+i] = m_intArray[d*m_dim+i] - m_intArray[c*m_dim+i] + m_intArray[b*m_dim+i] - m_intArray[a*m_dim+i];
					}
					b = c;
					c = d;
					m_tagVertex[d]=1;
				}
			}
		}
	}
	for (v=0; v < m_nVertices; v++) {
		if(m_tagVertex[v]==0){
			for (int i = 0; i < m_dim; i++) {
				m_intArrayPredicted[v*m_dim+i] = m_intArray[v*m_dim+i];
			}
			m_tagVertex[v]=2;
		}
	}
	return 0;
}


int ConnectivityBasedPredictor::Predict(){

	Init();
	DirectFastParaPred();
	return 0;
}

int ConnectivityBasedPredictor::InvPredict(){
	Init();
	InvFastParaPred();
	return 0;
}

int ConnectivityBasedPredictor::InvFastParaPred(){
	int a = 0;
	int b = 0;
	int c = 0;
	int d = 0;
	int e = -1;
	int ta = -1;
	int x = -1;
	int y = -1;
	int z = -1;
	int v = 0;
	for(v = 0; v < m_tfans->GetV(); v++) {
		if(m_tfans->GetSize(v)>2){
			// first vertex
			a = m_tfans->GetElement(v,0);
			if(m_tagVertex[a]==0){
				for (int i = 0; i < m_dim; i++) {
					m_intArrayPredicted[a*m_dim+i] = m_intArray[a*m_dim+i];
				}
				m_tagVertex[a]=2;
			}

			// second vertex
			b = m_tfans->GetElement(v,1);
			if(m_tagVertex[b]==0){
				for (int i = 0; i < m_dim; i++) {
					m_intArrayPredicted[b*m_dim+i] =  m_intArray[b*m_dim+i] + m_intArrayPredicted[a*m_dim+i];
				}
				m_tagVertex[b]=1;
			}

			//third vertex
			c = m_tfans->GetElement(v,2);
			if(m_tagVertex[c]==0){
				e = -1;
				ta = -1;
				x = -1;
				y = -1;
				z = -1;

				for (int u = 0; u < m_V2T->GetSize(a); u++) {
					ta = m_V2T->GetElement(a, u);
					x = m_triangles[ta*3];
					y = m_triangles[ta*3+1];
					z = m_triangles[ta*3+2];
					if ( (x==a) && (y==b) && (z!=c) && (m_tagVertex[z]!=0) ) { e = z; break;}
					if ( (x==a) && (z==b) && (y!=c) && (m_tagVertex[y]!=0) ) { e = y; break;}

					if ( (y==a) && (x==b) && (z!=c) && (m_tagVertex[z]!=0) ) { e = z; break;}
					if ( (y==a) && (z==b) && (x!=c) && (m_tagVertex[x]!=0) ) { e = x; break;}

					if ( (z==a) && (x==b) && (y!=c) && (m_tagVertex[y]!=0) ) { e = y; break;}
					if ( (z==a) && (y==b) && (x!=c) && (m_tagVertex[x]!=0) ) { e = x; break;}
				}
				if (e==-1) {
					for (int i = 0; i < m_dim; i++) {
						m_intArrayPredicted[c*m_dim+i] = m_intArray[c*m_dim+i] + m_intArrayPredicted[b*m_dim+i];
					}
					m_tagVertex[c]=1;
				}
				else {
					for (int i = 0; i < m_dim; i++) {
						m_intArrayPredicted[c*m_dim+i] = m_intArray[c*m_dim+i] + m_intArrayPredicted[b*m_dim+i] - m_intArrayPredicted[e*m_dim+i] + m_intArrayPredicted[a*m_dim+i];
					}
					m_tagVertex[c]=3;
				}
			}

			// remaining vertices
			for(int k = 3; k < m_tfans->GetSize(v); k++){
				d = m_tfans->GetElement(v,k);
				if(m_tagVertex[d]==0){
					for (int i = 0; i < m_dim; i++) {
						m_intArrayPredicted[d*m_dim+i] = m_intArray[d*m_dim+i] + m_intArrayPredicted[c*m_dim+i] - m_intArrayPredicted[b*m_dim+i] + m_intArrayPredicted[a*m_dim+i];
					}
					b = c;
					c = d;
					m_tagVertex[d]=3;
				}
			}
		}
	}
	for (v=0; v < m_nVertices; v++) {
		if(m_tagVertex[v]==0){
			for (int i = 0; i < m_dim; i++) {
				m_intArrayPredicted[v*m_dim+i] = m_intArray[v*m_dim+i];
			}
			m_tagVertex[v]=2;
		}
	}
	return 0;
}

//dmlab

void DecodeFloatArray(DecoderStreamInfo &streamInfo, float * floatArray, int numberOfdata, int dim, int &predictionMode, int &binarizationMode, int quantizationMode, int* nQBits, float *quantMin, float *quantRange,
								   int nTriangles, int * triangles, MultiVectOpt * tfans, MultiVectOpt * V2T)
{
    if(quantizationMode == 1)	// added to be coherent with the normals quatization
    {							// one value is associated to each normal (3 coordinates)
		dim = 1;
    }
	int * intArray = new int[numberOfdata * dim];
	DecodeIntArray(streamInfo, (1<<nQBits[0]), intArray, numberOfdata, dim, predictionMode, binarizationMode, nTriangles, triangles, tfans, V2T);
	if(quantizationMode == UniformQuantization)
	{
		InverseQuantizeFloatArray(floatArray, numberOfdata, dim, nQBits[0], quantMin, quantRange, intArray);
	}else if(quantizationMode == NormalQuantization ){
		InverseQuantizeFloatArray(floatArray, numberOfdata, nQBits[0], intArray);
	}else if(quantizationMode == UniformTextureQuantization)
	{
		int tQP[2];
		tQP[0] = nQBits[1];
		tQP[1] = nQBits[2];
		InverseQuantizeFloatArray(floatArray, numberOfdata, dim, tQP, intArray);
	}


	delete [] intArray;

}
//~dmlab

void DecodeIntArray(DecoderStreamInfo &streamInfo, int sizeOfData, int * intArray, int numberOfdata, int dim, int &predictionMode, int &binarizationMode,
								 int nTriangles, int * triangles, MultiVectOpt * tfans, MultiVectOpt * V2T)
{
	int * preds = new int[numberOfdata * dim];
	InverseBinarizeIntArray(streamInfo, intArray, preds, numberOfdata, dim, predictionMode, binarizationMode);

	InversePredictIntArray(streamInfo, intArray, numberOfdata, dim, predictionMode, sizeOfData, preds, nTriangles, triangles, tfans, V2T);
	delete [] preds;
}

unsigned int ReadBitsFromStorage(DecoderStreamInfo &streamInfo, unsigned int unReadLength)
{
	unsigned int n = unReadLength;
	int c=(n + 14 - streamInfo.m_unCurrentBitPosition) >> 3;

	union {
		long long l; //is not compatible with VS C++ 6.0
		//__int64 l;
		unsigned char c[8];
	} buf;
	buf.l = 0;

	int i=0;
	while(n) {
		i++;
		buf.c[c - i] = *(streamInfo.m_compressedStream + streamInfo.m_unCurrentBufferPosition);
		if(streamInfo.m_unCurrentBitPosition >= n) {
			streamInfo.m_unCurrentBitPosition -= n;
			break;
		} else {
			n -= streamInfo.m_unCurrentBitPosition + 1;
			streamInfo.m_unCurrentBitPosition = 7;
			streamInfo.m_unCurrentBufferPosition++;
		}
	}

	return (unsigned int) (buf.l >> ((streamInfo.m_unCurrentBitPosition + 1) % 8)) & ~(~static_cast<unsigned int>(0) << unReadLength);
}


void InverseQuantizeFloatArray(float * floatArray, int numberOfdata, int dim, int nQBits, float *quantMin, float *quantRange, int * intArray)
{
	float * delta = new float[dim]; 
	for(int d = 0; d < dim; d++)
	{
		if (quantRange[d] > 0.0f) delta[d] = (float)((1 << nQBits) - 1) / quantRange[d];
		else delta[d] = 1.0f;
	}

	for(int i = 0; i < numberOfdata * dim; i++)
	{
		floatArray[i] = quantMin[i%dim] + intArray[i] / delta[i%dim];
	}
	delete [] delta;
}


//dmlab
void InverseQuantizeFloatArray(float * floatArray, int numberOfdata, int dim, int* nQBits, int * intArray)
{

	for(int i = 0; i < numberOfdata * dim; i++)
	{
		floatArray[i] = (float)intArray[i] / nQBits[i%dim];
	}

}

void InverseQuantizeFloatArray(float * floatArray, int numberOfdata, int nQBits, int * intArray)
{
	int subdivision = (nQBits-3)/2;
	for(int i = 0; i < numberOfdata; i++)
	{
		_code2Normal(intArray[i],floatArray+(i*3),subdivision);
	}

}
//~dmlab

//dmlab
void InversePredictIntArray(DecoderStreamInfo &streamInfo, int * intArray, int numberOfdata, int dim, int predictionMode, int sizeOfData, int * preds,
										 int nTriangles, int * triangles, MultiVectOpt * tfans, MultiVectOpt * V2T)
{
	if(predictionMode == NoPrediction)  
	{
		return;
	}
	else if(predictionMode == DifferentialPrediction)  
	{
		for(int i = dim; i < numberOfdata * dim; i++)
		{
			intArray[i] +=  intArray[i-dim];
		}
	}
	else if(predictionMode == XORPrediction) 
	{
		for(int i = dim; i < numberOfdata * dim; i++)
		{
			intArray[i] ^=  intArray[i-dim];
		}
	}
	else if(predictionMode == AdaptiveDifferentialPrediction)
	{
		for(int i = 0; i < numberOfdata * dim; i++)
		{
			if (preds[i] > 0)
			{
				intArray[i] += intArray[i-dim * preds[i]];
			}
		}
		
	}
	else if(predictionMode == CircularDifferentialPrediction)
	{
		int nData;
		for(int i = dim; i < numberOfdata * dim; i++)
		{
			nData = intArray[i] + intArray[i-dim];
			if(nData < 0 )
				intArray[i] = sizeOfData + nData;
			else if(nData >= sizeOfData)
				intArray[i] = nData - sizeOfData;
			else
				intArray[i] = nData;
		}
	}
	else
	{
		ConnectivityBasedPredictor myPredictor(intArray, numberOfdata, dim, nTriangles, triangles, predictionMode, intArray, tfans, V2T);
		myPredictor.InvPredict();
	}
}
//~dmlab

void InverseBinarizeIntArray(DecoderStreamInfo &streamInfo, int * intArray, int * preds, int numberOfdata, int dim, int &predictionMode, int &binarizationMode)
{
	unsigned char mask = 0;	
	ReadFromStream(streamInfo.m_compressedStream, &mask, sizeof(unsigned char));
	predictionMode = mask & 7;
	binarizationMode = mask>>4;

	if (binarizationMode == FL) // Fixed Length
	{
		unsigned char nQBits = 0;
		unsigned long long sizeofbs=0;
		int length;
        ReadFromStream(streamInfo.m_compressedStream, &sizeofbs, 4);		
		
		ReadFromStream(streamInfo.m_compressedStream, &nQBits, sizeof(unsigned char));
		for(int i = 0 ; i < numberOfdata*dim; i++)
		{
			intArray[i] = ReadBitsFromStorage(streamInfo, nQBits);
		}
		Flush(streamInfo);
	}
	else if(binarizationMode == BP)// to be updated
	{
        unsigned long long sizeofbs=0;
        ReadFromStream(streamInfo.m_compressedStream, &sizeofbs, 4);
		if ((predictionMode == DifferentialPrediction) || (predictionMode == TFANParallelogramPrediction) || (predictionMode == CircularDifferentialPrediction) ) 
		{
			int * BPLTable = new int [32];
			BPLTable[0] = 0;
			BPLTable[1] = 1;
			BPLTable[2] = 2;
			int i = 0;
			for(i = 3; i <32; i++)
			{
				BPLTable[i] = static_cast<int>(pow(2.0,(double)(i-1))-1);
			}

			unsigned int prefixSize, nBPL, nPayload, nSign;
			int difValue, nQuantValue;
			nQuantValue=0;
			prefixSize = ReadBitsFromStorage(streamInfo, 5);
			Flush(streamInfo); 

			for(i = 0 ; i < numberOfdata*dim; i++)
			{
				nBPL=ReadBitsFromStorage(streamInfo, prefixSize);
				if(nBPL > 2)
				{
					nPayload = ReadBitsFromStorage(streamInfo, nBPL-1);
				}
				else
				{
					nPayload = 0;
				}

				difValue = BPLTable[nBPL] + nPayload;
				if(difValue != 0)
				{
					nSign = ReadBitsFromStorage(streamInfo, 1);
					if(nSign == 1)
					{
						difValue = -difValue;
					}
				}
				intArray[i] = difValue;
			}
			delete [] BPLTable;
		}
		else if(predictionMode == XORPrediction)
		{
			int * BPLTable = new int [32];
			BPLTable[0] = 0;
			BPLTable[1] = 1;
			BPLTable[2] = 2;
			int i = 3;
			for(i = 3; i <32; i++)
			{
				BPLTable[i] = static_cast<int>(pow(2.0,(double)(i-1))-1);
			}

			unsigned int prefixSize, nBPL, nPayload; //, nSign;
			int difValue, nQuantValue;
			nQuantValue=0;
			prefixSize = ReadBitsFromStorage(streamInfo, 5);
			Flush(streamInfo); 

			for(i = 0 ; i < numberOfdata*dim; i++)
			{
				nBPL=ReadBitsFromStorage(streamInfo, prefixSize);

				if(nBPL > 2)
				{
					nPayload = ReadBitsFromStorage(streamInfo, nBPL-1);

				}
				else
				{
					nPayload = 0;
				}

				difValue = BPLTable[nBPL] + nPayload;
				intArray[i] = difValue;
			}
			delete [] BPLTable;

		}
		else if(predictionMode == AdaptiveDifferentialPrediction)
		{
			int * BPLTable = new int [32];
			BPLTable[0] = 0;
			BPLTable[1] = 1;
			BPLTable[2] = 2;
			int i = 0;
			for(i = 3; i <32; i++)
			{
				BPLTable[i] = static_cast<int>(pow(2.0,(double)(i-1))-1);
			}

			unsigned int prefixSize, nBPL, nPayload, nPred;
			int difValue, nQuantValue;
			nQuantValue=0;
			prefixSize = ReadBitsFromStorage(streamInfo, 5);
			Flush(streamInfo); 

			const int sign[2]={1, -1};
			for(i = 0 ; i < numberOfdata*dim; i++)
			{
				preds[i] = 0;
				while(1)
				{
					nPred = ReadBitsFromStorage(streamInfo, 2);
					preds[i] += nPred;
					if(nPred != 3)
						break;
				}

				nBPL=ReadBitsFromStorage(streamInfo, prefixSize);
				if(nBPL > 2)
				{
					nPayload = ReadBitsFromStorage(streamInfo, nBPL-1);
				}
				else
				{
					nPayload = 0;
				}

				difValue = BPLTable[nBPL] + nPayload;
				if ( preds[i] != 0) 
					intArray[i] = sign[difValue % 2] * ((difValue+1)/2);
				else 
					intArray[i] = difValue;
				
			}
			delete [] BPLTable;
		}
		Flush(streamInfo);
	}
	else if(binarizationMode == AC)// to be updated
	{
		if ((predictionMode == DifferentialPrediction) || (predictionMode == TFANParallelogramPrediction) || (predictionMode == CircularDifferentialPrediction) ) 
		{
			unsigned int nSign;
			int difValue,  nQuantValue;
			nQuantValue=0;
			unsigned int code_bytes = 0;
			ReadFromStream(streamInfo.m_compressedStream, &code_bytes, sizeof(unsigned int));
			
			unsigned char * code_buffer = new unsigned char [code_bytes + EXTRA_MEM_ALLOCATED];                
			memset(code_buffer, 0, code_bytes + EXTRA_MEM_ALLOCATED);
			ReadFromStream(streamInfo.m_compressedStream, code_buffer, sizeof(unsigned char)*code_bytes);
			
			Adaptive_Data_Model mModel(1024);
			Adaptive_Data_Model mSign (2);
			Adaptive_Data_Model mhasnext (2);
			Arithmetic_Codec acd(code_bytes, code_buffer);
			acd.start_decoder();	// initialize decoder

			int data[4];
			data[0] = 1;
			data[1] = 1<<10;
			data[2] = 1<<20;
			data[3] = 1<<30;
			int count = 0;
			for(int i = 0 ; i < numberOfdata*dim; i++)
			{
				difValue = 0;
				while(1)
				{
					difValue += data[count] * acd.decode(mModel);
					count++;
					int temp = acd.decode(mhasnext);

					if( temp ==0)
						break;
				}
				count = 0;

				if(difValue != 0)
				{
					nSign = acd.decode(mSign);
					if(nSign == 1)
					{
						difValue = -difValue;
					}
				}
				intArray[i] = difValue;
				
			}
			delete [] code_buffer;
		}
		else if(predictionMode == XORPrediction)
		{
//			unsigned int nSign;
			int difValue,  nQuantValue;
			nQuantValue=0;
			unsigned int code_bytes = 0;
			ReadFromStream(streamInfo.m_compressedStream, &code_bytes, sizeof(unsigned int));
			
			unsigned char * code_buffer = new unsigned char [code_bytes+ EXTRA_MEM_ALLOCATED];                                                   
			memset(code_buffer, 0, code_bytes + EXTRA_MEM_ALLOCATED);
			ReadFromStream(streamInfo.m_compressedStream, code_buffer, sizeof(unsigned char)*code_bytes);
			
			Adaptive_Data_Model mModel(1024);
			Adaptive_Data_Model mhasnext (2);
			Arithmetic_Codec acd(code_bytes, code_buffer);
			acd.start_decoder();	// initialize decoder

			int data[4];
			data[0] = 1;
			data[1] = 1<<10;
			data[2] = 1<<20;
			data[3] = 1<<30;
			int count = 0;
			for(int i = 0 ; i < numberOfdata*dim; i++)
			{
				difValue = 0;
				while(1)
				{
                    //unsigned int t;
					difValue += data[count] * acd.decode(mModel);
					count++;
					int temp = acd.decode(mhasnext);
					if( temp ==0)
						break;
				}
				count = 0;
				intArray[i] = difValue;  
			}
			delete [] code_buffer;
		}
		else if(predictionMode == AdaptiveDifferentialPrediction)
		{
			int difValue,  nQuantValue;
			nQuantValue=0;
			unsigned int code_bytes = 0;
			ReadFromStream(streamInfo.m_compressedStream, &code_bytes, sizeof(unsigned int));
			
			unsigned char * code_buffer = new unsigned char [code_bytes+ EXTRA_MEM_ALLOCATED];
			memset(code_buffer, 0, code_bytes + EXTRA_MEM_ALLOCATED);
			ReadFromStream(streamInfo.m_compressedStream, code_buffer, sizeof(unsigned char)*code_bytes);
			
			Adaptive_Data_Model mModel(1024);
			Adaptive_Data_Model mPred (8);
			Adaptive_Data_Model mhasnext (2);
			Arithmetic_Codec acd(code_bytes, code_buffer);
			acd.start_decoder();	// initialize decoder

			int data[4];
			data[0] = 1;
			data[1] = 1<<10;
			data[2] = 1<<20;
			data[3] = 1<<30;
			int count = 0;
			const int sign[2]={1, -1};
			for(int i = 0 ; i < numberOfdata*dim; i++)
			{			
				preds[i] = acd.decode(mPred);		
				difValue = 0;
				while(1)
				{
					difValue += data[count] * acd.decode(mModel);
					count++;
					int temp = acd.decode(mhasnext);		
					if( temp ==0)
						break;
				}
				count = 0;
				if ( preds[i] != 0) 
					intArray[i] = sign[difValue % 2] * ((difValue+1)/2);
				else 
					intArray[i] = difValue;
				
			}
			delete [] code_buffer;
		}
	}
	else if(binarizationMode == FC)// 4C
	{
		const int sign[2]={1, -1};
		unsigned int pValueOpt = 0;
		unsigned int predOpt = 0;
		unsigned int lowHigh = 0;
		unsigned char temp = 0;
		int k = 0;
		unsigned int code_bytes = 0;
		ReadFromStream(streamInfo.m_compressedStream, &code_bytes, sizeof(unsigned int));
		printf("4c_bytes:%d\n", code_bytes);
		if (predictionMode == AdaptiveDifferentialPrediction)
		{
			for(int i = 0 ; i < numberOfdata*dim; i++)
			{
				if(lowHigh == 0) 
					temp = (*(streamInfo.m_compressedStream))& 15;
				else 
				{
					temp=((*(streamInfo.m_compressedStream)++)& 240)>>4;					
				}
				lowHigh=(lowHigh+1)%2;

				predOpt = temp & 7;
				pValueOpt = 0;
				temp >>= 3;
				pValueOpt += temp;
				k = 0;
				while (temp)
				{
					if(lowHigh == 0) 
						temp = (*(streamInfo.m_compressedStream))& 15;
					else 
					{
						temp=((*(streamInfo.m_compressedStream)++)& 240)>>4;						
					}
					lowHigh=(lowHigh+1)%2;

					pValueOpt += (temp & 7) << k;
					k+=3;
					temp >>= 3;
					pValueOpt += temp << k;
				}
				preds[i] = predOpt;
			
				if ( predOpt > 0) intArray[i] = sign[pValueOpt % 2] * ((pValueOpt+1)/2);
				else intArray[i] = pValueOpt;
			}
		}
		else
		{
			for(int i = 0 ; i < numberOfdata*dim; i++)
			{
				k=0;
				pValueOpt = 0;
				temp = 1;
				while(temp)
				{
					READ_TEMP;
					pValueOpt += (temp & 7) << k;
					k+=3;
					temp >>= 3;
					pValueOpt += temp << k;
				}
				
				if (predictionMode == NoPrediction) intArray[i] = pValueOpt;
				else intArray[i] = sign[pValueOpt % 2] * ((pValueOpt+1)/2);
			}
		}
		streamInfo.m_compressedStream++;
	}
	else if(binarizationMode == AC_EGC)// AC/EGC
	{
		const int sign[2]={1, -1};
		unsigned int code_bytes = 0;
		ReadFromStream(streamInfo.m_compressedStream, &code_bytes, sizeof(unsigned int));

		unsigned char * code_buffer = new unsigned char [code_bytes + EXTRA_MEM_ALLOCATED];
		memset(code_buffer, 0, code_bytes + EXTRA_MEM_ALLOCATED);
		ReadFromStream(streamInfo.m_compressedStream, code_buffer, sizeof(unsigned char)*code_bytes);

		Arithmetic_Codec acd(code_bytes, code_buffer);
		acd.start_decoder();	// initialize decoder

		unsigned int exp_k = acd.ExpGolombDecode(0);
		unsigned int M = acd.ExpGolombDecode(0);

		Adaptive_Data_Model mModelValues(M+2);
		unsigned int pValueOpt = 0;
		if (predictionMode == AdaptiveDifferentialPrediction)
		{
			unsigned int maxV = acd.ExpGolombDecode(0);
			Adaptive_Data_Model mModel(maxV);
			for(int i = 0 ; i < numberOfdata*dim; i++)
			{	
				preds[i] = acd.decode(mModel);
				
				pValueOpt = acd.decode(mModelValues);
				if ( pValueOpt == M+1) pValueOpt += acd.ExpGolombDecode(exp_k);
				
 				if ( preds[i] != 0) intArray[i] = sign[pValueOpt % 2] * ((pValueOpt+1)/2);
				else intArray[i] = pValueOpt;
			}
		}
		else
		{
			for(int i = 0 ; i < numberOfdata*dim; i++)
			{
				pValueOpt = acd.decode(mModelValues);
				if (pValueOpt == M+1) pValueOpt += acd.ExpGolombDecode(exp_k);
				if (predictionMode == NoPrediction) intArray[i] = pValueOpt;
				else intArray[i] = sign[pValueOpt % 2] * ((pValueOpt+1)/2);
			}
		}
		delete [] code_buffer;
	}
}

//dmlab
// -> need parameter quantizationMode

void EncodeFloatArray(EncoderStreamInfo &streamInfo, float * floatArray, int numberOfdata, int dim, int predictionMode, int binarizationMode, int quantizationMode, int* nQBits, float *quantMin, 
	float *quantRange, int nTriangles, int * triangles, MultiVectOpt * tfans, MultiVectOpt * V2T)
{
	int * intArray = new int[numberOfdata * dim];
	if(quantizationMode == UniformQuantization)
	{
		QuantizeFloatArray(floatArray, numberOfdata, dim, nQBits[0], quantMin, quantRange, intArray);
	}
	else if(quantizationMode == NormalQuantization)
	{
		QuantizeFloatArray(floatArray, numberOfdata, nQBits[0], intArray);
		dim = 1;  // added to be coherent with the normals encoding
				  // one value is associated to each normal (3 coordinates)
	}
	else if(quantizationMode == UniformTextureQuantization)
	{
		int tQP[2]={0,0};
		tQP[0] = nQBits[1];
		tQP[1] = nQBits[2];
		QuantizeFloatArray(floatArray, numberOfdata, dim, tQP, intArray);
	}
	
	EncodeIntArray(streamInfo, intArray, numberOfdata, dim, predictionMode, binarizationMode, nQBits[0], (1<<nQBits[0]),  nTriangles, triangles, tfans, V2T );
	delete [] intArray;
}
//~dmlab

void EncodeIntArray(EncoderStreamInfo &streamInfo, int * intArray, int numberOfdata, int dim, int predictionMode, int binarizationMode, int nQBits, int sizeOfData,
	int nTriangles, int * triangles, MultiVectOpt * tfans, MultiVectOpt * V2T)
{
	int * intArrayPredicted = new int[numberOfdata * dim];
	int * preds = new int[numberOfdata * dim];
	PredictIntArray(streamInfo, intArray, numberOfdata, dim, predictionMode, intArrayPredicted, preds, nTriangles, triangles, tfans, V2T, sizeOfData);
	BinarizeIntArray(streamInfo, intArrayPredicted, preds, numberOfdata, nQBits, dim, predictionMode, binarizationMode);	

	delete [] intArrayPredicted;
	delete [] preds;
}


void QuantizeFloatArray(float * floatArray, int numberOfdata, int dim, int nQBits, float *quantMin, float *quantRange, int * intArray)
{
	float * delta = new float[dim]; 

	for(int d = 0; d < dim; d++)
	{
		if (quantRange[d] > 0.0f) delta[d] = (float)((1 << nQBits) - 1) / quantRange[d];
		else delta[d] = 1.0f;
	}

	
		for(int i = 0; i < numberOfdata * dim; i++)
		{	
			intArray[i] = (int)((floatArray[i]-quantMin[i%dim]) * delta[i%dim] + 0.5f);
			//printf("array[%d]:%d\n",i,intArray[i]);
		}


	delete [] delta;
}

//dmlab
void QuantizeFloatArray(float * floatArray, int numberOfdata, int dim, int* nQBits,int * intArray)
{	
	for(int i = 0; i < numberOfdata * dim; i++)
	{
		intArray[i] = (int)(floatArray[i]*nQBits[i%dim]);
	}
}
//~dmlab

//dmlab
void QuantizeFloatArray(float * floatArray, int numberOfdata, int nQBits, int * intArray)
{

	int subdivision = (nQBits-3) / 2;
	for(int i = 0; i < numberOfdata; i++)
	{
		intArray[i] = _normal2code(floatArray+(i*3),subdivision);
	}
}
//~dmlab



//dmlab
unsigned long _normal2code(const float *normal,unsigned int subdivision)
{
	  float vx = normal[0], vy = normal[1], vz = normal[2];
	  unsigned long octantCode = 0;

	  // Generate bits b30, b29, b28 of code (0: positive, 1: negative)
	  if (vx < 0) { vx = -vx; octantCode  = 0x00000004; }
	  if (vy < 0) { vy = -vy; octantCode |= 0x00000002; }
	  if (vz < 0) { vz = -vz; octantCode |= 0x00000001; }

	  // Convert coordinates to integer values
	  unsigned long maxCoord = 1 << subdivision;
	  float s = maxCoord / (vx + vy + vz);
	  unsigned long x = (unsigned long)(vx*s);
	  unsigned long y = (unsigned long)(vy*s);
	  unsigned long z = (unsigned long)(maxCoord-vx*s-vy*s); //  long z = vz * s;

	  short upsideDown = 0;

	  if (maxCoord - x - y - z == 2) upsideDown = 1;
	  else if (x == maxCoord ) x -= 1;
	  else if ( x + y == maxCoord ) y -= 1;

	  unsigned long tricode = y * (2 * maxCoord - 1) - y * (y - 1) +    2 * x + upsideDown;

	  unsigned long code =  (octantCode << 2*subdivision) | tricode;
 
	  return code;
}

//~dmlab



//dmlab
void _code2Normal(const unsigned long code, float *normal, unsigned int subdivision)
{
  unsigned long mask =  (1 << (2 * subdivision)) - 1;
  unsigned long tricode = code & mask;
  unsigned long tcode = tricode;

  // Find y coordinate by solving 2nd degree equation
  // long factor = 1 << (numLevels + 1);
  // long y = (factor - sqrt(factor * factor - 4 * tricode)) / 2.0;
  unsigned long factor = 1 << subdivision;
  unsigned long y = (unsigned long)(factor-sqrt((double)factor*factor-tricode));
  tricode += y * ( y - 2 * factor);
  unsigned long x = tricode / 2;
  short upsideDown = (short) (tricode % 2);

  // Calculate coordinates for all vertices in triangle
  float v1x = float(x + upsideDown);
  float v1y = float(y + upsideDown);
  float v2x = float(x + 1);
  float v2y = float(y);
  float v3x = float(x);
  float v3y = float(y + 1);

  // Calculate coordinates of barycenter
  float invMaxCoord = (float) (1.0 / factor);
  normal[0] = (v1x + v2x + v3x) * invMaxCoord;
  normal[1] = (v1y + v2y + v3y) * invMaxCoord;
  normal[2] = (float) (3.0 - normal[0] - normal[1]);

  // Flip component signs if necessary
  // unsigned long octantCode = (code & ~mask) >> 2*_nSubdivisionLevels;
  unsigned long octantCode = (code >> 2*subdivision)&0x7;

  if (octantCode & 0x4) normal[0] = -normal[0];
  if (octantCode & 0x2) normal[1] = -normal[1];
  if (octantCode & 0x1) normal[2] = -normal[2];

  //Normalize vector
  float invNorm = (float) (1.0 / sqrt(normal[0]*normal[0] +
			     normal[1]*normal[1] +
			     normal[2]*normal[2]));
  normal[0] *= invNorm; normal[1] *= invNorm; normal[2] *= invNorm;
}

//~dmlab


void BinarizeIntArray(EncoderStreamInfo &streamInfo, int * intArray, int * preds, int numberOfdata, int nQBits, int dim, int predictionMode, int binarizationMode, unsigned int M)
{
	unsigned char mask = predictionMode & 7;
	mask += (binarizationMode & 7)<<4;
	Write2Stream(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize, &mask, sizeof(unsigned char));

	if ((binarizationMode == FL) && (predictionMode == NoPrediction)) // Fixed Length
	{
		unsigned char ucnQBits = nQBits;		
		unsigned int beforelength;
		unsigned int encoded_length;
		unsigned int a_encoded_length;
        InsertData(streamInfo,0x0, 32);//coord header
        beforelength=streamInfo.m_compressedStreamSize;
		Write2Stream(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize, &ucnQBits, sizeof(unsigned char));
		
		for(int i = 0 ; i < numberOfdata*dim; i++)
		{
			InsertData(streamInfo, intArray[i], nQBits);
		}
		encoded_length=streamInfo.m_compressedStreamSize-beforelength;
        a_encoded_length=encoded_length;
        if(streamInfo.m_bitPostion != 0)
            a_encoded_length++;
        memcpy( streamInfo.m_compressedStream - (encoded_length+4), &a_encoded_length, 4 );
		
		Flush(streamInfo);
		
	}
	else if(binarizationMode == BP) // to be updated
	{
        unsigned int encoded_length,a_encoded_length;
        unsigned int beforelength;
        InsertData(streamInfo,0x0, 32);//coord header
        beforelength=streamInfo.m_compressedStreamSize;
		int maxDifferential = 0;
		for(int i = 0; i < numberOfdata * dim; i++)
		{		
			if(maxDifferential < abs(intArray[i])) maxDifferential = abs(intArray[i]);
		}
		

		if ((predictionMode == DifferentialPrediction) || (predictionMode == TFANParallelogramPrediction) || (predictionMode == CircularDifferentialPrediction) || (predictionMode == CircularDifferentialPrediction)) 
		{
			int maxValue = maxDifferential+1;
			int * BPLTable = new int [maxValue];
			int * payloadTable = new int [maxValue];
			int i = 0;
			for(i = 0 ; i < 3; i++)
			{
				if(i == maxValue)
					break;
				BPLTable[i] = i;
				payloadTable[i] = -1;
			}
			int payloadLength = 4;
			int minValueInPresentBPL = 3;
			int presentBPL = 3;
			for( ; i<maxValue; i++)
			{
				if( i < 3)
				{
					BPLTable[i] = i;
					payloadTable[i] = -1;
				}
				if(i == payloadLength+minValueInPresentBPL)
				{
					minValueInPresentBPL += payloadLength;
					payloadLength *= 2;
					presentBPL++;
				}
				BPLTable[i] = presentBPL;
				payloadTable[i] = i - minValueInPresentBPL;
			}

			unsigned int nMaxCheckBPL = int(log((float)(maxDifferential)) / log((float)2)) +1;
			unsigned int prefix_size = int(log((float)(nMaxCheckBPL)) / log((float)2)) +1;	

			InsertData(streamInfo, prefix_size, LEN_PREFIX);
			Flush(streamInfo); 

			int nSymbol, nSign;
			for(i = 0 ; i < numberOfdata*dim; i++)
			{
				nSymbol = intArray[i];		
				if(nSymbol < 0)
				{
					nSymbol = -nSymbol;
					nSign = 1;
				}
				else if(nSymbol > 0)
					nSign = 0;
				else
					nSign = 2;
				InsertData(streamInfo, BPLTable[nSymbol],prefix_size);
				if(payloadTable[nSymbol] != -1)
					InsertData(streamInfo, payloadTable[nSymbol], (BPLTable[nSymbol]-1));
				if(nSign != 2)
				{
					InsertData(streamInfo, nSign, 1);
				}
			}
			delete [] BPLTable;
			delete [] payloadTable;
		}
		else if (predictionMode == XORPrediction)
		{
			int maxValue = (maxDifferential+1)<<1;
			int * BPLTable = new int [maxValue];
			int * payloadTable = new int [maxValue];
			int i = 0;
			for(i = 0 ; i < 3; i++)
			{
				if(i == maxValue)
					break;
				BPLTable[i] = i;
				payloadTable[i] = -1;
			}
			int payloadLength = 4;
			int minValueInPresentBPL = 3;
			int presentBPL = 3;
			for( ; i<maxValue; i++)
			{
				if( i < 3)
				{
					BPLTable[i] = i;
					payloadTable[i] = -1;
				}
				if(i == payloadLength+minValueInPresentBPL)
				{
					minValueInPresentBPL += payloadLength;
					payloadLength *= 2;
					presentBPL++;
				}
				BPLTable[i] = presentBPL;
				payloadTable[i] = i - minValueInPresentBPL;
			}

			unsigned int nMaxCheckBPL = int(log((float)(maxValue)) / log((float)2)) +1;
			unsigned int prefix_size = int(log((float)(nMaxCheckBPL)) / log((float)2)) +1;	

			InsertData(streamInfo, prefix_size, LEN_PREFIX);
			Flush(streamInfo); 

			int nSymbol;
			for(i = 0 ; i < numberOfdata*dim; i++)
			{
				nSymbol = intArray[i];		
				InsertData(streamInfo, BPLTable[nSymbol],prefix_size);
				if(payloadTable[nSymbol] != -1)
				{
					InsertData(streamInfo, payloadTable[nSymbol], (BPLTable[nSymbol]-1));

				}
			}
			delete [] BPLTable;
			delete [] payloadTable;

		}
		else if (predictionMode == AdaptiveDifferentialPrediction)
		{
			int maxValue = (maxDifferential+1)*2;
			int * BPLTable = new int [maxValue];
			int * payloadTable = new int [maxValue];
			int i = 0;
			for(i = 0 ; i < 3; i++)
			{
				if(i == maxValue)
					break;
				BPLTable[i] = i;
				payloadTable[i] = -1;
			}
			int payloadLength = 4;
			int minValueInPresentBPL = 3;
			int presentBPL = 3;
			for( ; i<maxValue; i++)
			{
				if(i == payloadLength+minValueInPresentBPL)
				{
					minValueInPresentBPL += payloadLength;
					payloadLength *= 2;
					presentBPL++;
				}
				BPLTable[i] = presentBPL;
				payloadTable[i] = i - minValueInPresentBPL;
			}
			unsigned int nMaxCheckBPL = int(log((float)(maxValue)) / log((float)2)) +1;
			unsigned int prefix_size = int(log((float)(nMaxCheckBPL)) / log((float)2)) +1;	
			InsertData(streamInfo, prefix_size, LEN_PREFIX);
			Flush(streamInfo); 

			unsigned int pValueOpt = 0;
			int nSymbol;

			for(i = 0 ; i < numberOfdata*dim; i++)
			{
				if (preds[i] != 0)		// if prediction applied we need to convert the 
				{						// signed residual into an unsigned int
					if (intArray[i]>=0) pValueOpt = 2* intArray[i];
					else pValueOpt = -2* intArray[i]-1;
				}
				else
				{
					pValueOpt = intArray[i];
				}
				nSymbol = preds[i];
				while(1)
				{
					if(nSymbol < 3)
					{
						InsertData(streamInfo, nSymbol, 2);
						break;
					}
					InsertData(streamInfo, 3, 2);
					nSymbol -= 3;					
				}	
				InsertData(streamInfo, BPLTable[pValueOpt],prefix_size);
				if(payloadTable[pValueOpt] != -1)
					InsertData(streamInfo, payloadTable[pValueOpt], (BPLTable[pValueOpt]-1));
		
			}
			delete [] BPLTable;
			delete [] payloadTable;
		}
       encoded_length=streamInfo.m_compressedStreamSize-beforelength;
        a_encoded_length=encoded_length;
        if(streamInfo.m_bitPostion != 0)
            a_encoded_length++;
        memcpy( streamInfo.m_compressedStream - (encoded_length+4), &a_encoded_length, 4 );
		Flush(streamInfo);
	}
	else if(binarizationMode == AC)// to be updated
	{
		if ((predictionMode == DifferentialPrediction) || 
			(predictionMode == TFANParallelogramPrediction) || 
			(predictionMode == CircularDifferentialPrediction) ) 
		{
			int nSymbol, nSign;
			unsigned int NMAX = numberOfdata*dim * 4 + 100;
			Adaptive_Data_Model mModel(1024);
			Adaptive_Data_Model mSign (2);
			Adaptive_Data_Model mhasnext (2);
			Arithmetic_Codec ace(NMAX, NULL);
			ace.start_encoder();
			for(int i = 0 ; i < numberOfdata*dim; i++)
			{
				nSymbol = intArray[i];
				if(nSymbol < 0)
				{
					nSymbol = -nSymbol;
					nSign = 1;
				}
				else if(nSymbol > 0)
					nSign = 0;
				else
					nSign = 2;
				while(1)
				{
					ace.encode(nSymbol%1024, mModel);
					nSymbol/=1024;
					if(nSymbol)
						ace.encode(1, mhasnext);
					else
						break;
				}
				ace.encode(0, mhasnext);
				if(nSign != 2)
					ace.encode(nSign, mSign);
			}
			ace.write_to_mem(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize);
		}
		else if(predictionMode == XORPrediction)
		{
			int nSymbol;
			unsigned int NMAX = numberOfdata*dim * 4 + 100;
			Adaptive_Data_Model mModel(1024);
			Adaptive_Data_Model mhasnext (2);
			Arithmetic_Codec ace(NMAX, NULL);
			ace.start_encoder();
			for(int i = 0 ; i < numberOfdata*dim; i++)
			{
				nSymbol = intArray[i];
				while(1)
				{
					ace.encode(nSymbol%1024, mModel);
					nSymbol/=1024;
					if(nSymbol)
						ace.encode(1, mhasnext);
					else
						break;
				}
				ace.encode(0, mhasnext);
			}
			ace.write_to_mem(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize);
		}
		else if(predictionMode == AdaptiveDifferentialPrediction)
		{
			unsigned int pValueOpt = 0;
			unsigned int NMAX = numberOfdata*dim * 4 + 100;
			Adaptive_Data_Model mModel(1024);
			Adaptive_Data_Model mhasnext (2);
			Adaptive_Data_Model mPred (8);
			Arithmetic_Codec ace(NMAX, NULL);
			ace.start_encoder();
			for(int i = 0 ; i < numberOfdata*dim; i++)
			{				
				if (preds[i] != 0)		// if prediction applied we need to convert the 
				{						// signed residual into an unsigned int
					if (intArray[i]>=0) pValueOpt = 2* intArray[i];
					else pValueOpt = -2* intArray[i]-1;
				}
				else
				{
					pValueOpt = intArray[i];
				}
				ace.encode(preds[i], mPred);

				while(1)
				{
					ace.encode(pValueOpt%1024, mModel);
					pValueOpt/=1024;
					if(pValueOpt)
						ace.encode(1, mhasnext);
					else
						break;
				}
				ace.encode(0, mhasnext);
			}
			ace.write_to_mem(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize);
		}
	}
	else if(binarizationMode == FC)// 4C
	{
		unsigned char * buffer;
		unsigned int code_bytes = 0;		
		unsigned int pValueOpt = 0;
		unsigned int lowHigh = 0;
		unsigned char temp = 0;
		int beforelength;
		unsigned int codeBytes = 0x0;

		Flush(streamInfo);
		Write2Stream(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize, &codeBytes, sizeof(unsigned int));
		beforelength=streamInfo.m_compressedStreamSize;

		buffer = streamInfo.m_compressedStream;
		buffer[0] = 0;

		if (predictionMode == AdaptiveDifferentialPrediction)
		{
			for(int i = 0 ; i < numberOfdata*dim; i++)
			{
				temp = static_cast<unsigned char>(preds[i]);
				if (preds[i] != 0)		// if prediction applied we need to convert the 
				{						// signed residual into an unsigned int
					if (intArray[i]>=0) pValueOpt = 2* intArray[i];
					else pValueOpt = -2* intArray[i]-1;
				}
				else
				{
					pValueOpt = intArray[i];
				}

				if (pValueOpt) 
				{
					temp+=8;
					WRITE_TEMP;
					pValueOpt--;
					
					do
					{
						temp = (pValueOpt & 7);
						pValueOpt >>= 3;
						if (pValueOpt != 0) 
						{
							temp+=8;
							WRITE_TEMP;
							pValueOpt--;
						}
						else
						{
							WRITE_TEMP;
							break;
						}
					}
					while (1);
				}
				else
				{
					WRITE_TEMP;
				}
			}
		}
		else
		{
			for(int i = 0 ; i < numberOfdata*dim; i++)
			{
				if (predictionMode == NoPrediction)
				{
					pValueOpt = intArray[i];
				}
				else
				{
					if (intArray[i]>=0) pValueOpt = 2* intArray[i];
					else pValueOpt = -2* intArray[i]-1;
				}
				do
				{
					temp = (pValueOpt & 7);
					pValueOpt >>= 3;
					if (pValueOpt != 0) 
					{
						temp+=8;
						WRITE_TEMP;
						pValueOpt--;
					}
					else
					{
						WRITE_TEMP;
						break;
					}
				}
				while (1);
			}
		}
		code_bytes++;
		streamInfo.m_compressedStream += code_bytes;
		streamInfo.m_compressedStreamSize += code_bytes;
		{
			int encoded_length=streamInfo.m_compressedStreamSize-beforelength;
			int a_encoded_length=encoded_length;
			if(streamInfo.m_bitPostion != 0)
				a_encoded_length++;
			memcpy( streamInfo.m_compressedStream - (encoded_length+4), &a_encoded_length, 4 );
		}
		Flush(streamInfo);
	}
	else if(binarizationMode == AC_EGC)// AC/EGC
	{
		unsigned int sizeArray = numberOfdata*dim;
		unsigned int NMAX = (int) sizeArray * 4+100;
		unsigned int maxV = 8;
		unsigned int exp_k = 8;
		unsigned int pValueOpt = 0;

		unsigned char * buffer = streamInfo.m_compressedStream;
		Arithmetic_Codec ace;
		ace.set_buffer(NMAX, NULL);
		ace.start_encoder();
		Adaptive_Data_Model mModelValues(M+2);


		// Compute best encoding EGk parameter
		std::map<int, int> freq;
		for(int i = 0 ; i < numberOfdata*dim; i++)
		{
			if ( ((predictionMode == AdaptiveDifferentialPrediction) && (preds[i] == 0)) || 
				 ((predictionMode == NoPrediction)))
				// if prediction applied we need to convert the 
			{	// signed residual into an unsigned int
				pValueOpt = intArray[i];
			}
			else
			{
				if (intArray[i]>=0) pValueOpt = 2* intArray[i];
				else pValueOpt = -2* intArray[i]-1;
			}

			if (pValueOpt > M) {
				freq[pValueOpt-M-1]++;
			}	
		}

		double bestSize = 0;
		double currentSize = 0;
		double  x = 0.0;
		double n = 0.0;
		double c = 0.0;
		for(unsigned int k = 0; k < 16; k++)
		{
			currentSize = 0;
			c = (1<<k);
			for(std::map<int, int>::iterator it = freq.begin(); it != freq.end(); it++)
			{
				x = it->first;
				n = it->second;
				currentSize += n * ( 2.0 * (static_cast<int>(log(x/c+1.0) / log(2.0)))+k+1.0);
			}
			if ((k==0) || (currentSize < bestSize))
			{
				bestSize = currentSize;
				exp_k = k;
			}
			else
			{
				break;
			}
		}

		ace.ExpGolombEncode(exp_k, 0);
		ace.ExpGolombEncode(M, 0);
		
		if (predictionMode == AdaptiveDifferentialPrediction)
		{
			Adaptive_Data_Model mModel(maxV);
			ace.ExpGolombEncode(maxV, 0);		

			for(int i = 0 ; i < numberOfdata*dim; i++)
			{
				if (preds[i] != 0)
									// if prediction applied we need to convert the 
				{					// signed residual into an unsigned int
					if (intArray[i]>=0) pValueOpt = 2* intArray[i];
					else pValueOpt = -2* intArray[i]-1;
				}
				else
				{
					pValueOpt = intArray[i];
				}

				ace.encode(preds[i], mModel);

				if (pValueOpt <= M) {
					ace.encode(pValueOpt, mModelValues);
				}
				else {
					ace.encode(M+1, mModelValues);
					ace.ExpGolombEncode(pValueOpt-M-1, exp_k);
				}	
			}
		}
		else
		{
			for(int i = 0 ; i < numberOfdata*dim; i++)
			{
				if (predictionMode == NoPrediction)  
					// if prediction applied we need to convert the 
				{	// signed residual into an unsigned int
					pValueOpt = intArray[i];
				}
				else
				{
					if (intArray[i]>=0) pValueOpt = 2* intArray[i];
					else pValueOpt = -2* intArray[i]-1;
				}
				if (pValueOpt <= M) {
					ace.encode(pValueOpt, mModelValues);
				}
				else {
					ace.encode(M+1, mModelValues);
					ace.ExpGolombEncode(pValueOpt-M-1, exp_k);
				}	
			}
		}
		ace.write_to_mem(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize);
	}
}

void InsertData(EncoderStreamInfo &streamInfo, int symbol, int length)
{
	int i;
	int comparison = 1 << length;
	for(i=0; i < length; i++)
	{
		streamInfo.m_dumpBit <<= 1;
		comparison >>= 1;
		streamInfo.m_bitPostion++;
		streamInfo.m_dumpBit += (symbol & comparison) ?  1:0;
		if(streamInfo.m_bitPostion == LEN_BYTE)
		{
			streamInfo.m_bitPostion = 0;
			Write2Stream(streamInfo.m_compressedStream, streamInfo.m_compressedStreamSize, &streamInfo.m_dumpBit, sizeof(unsigned char));
			streamInfo.m_dumpBit = INIT_UCHAR;
		}
	}
}
void Flush(EncoderStreamInfo &streamInfo)
{
	if(streamInfo.m_bitPostion == 0)
		return;
	InsertData(streamInfo, 0, LEN_BYTE - streamInfo.m_bitPostion);
}
void Flush(DecoderStreamInfo &streamInfo)
{
	if(streamInfo.m_unCurrentBitPosition != 7)
	{
		streamInfo.m_unCurrentBitPosition = 7;
		streamInfo.m_unCurrentBufferPosition++;
	}
	streamInfo.m_compressedStream += streamInfo.m_unCurrentBufferPosition;
	streamInfo.m_unCurrentBufferPosition = 0;
}

//dmlab

void PredictIntArray(EncoderStreamInfo &streamInfo, int * intArray, int numberOfdata, int dim, int predictionMode, int * intArrayPredicted, int * preds,
	int nTriangles, int * triangles, MultiVectOpt * tfans, MultiVectOpt * V2T, int sizeOfData)
{
	if(predictionMode == NoPrediction)  
	{
		for(int i = 0; i < numberOfdata * dim; i++)
		{
			intArrayPredicted[i] = intArray[i];
		}
	}
	else if(predictionMode == DifferentialPrediction)  
	{
		for(int i = 0; i < numberOfdata * dim; i++)
		{
			if (i >= dim) intArrayPredicted[i] = intArray[i] - intArray[i-dim];
			else intArrayPredicted[i] = intArray[i];
		}
	}
	else if(predictionMode == XORPrediction) 
	{
		for(int i = 0; i < numberOfdata * dim; i++)
		{
			if (i >= dim) intArrayPredicted[i] = intArray[i] ^ intArray[i-dim];
			else intArrayPredicted[i] = intArray[i];
		}
	}
	else if(predictionMode == AdaptiveDifferentialPrediction)
	{
		int maxPred = 7;
		int pValue = 0;
		int pValueOpt = 0;
		int pred = 0;
		int predOpt = 0;
		int sizeOpt = 0;
		int size = 0;
		for(int i = 0; i < numberOfdata * dim; i++)
		{
			// no prediction
			pred = predOpt = 0;
			pValueOpt = pValue = intArray[i];
			sizeOpt = pValueOpt;

			// order 0 prediction
			for (int k = 1; (k <= maxPred) && (i >= k * dim); k++) {
				pred = k;
				pValue = (intArray[i] - intArray[i-k*dim]);
				if (pValue>=0) size = 2* pValue;
				else size = -2* pValue-1;
				if (size < sizeOpt){
					sizeOpt = size;
					predOpt = pred;
					pValueOpt = pValue;
				}
			}

			intArrayPredicted[i] = pValueOpt;
			preds[i] = predOpt;
		}
	}
	else if(predictionMode == CircularDifferentialPrediction)
	{

		int halfMax = sizeOfData/2;
		for(int i = 0; i < numberOfdata * dim; i++)
		{
			if (i >= dim)
			{
				intArrayPredicted[i] = intArray[i] - intArray[i-dim];
				if(abs(intArrayPredicted[i]) > halfMax)
				{
					if(intArray[i] > intArray[i-dim])
					{
						intArrayPredicted[i] = - sizeOfData + intArray[i] - intArray[i-dim] ;
					}
					else
					{
						intArrayPredicted[i] = sizeOfData + intArray[i] - intArray[i-dim] ;
					}
				}
				
			}
			else intArrayPredicted[i] = intArray[i];
		}
	}
	else
	{
		ConnectivityBasedPredictor myPredictor(intArray, numberOfdata, dim, nTriangles, triangles, predictionMode, intArrayPredicted, tfans, V2T);
		myPredictor.Predict();
	}
}

//~dmlab

