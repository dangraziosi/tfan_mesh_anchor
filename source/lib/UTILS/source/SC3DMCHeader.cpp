//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom, ETRI and Hanyang University 2008, 2009
// Licensed Materials, Program Property of Institut Telecom, ETRI and Hanyang University
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// ETRI and Hanyang University retain full right to use the code 
// for his/her own purpose, assign or donate the code to a third party and
// to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//    Marius Preda
//    Seung Wook Lee
//    Daiyong Kim
//    KyoungSoo Son
//
//
//    http://www-artemis.it-sudparis.eu/
//    http://www.etri.re.kr/
//    http://dmlab.hanyang.ac.kr/
//    
//  
//    Marius Preda       (marius.preda@int-evry.fr)
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//    Seung Wook Lee     (tajinet@etri.re.kr)  
//    Daiyong Kim        (dykim@dmlab.hanyang.ac.kr)    
//    KyoungSoo Son      (bonokensin@dmlab.hanyang.ac.kr)
//
//-------------------------------------------------------------------------



#include "SC3DMCHeader.h"
#include "utils.h"

SC3DMCHeader::SC3DMCHeader()
{
	Initializate();
}

SC3DMCHeader::~SC3DMCHeader()
{
	if(quantMinOtherAttribute != NULL)
		delete [] quantMinOtherAttribute;
}

void SC3DMCHeader::Initializate()
{
	numberOfOtherAttribute = 0;
	numberOfOtherAttributeIndex = 0;
	dimensionOfOtherAttribute = 0;
	QPforGeometry = DEFAULTQP;
	QPforNormal = DEFAULTQP;
	QPforTexCoord = DEFAULTQP;
	TexCoordHeight = DEFAULTQP;
	TexCoordWidth = DEFAULTQP;
	QPforColor = DEFAULTQP;
	QPforOtherAttribute = DEFAULTQP; 
	for(int i = 0 ; i<3; i++)
	{
		quantMinGeometry[i] = 0;
		rangeGeometry[i] = 0;
		quantMinNormal[i] = 0;
		quantMinColor[i] = 0;
		rangeNormal[i] = 0;
		rangeColor[i] = 0;

	}
	for(int i = 0 ; i<2; i++)
	{
		rangeTexCoord[i] = 0;
		quantMinTexCoord[i] = 0;

	}
	otherAttributesPerVertex=false;
	isTriangularMesh=false;
	quantMinOtherAttribute = NULL;		
	rangeOtherAttribute = NULL;		

	quantmodeGeometry = 0;
	quantmodeNormal = 0;
	quantmodeColor = 0;
	quantmodeTexCoord = 0;
	quantmodeOtherAttribute = 0;

	quantRangeGeometry = 0;
	quantRangeNormal = 0;						
	quantRangeColor = 0;					
	quantRangeTexCoord = 0;				
	quantRangeOtherAttribute = 0;
}

