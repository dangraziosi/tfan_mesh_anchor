//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom 2008, 2009
// Licensed Materials, Program Property of Institut Telecom
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// retains full right to use the code for his/her own purpose, assign or donate 
// the code to a third party and to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//
//
//    http://www-artemis.it-sudparis.eu/
//    
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//
//-------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdarg.h>

#include "Console.h"


CConsole::CConsole(const char* filename)
{
	file_log = fopen(filename, "w");
}

void CConsole::ChangeFileName(const char* filename) {
	if (file_log != NULL) {
		fclose(file_log);
		file_log = fopen(filename, "w");
	}

}

CConsole::~CConsole()
{
  write_2_log("");
  write_2_log("END OF LOG");

  if (file_log) 
	  fclose(file_log);
}


void CConsole::write_2_log(char *fmt, ...)
{
  va_list	argptr;
  char		str[1024*4];	// 4 Kb should be enough

  // re-build the string
  va_start(argptr,fmt);
  vsprintf(str,fmt,argptr);
  va_end(argptr);

  printf("%s", str);
  if (!file_log) 
	  return;



  fprintf(file_log, "%s", str);
  fflush(file_log);
}

void CConsole::tic() {
	    start_ = clock();
}

void CConsole::toc() {
	finish_ = clock();
	write_2_log("%f s\n", ((double)(finish_ - start_))/CLOCKS_PER_SEC);
}
