//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom 2008, 2009
// Licensed Materials, Program Property of Institut Telecom
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// retains full right to use the code for his/her own purpose, assign or donate 
// the code to a third party and to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//
//
//    http://www-artemis.it-sudparis.eu/
//    
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//
//-------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "FileIO.h"


FileIO::FileIO(char * fileName, char * mode)
{
	strcpy(sep_, " ,[]{}\t");
	strcpy(fileName_, fileName);
	strcpy(mode_, mode);
	file_ = fopen(fileName_, mode);
	word_ = NULL;
	lineNbr_ = 0;
	wordNbr_ = 0;
	if (file_) {
//		printf("File %s opened (mode = %s)\n", fileName_, mode_);
	}
	else {
		printf("Can't open file %s (mode = %s)\n", fileName_, mode_);
	}
}

FileIO::~FileIO(void)
{
	if ( file_ != NULL) {
		fclose(file_);
	}
}

bool FileIO::ReadLine() {
		if (!feof(file_)) {	// we read a line
			fgets(line_ , FILEIO_MAX_BUFFER, file_);
			char *p = line_;
			while(*p) {
				if (*p == 10) {
					*p = ' ';
				}
				p++;
			}
//			printf("(line %i) \t %s\n", lineNbr_, line_);
			lineNbr_++;
			wordNbr_ = 0;
			return true;
		}
		else {
			return false;
		}

}
bool FileIO::GetWord(char * word) {
	if (wordNbr_ == 0) {
		word_ = strtok (line_, sep_);
	}
	else {

		if (word_ == NULL) {		// if we don't have nothing in the buffer
			return false;
		}
		word_ = strtok (NULL, sep_);
	}

	if (word_) {
		wordNbr_++;
		strcpy(word, word_);
		return true;
	}
	else {
		return false;
	}

}

bool FileIO::GetWordAll(char * word) {
	while ( (!GetWord(word)) )  {
		if (!ReadLine()) {
			return false;
		}
	}

	return true;
}
bool FileIO::Goto(char * target, char * word) {
	if (GetWordAll(word)) {
		while (strcmp(target, word)) {
			if (!GetWordAll(word)) {
				return false;
			}
		}
		return true;
	}
	return false;
}

bool FileIO::GotoEOL(char * word) {
	while (GetWord(word));
	return true;
}

bool FileIO::GetFloat(char * word, float & f) {
	if (sscanf(word, "%f", &f)) return true; 
	else return false;
}

bool FileIO::GetInt(char * word, int & i) {
	if (sscanf(word, "%i", &i)) return true; 
	else return false;
}

bool FileIO::GetFaceIndex(char* word, int& iV, int& iT, int& iN, int& iC)
{
	if (sscanf(word, "%i/%i/%i/%i", &iV, &iT, &iN, &iC)) return true;
	return false;
}
