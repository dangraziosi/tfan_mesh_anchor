//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom 2008, 2009
// Licensed Materials, Program Property of Institut Telecom
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// retains full right to use the code for his/her own purpose, assign or donate 
// the code to a third party and to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//
//
//    http://www-artemis.it-sudparis.eu/
//    
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//
//-------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <vector>
#include <algorithm>
#include <map>
#include <stdlib.h>
#include <time.h>

using namespace std;

#include "IndexedFaceSet.h"
#include "Console.h"
#include "FileIO.h"

#include "ostream.h"

#define VRML_HEADER "#VRML V2.0 utf8"

#define MAX_VLAUE 1000000
#define MIN_VLAUE -1000000


IndexedFaceSet::IndexedFaceSet(void)
{
	coord_ = NULL;			
	color_ = NULL;
	normal_ = NULL;
	texCoord_ = NULL;

	nCoord_ = 0;
	nColor_ = 0;
	nNormal_ = 0;
	nTexCoord_ = 0;

	ccw_ = true;
	solid_ = true;
	convex_ = true;
	colorPerVertex_ = true;
	normalPerVertex_ = true;

	creaseAngle_ = 0.0;

	coordIndex_ = NULL;
	colorIndex_ = NULL;
	normalIndex_ = NULL;
	texCoordIndex_ = NULL;

	nCoordIndex_ = 0;
	nColorIndex_ = 0;
	nNormalIndex_ = 0;
	nTexCoordIndex_ = 0;
}
IndexedFaceSet::~IndexedFaceSet(void)
{
	FreeMem();
}



bool IndexedFaceSet::GetCoord(int vertex, float * coord) {
	if (vertex < nCoord_) {
		for(int h = 0; h < 3; h++) {
			coord[h] = coord_[vertex*3+h];
		}
		return true;
	}
	return false;
}
bool IndexedFaceSet::SetCoord(int vertex, float * coord) {
	if (vertex < nCoord_) {
		for(int h = 0; h < 3; h++) {
			coord_[vertex*3+h] = coord[h];
		}
		return true;
	}
	return false;
}

bool IndexedFaceSet::GetNormal(int pos, float * normal) {
	if (pos < nNormal_) {
		for(int h = 0; h < 3; h++) {
			normal[h] = normal_[pos*3+h];
		}
		return true;
	}
	return false;
}
bool IndexedFaceSet::SetNormal(int pos, float * normal) {
	if (pos < nNormal_) {
		for(int h = 0; h < 3; h++) {
			normal_[pos*3+h] = normal[h];
		}
		return true;
	}
	return false;
}

bool IndexedFaceSet::GetColor(int pos, float * color) {
	if (pos < nColor_) {
		for(int h = 0; h < 3; h++) {
			color[h] = color_[pos*3+h];
		}
		return true;
	}
	return false;
}
bool IndexedFaceSet::SetColor(int pos, float * color) {
	if (pos < nColor_) {
		for(int h = 0; h < 3; h++) {
			color_[pos*3+h] = color[h];
		}
		return true;
	}
	return false;
}
bool IndexedFaceSet::GetTexCoord(int pos, float * texCoord) {
	if (pos < nTexCoord_) {
		for(int h = 0; h < 2; h++) {
			texCoord[h] = texCoord_[pos*2+h];
		}
		return true;
	}
	return false;
}
bool IndexedFaceSet::SetTexCoord(int pos, float * texCoord) {
	if (pos < nTexCoord_) {
		for(int h = 0; h < 2; h++) {
			texCoord_[pos*2+h] = texCoord[h];
		}
		return true;
	}
	return false;
}

bool IndexedFaceSet::GetCoordIndex(int pos, int * coordIndex) {
	if (pos < nCoordIndex_) {
		for(int h = 0; h < 3; h++) {
			coordIndex[h] = coordIndex_[pos*3+h];
		}
		return true;
	}
	return false;
}
bool IndexedFaceSet::SetCoordIndex(int pos, int * coordIndex) {
	if (pos < nCoordIndex_) {
		for(int h = 0; h < 3; h++) {
			coordIndex_[pos*3+h] = coordIndex[h];
		}
		return true;
	}
	return false;
}

bool IndexedFaceSet::GetNormalIndex(int pos, int * normalIndex) {
	int dim = 1;
	if (GetNormalPerVertex()) dim = 3;	

	if (pos < nNormalIndex_) {
		for(int h = 0; h < dim; h++) {
			normalIndex[h] = normalIndex_[pos*3+h];
		}
		return true;
	}
	return false;
}
bool IndexedFaceSet::SetNormalIndex(int pos, int * normalIndex) {
	int dim = 1;
	if (GetNormalPerVertex()) dim = 3;	

	if (pos < nNormalIndex_) {
		for(int h = 0; h < dim; h++) {
			normalIndex_[pos*3+h] = normalIndex[h];
		}
		return true;
	}
	return false;
}

bool IndexedFaceSet::GetColorIndex(int pos, int * colorIndex) {
	int dim = 1;
	if (GetColorPerVertex()) dim = 3;
	if (pos < nColorIndex_) {
		for(int h = 0; h < dim; h++) {
			colorIndex[h] = colorIndex_[pos*3+h];
		}
		return true;
	}
	return false;
}
bool IndexedFaceSet::SetColorIndex(int pos, int * colorIndex) {
	int dim = 1;
	if (GetColorPerVertex()) dim = 3;
	if (pos < nColorIndex_) {
		for(int h = 0; h < dim; h++) {
			colorIndex_[pos*3+h] = colorIndex[h];
		}
		return true;
	}
	return false;
}

bool IndexedFaceSet::GetTexCoordIndex(int pos, int * texCoordIndex) {
	if (pos < nTexCoordIndex_) {
		for(int h = 0; h < 3; h++) {
			texCoordIndex[h] = texCoordIndex_[pos*3+h];
		}
		return true;
	}
	return false;
}
bool IndexedFaceSet::SetTexCoordIndex(int pos, int * texCoordIndex) {
	if (pos < nTexCoordIndex_) {
		for(int h = 0; h < 3; h++) {
			texCoordIndex_[pos*3+h] = texCoordIndex[h];
		}
		return true;
	}
	return false;
}

void IndexedFaceSet::FreeMem()
{
	if (coord_) delete [] coord_;
	if (color_) delete [] color_;
	if (normal_) delete [] normal_;
	if (texCoord_) delete [] texCoord_;
	nCoord_ = 0;
	nColor_ = 0;
	nNormal_ = 0;
	nTexCoord_ = 0;

	ccw_ = true;
	solid_ = true;
	convex_ = true;
	colorPerVertex_ = true;
	normalPerVertex_ = true;

	creaseAngle_ = 0.0;

	delete [] coordIndex_;
	delete [] normalIndex_;
	delete [] colorIndex_;
	delete [] texCoordIndex_;

	nCoordIndex_ = 0;
	nColorIndex_ = 0;
	nNormalIndex_ = 0;
	nTexCoordIndex_ = 0;

}
void IndexedFaceSet::FreeInt(int ** tab, int dim)
{
	if (tab) {
		for (int i = 0; i < dim; i++) {
			delete [] tab[i];
		}
		delete [] tab;
	}
}

bool IndexedFaceSet::LoadIFSVRML2(char * fileName) {
	FreeMem();

	std::vector<float> coord;
	std::vector<float> color;
	std::vector<float> normal;
	std::vector<float> texCoord;

	std::vector<int> coordIndex;
	std::vector<int> colorIndex;
	std::vector<int> normalIndex;
	std::vector<int> texCoordIndex;

	FileIO vrml(fileName, "r");
	if (!vrml.IsOpned()) return false;

//	printf("Parsing ");

	char word[1024];
	int t = 0;
	vrml.ReadLine();

	// we search for the first IndexedFaceSet object
	bool foundIFS  = vrml.Goto("IndexedFaceSet", word);
	if (!foundIFS) return false;
	int notEOF = 1;
	int readWord = 1;
	while(notEOF) {
		readWord = 1;

		//---------------------
		if (!strcmp(word, "creaseAngle")) {
			vrml.GetWordAll(word);
			vrml.GetFloat(word,creaseAngle_);
		}
		//---------------------
		if (!strcmp(word, "normalPerVertex")) {
			vrml.GetWordAll(word);
			if (!strcmp(word, "FALSE")) {
				normalPerVertex_ = false;
			}
			else {
				normalPerVertex_ = true;
			}
		}
		//---------------------
		if (!strcmp(word, "colorPerVertex")) {
			vrml.GetWordAll(word);
			if (!strcmp(word, "FALSE")) {
				colorPerVertex_ = false;
			}
			else {
				colorPerVertex_ = true;
			}
		}
		//---------------------
		if (!strcmp(word, "convex")) {
			vrml.GetWordAll(word);
			if (!strcmp(word, "FALSE")) {
				convex_ = false;
			}
			else {
				convex_ = true;
			}
		}
		//---------------------
		if (!strcmp(word, "ccw")) {
			vrml.GetWordAll(word);
			if (!strcmp(word, "FALSE")) {
				ccw_ = false;
			}
			else {
				ccw_ = true;
			}
		}
		//---------------------
		if (!strcmp(word, "solid")) {
			vrml.GetWordAll(word);
			if (!strcmp(word, "FALSE")) {
				solid_ = false;
			}
			else {
				solid_ = true;
			}
		}
		//---------------------
		// we get the normals
		if (!strcmp(word, "Normal")) {
			vrml.Goto("vector", word);			
			while (	vrml.GetWordAll(word) ) {
				if (word[0] == '#') {// we skip commentaries
					vrml.GotoEOL(word);
				}
				else {
					float f = 0.0f;
					bool isFloat = vrml.GetFloat(word,f);
					if (isFloat) {
						normal.push_back(f);
					}
					else {
						readWord = 0;
						break;
					}
				}
			}
		}
		//---------------------
		// we get the colors
		if (!strcmp(word, "Color")) {
			vrml.Goto("color", word);			
			while (	vrml.GetWordAll(word) ) {
				printf("%s\n", word);
				if (word[0] == '#') {// we skip commentaries
					vrml.GotoEOL(word);
				}
				else {
					float f = 0.0f;
					bool isFloat = vrml.GetFloat(word,f);
					if (isFloat) {
						color.push_back(f);
					}
					else {
						readWord = 0;
						break;
					}
				}
			}
		}
		//---------------------
		// we get the coordinates
		if (!strcmp(word, "Coordinate")) {
			vrml.Goto("point", word);			
			while (	vrml.GetWordAll(word) ) {
				if (word[0] == '#') {// we skip commentaries
					vrml.GotoEOL(word);
				}
				else {
					float f = 0.0f;
					bool isFloat = vrml.GetFloat(word,f);
					if (isFloat) {
						coord.push_back(f);
					}
					else {
						readWord = 0;
						break;
					}
				}
			}
		}
		//-------------------------
		// we get the texture coordinates
		if (!strcmp(word, "TextureCoordinate")) {
			vrml.Goto("point", word);			
			while (	vrml.GetWordAll(word) ) {
				if (word[0] == '#') {// we skip commentaries
					vrml.GotoEOL(word);
				}
				else {
					float f = 0.0f;
					bool isFloat = vrml.GetFloat(word,f);
					if (isFloat) {
						texCoord.push_back(f);
					}
					else {
						readWord = 0;
						break;
					}
				}
			}
		}
		//-------------------------
		// we get the coordIndex
		if (!strcmp(word, "coordIndex")) {
			while (	vrml.GetWordAll(word) ) {
				if (word[0] == '#') {// we skip commentaries
					vrml.GotoEOL(word);
				}
				else {
					int i = -1;
					bool isInt = vrml.GetInt(word,i);
					if (isInt) {
						coordIndex.push_back(i);
					}
					else {
						readWord = 0;
						break;
					}
				}
			}
		}
		//-------------------------
		// we get the texCoordIndex
		if (!strcmp(word, "texCoordIndex")) {
			while (	vrml.GetWordAll(word) ) {
				if (word[0] == '#') {// we skip commentaries
					vrml.GotoEOL(word);
				}
				else {
					int i = -1;
					bool isInt = vrml.GetInt(word,i);
					if (isInt) {
						texCoordIndex.push_back(i);
					}
					else {
						readWord = 0;
						break;
					}
				}
			}
		}
		//-------------------------
		// we get the normalIndex
		if (!strcmp(word, "normalIndex")) {
			while (	vrml.GetWordAll(word) ) {
				if (word[0] == '#') {// we skip commentaries
					vrml.GotoEOL(word);
				}
				else {
					int i = -1;
					bool isInt = vrml.GetInt(word,i);
					if (isInt) {
						normalIndex.push_back(i);
					}
					else {
						readWord = 0;
						break;
					}
				}
			}
		}

		//-------------------------
		// we get the colorIndex
		if (!strcmp(word, "colorIndex")) {
			while (	vrml.GetWordAll(word) ) {
				if (word[0] == '#') {// we skip commentaries
					vrml.GotoEOL(word);
				}
				else {
					int i = -1;
					bool isInt = vrml.GetInt(word,i);
					if (isInt) {
						colorIndex.push_back(i);
					}
					else {
						readWord = 0;
						break;
					}
				}
			}
		}

		if (readWord) {
			notEOF = vrml.GetWordAll(word);
		}
	}



//	printf("Filling the data structures\n");

	nCoord_ = (int) coord.size()/3;
	nCoordIndex_ = (int) coordIndex.size()/4;
	nNormal_ = (int) normal.size()/3;
	if (normalPerVertex_) {
		nNormalIndex_ = (int) normalIndex.size()/4;
	}
	else {
		nNormalIndex_ = (int) normalIndex.size();
	}
	nColor_ = (int) color.size()/3;
	if (colorPerVertex_) {
		nColorIndex_ = (int) colorIndex.size()/4;
	}
	else {
		nColorIndex_ = (int) colorIndex.size();
	}
	nTexCoord_ = (int) texCoord.size()/2;
	nTexCoordIndex_ = (int) texCoordIndex.size()/4;

	// coord
//	printf("Filling coord\t");
	coord_ = new float [3 * nCoord_];
	for (int v = 0; v < 3 * nCoord_; v++) {
		coord_[v] = coord[v];
	}

	// normal
//	printf("Filling normal\t");
	normal_ = new float [3 * nNormal_];
	for (int v = 0; v < 3 * nNormal_; v++) {
		normal_[v] = normal[v];
	}

	// color
//	printf("Filling color\t");
	color_ = new float [3 * nColor_];
	for (int v = 0; v < 3 * nColor_; v++) {
		color_[v] = color[v];
	}
	// texCoord
//	printf("Filling texCoord\t");
	texCoord_ = new float [2 * nTexCoord_];
	for (int v = 0; v < 2 * nTexCoord_; v++) {
		texCoord_[v] = texCoord[v];
	}

	// fill coordIndex
//	printf("Filling coordIndex\t");
	coordIndex_ = new int [nCoordIndex_*3];
	int k=0;
	for (int p = 0; p < nCoordIndex_; p++) {
		for (int h = 0; h <3; h++) {
			coordIndex_[p*3+h] = coordIndex[k];k++;
		}
		k++;
	}	

	// fill normalIndex
//	printf("Filling normalIndex\t");

	if (normalPerVertex_){
		k=0;
		normalIndex_ = new int [3*nNormalIndex_];
		for (int p = 0; p < nNormalIndex_; p++) {
			for (int h = 0; h <3; h++) {
				normalIndex_[p*3+h] = normalIndex[k];k++;
			}
			k++;
		}
	}
	else {
		normalIndex_ = new int [nNormalIndex_];
		for (int p = 0; p < nNormalIndex_; p++) {
			normalIndex_[p] = normalIndex[p];
		}	
	}

	// fill colorIndex
//	printf("Filling colorIndex\t");
	if (colorPerVertex_){
		k=0;
		colorIndex_ = new int[3*nColorIndex_];
		for (int p = 0; p < nColorIndex_; p++) {
			for (int h = 0; h <3; h++) {
				colorIndex_[p*3+h] = colorIndex[k];k++;
			}
			k++;
		}	
	}
	else {
		colorIndex_ = new int[nColorIndex_];
		for (int p = 0; p < nColorIndex_; p++) {
			colorIndex_[p] = colorIndex[p];
		}
	}

	// fill texCoordIndex
//	printf("Filling texCoordIndex\t");
	texCoordIndex_ = new int [3*nTexCoordIndex_];
	k=0;
	for (int p = 0; p < nTexCoordIndex_; p++) {
		for (int h = 0; h <3; h++) {
			texCoordIndex_[p*3+h] = texCoordIndex[k];k++;
		}
		k++;
	}	
//	printf("Filling done!\n");
	return true;
}

void IndexedFaceSet::AddNeighborVertex2Vertex(int v1, int v2)
{
	int found = 0;	
	for (IntVect::iterator posc = vertex2Vertex_[v1].begin(); posc != vertex2Vertex_[v1].end(); ++posc) {
		if ((*posc) == v2) {
			found = 1;
			break;
		}
	}
	if (found == 0)
		vertex2Vertex_[v1].push_back(v2);
}

void IndexedFaceSet::ComputeVertex2Vertex(){
	for (int f = 0; f < nCoordIndex_; f++) {
		AddNeighborVertex2Vertex(coordIndex_[f*3+0], coordIndex_[f*3+1]);
		AddNeighborVertex2Vertex(coordIndex_[f*3+0], coordIndex_[f*3+2]);

		AddNeighborVertex2Vertex(coordIndex_[f*3+1], coordIndex_[f*3+0]);
		AddNeighborVertex2Vertex(coordIndex_[f*3+1], coordIndex_[f*3+2]);

		AddNeighborVertex2Vertex(coordIndex_[f*3+2], coordIndex_[f*3+0]);
		AddNeighborVertex2Vertex(coordIndex_[f*3+2], coordIndex_[f*3+1]);
	}
}

bool IndexedFaceSet::SaveIFSVRML2(char * fileName) {

	FILE * fid = fopen(fileName, "w");
//	printf("Saving %s\t", fileName);

	if (fid) {
		int p = 0;
		int i = 0;
//		char tmpbufTime[128];
//		char tmpbufDate[128];
//		_strtime( tmpbufTime );
//		_strdate( tmpbufDate );
		fprintf(fid,"#VRML V2.0 utf8\n");	    	
//		fprintf(fid,"# Produced by Khaled MAMOU, khaled.mamou@int-evry.fr. Copyright %s %s.\n", tmpbufTime, tmpbufDate);
		fprintf(fid,"# Produced by Khaled MAMOU, khaled.mamou@int-evry.fr. Copyright 2008.\n");
		fprintf(fid,"\n");
		fprintf(fid,"Group {\n");
		fprintf(fid,"	children [\n");
		fprintf(fid,"		Shape {\n");
		fprintf(fid,"			appearance Appearance {\n");
		fprintf(fid,"				material Material {\n");
		fprintf(fid,"					diffuseColor 0.6 0.4 0.6\n");  
		fprintf(fid,"					ambientIntensity 0.3333\n");
		fprintf(fid,"					specularColor 0.8 0.8 0.8\n");
		fprintf(fid,"					shininess 0.411\n");
		fprintf(fid,"					transparency 0\n");
		fprintf(fid,"				}\n");
		fprintf(fid,"				texture ImageTexture {\n");
		fprintf(fid,"				url \"texture.jpg\"\n");
		fprintf(fid,"				}\n");

		fprintf(fid,"			}\n");
		fprintf(fid,"			geometry IndexedFaceSet {\n");
		if (GetCCW()) {
			fprintf(fid,"				ccw TRUE\n");
		}
		else {
			fprintf(fid,"				ccw FALSE\n");
		}
		if (GetSolid()) {
			fprintf(fid,"				solid TRUE\n");
		}
		else {
			fprintf(fid,"				solid FALSE\n");
		}
		if (GetConvex()) {
			fprintf(fid,"				convex TRUE\n");
		}
		else {
			fprintf(fid,"				convex FALSE\n");
		}
		if (GetColorPerVertex()) {
			fprintf(fid,"				colorPerVertex TRUE\n");
		}
		else {
			fprintf(fid,"				colorPerVertex FALSE\n");
		}
		if (GetNormalPerVertex()) {
			fprintf(fid,"				normalPerVertex TRUE\n");
		}
		else {
			fprintf(fid,"				normalPerVertex FALSE\n");
		}
		if (GetNCoord() > 0) {
			fprintf(fid,"				coord DEF co Coordinate {\n");
			fprintf(fid,"					point [\n");
			float coord[3];
			for (p = 0; p < GetNCoord() ; p++) {
				GetCoord(p, coord);
				fprintf(fid,"						%f %f %f,\n", coord[0], coord[1], coord[2]);
			}
			fprintf(fid,"					]\n");
			fprintf(fid,"				}\n");
		}
		if (GetNCoordIndex() > 0) {
			fprintf(fid,"				coordIndex [ \n");
			int face[3];
			for (p = 0; p < (int) GetNCoordIndex(); p++) {
				GetCoordIndex(p, face);
				fprintf(fid,"						%i, %i, %i, -1,\n", face[0], face[1], face[2]);
			}	
			fprintf(fid,"				]\n");
		}
		if (GetNTexCoord() > 0) {
			fprintf(fid,"				texCoord DEF co TextureCoordinate {\n");
			fprintf(fid,"					point [\n");
			float TexCoord[2];
			for (p = 0; p < GetNTexCoord() ; p++) {
				GetTexCoord(p, TexCoord);
				fprintf(fid,"						%f %f,\n", TexCoord[0], TexCoord[1]);
			}
			fprintf(fid,"					]\n");
			fprintf(fid,"				}\n");
		}
		if (GetNTexCoordIndex() > 0) {
			fprintf(fid,"				texCoordIndex [ \n");
			int face[3];
			for (p = 0; p < (int) GetNTexCoordIndex(); p++) {
				GetTexCoordIndex(p, face);
				fprintf(fid,"						%i, %i, %i, -1,\n", face[0], face[1], face[2]);
			}	
			fprintf(fid,"				]\n");
		}

		if (GetNNormal() > 0) {
			fprintf(fid,"				normal DEF no Normal {\n");
			fprintf(fid,"					vector [\n");
			float Normal[3];
			for (p = 0; p < GetNNormal() ; p++) {
				GetNormal(p, Normal);
				fprintf(fid,"						%f %f %f,\n", Normal[0], Normal[1], Normal[2]);
			}
			fprintf(fid,"					]\n");
			fprintf(fid,"				}\n");
		}
		if (GetNNormalIndex() > 0) {
			fprintf(fid,"				normalIndex [ \n");
			int face[3];
			for (p = 0; p < (int) GetNNormalIndex(); p++) {
				GetNormalIndex(p, face);
				if ( GetNormalPerVertex()) {
					fprintf(fid,"						%i, %i, %i, -1,\n", face[0], face[1], face[2]);
				}
				else {
					fprintf(fid,"						%i,\n", face[0]);
				}
			}	
			fprintf(fid,"				]\n");
		}
		if (GetNColor() > 0) {
			fprintf(fid,"				color DEF cro Color {\n");
			fprintf(fid,"					vector [\n");
			float Color[3];
			for (p = 0; p < GetNColor() ; p++) {
				GetColor(p, Color);
				fprintf(fid,"						%f %f %f,\n", Color[0], Color[1], Color[2]);
			}
			fprintf(fid,"					]\n");
			fprintf(fid,"				}\n");
		}
		if (GetNColorIndex() > 0) {
			fprintf(fid,"				colorIndex [ \n");
			int face[3];
			for (p = 0; p < (int) GetNColorIndex(); p++) {
				GetColorIndex(p, face);
				if ( GetColorPerVertex()) {
					fprintf(fid,"						%i, %i, %i, -1,\n", face[0], face[1], face[2]);
				}
				else {
					fprintf(fid,"						%i,\n", face[0]);
				}
			}	
			fprintf(fid,"				]\n");
		}

		fprintf(fid,"			}\n");
		fprintf(fid,"		}\n");
		fprintf(fid,"	]\n");
		fprintf(fid,"}\n");	
		fclose(fid);
	}
	else {
		printf( "Saving error: file can't be created \n");
	}

	return true;
}

bool IndexedFaceSet::AllocateMem(){
	// coord
	if (nCoord_>0) {
		coord_ = new float [3 * nCoord_];
	}
	if (nNormal_>0) {
		normal_ = new float [3 * nNormal_];
	}
	if (nColor_>0) {
		color_ = new float [3 * nColor_];
	}
	if ( nTexCoord_>0) {
		texCoord_ = new float [2 * nTexCoord_];
	}
	if (nCoordIndex_ >0) {
		coordIndex_ = new int [3*nCoordIndex_];
	}	

	if (nNormalIndex_>0) {
		if (normalPerVertex_){
			normalIndex_ = new int [3*nNormalIndex_];
		}
		else {
			normalIndex_ = new int [nNormalIndex_];
		}
	}
	if (nColorIndex_>0) {
		if (colorPerVertex_){
			colorIndex_ = new int [3*nColorIndex_];
		}
		else {
			colorIndex_ = new int [nColorIndex_];
		}
	}
	if ( nTexCoordIndex_>0) {
		texCoordIndex_ = new int [3*nTexCoordIndex_];
	}	
	return true;
}
/*
------------------------
------------------------
*/

void IndexedFaceSet::AddNeighborVertex2Triangle(int v, int t)
{
	int found = 0;	
	for (IntVect::iterator posc = vertex2Triangle_[v].begin(); posc != vertex2Triangle_[v].end(); ++posc) {
		if ((*posc) == t) {
			found = 1;
			break;
		}
	}
	if (found == 0)
		vertex2Triangle_[v].push_back(t);
}

void IndexedFaceSet::ComputeVertex2Triangle(){
	for (int f = 0; f < nCoordIndex_; f++) {
		AddNeighborVertex2Triangle(coordIndex_[f*3+0], f);
		AddNeighborVertex2Triangle(coordIndex_[f*3+1], f);
		AddNeighborVertex2Triangle(coordIndex_[f*3+2], f);
	}
}
void IndexedFaceSet::AddNeighborTriangle2Triangle(int t1, int t2)
{
	int found = 0;	
	for (IntVect::iterator posc = triangle2Triangle_[t1].begin(); posc != triangle2Triangle_[t1].end(); ++posc) {
		if ((*posc) == t2) {
			found = 1;
			break;
		}
	}
	if (found == 0)
		triangle2Triangle_[t1].push_back(t2);
}
void IndexedFaceSet::ShowVertex2Triangle(int v){
	printf("v(%i)={ \t", v);
	for (int i=0; i <  (int) vertex2Triangle_[v].size();i++){
		int tr1 = vertex2Triangle_[v][i];
		printf("%i, ", tr1);
	}
	printf("}\n");

}

void IndexedFaceSet::ComputeTriangle2Triangle(){
	int coordIndex[3] = {-1, -1, -1};

	for (int t = 0; t < nCoordIndex_; t++) {
		GetCoordIndex(t, coordIndex);
		for (int k = 0; k < 3; k++) {
			for (IntVect::iterator pt0 = vertex2Triangle_[coordIndex[k]].begin(); pt0 != vertex2Triangle_[coordIndex[k]].end(); ++pt0){
				if ((*pt0) != t){
					int tr0 = (*pt0);
					for (int i=0; i <  (int) vertex2Triangle_[coordIndex[(k+1)%3]].size();i++){
						int tr1 = vertex2Triangle_[coordIndex[(k+1)%3]][i];
						if (tr1 == tr0){
							AddNeighborTriangle2Triangle(t, tr1);
						}
					}
				}
			}
		}
	}
}

bool IndexedFaceSet::SaveIFSOBJ(char * fileName) {

	FILE * fid = fopen(fileName, "w");
	printf("Saving %s\t", fileName);

	if (fid) {
		int p = 0;
		int i = 0;
		if (GetNCoord() > 0) {
			float coord[3];
			for (p = 0; p < GetNCoord() ; p++) {
				GetCoord(p, coord);
				fprintf(fid,"v %f %f %f\n", coord[0], coord[1], coord[2]);
			}
		}
		if (GetNTexCoord() > 0) {
			float TexCoord[2];
			for (p = 0; p < GetNTexCoord() ; p++) {
				GetTexCoord(p, TexCoord);
				fprintf(fid,"vt %f %f\n", TexCoord[0], TexCoord[1]);
			}
		}
		if (GetNNormal() > 0) {
			float Normal[3];
			for (p = 0; p < GetNNormal() ; p++) {
				GetNormal(p, Normal);
				fprintf(fid,"vn %f %f %f\n", Normal[0], Normal[1], Normal[2]);
			}
		}
		if (GetNCoordIndex() > 0) {
			int face[3];
			for (p = 0; p < (int) GetNCoordIndex(); p++) {
				GetCoordIndex(p, face);
				fprintf(fid,"f %i %i %i\n", face[0]+1, face[1]+1, face[2]+1);
			}	
		}
		fclose(fid);
	}
	else {
		printf( "Saving error: file can't be created \n");
	}
	return true;
}


bool IndexedFaceSet::LoadIFSOFF(char * fileName) {
	FreeMem();

	std::vector<float> coord;
	std::vector<int> coordIndex;

	FILE * offFile = fopen(fileName, "r");


	printf("Parsing ");
	int nCoord = 0;
	int nIndexCoord = 0;
	int nTemp = 0;
	char temp[1024];
	fscanf(offFile, "%s", temp);
	fscanf(offFile, "%i", &nCoord);
	fscanf(offFile, "%i", &nIndexCoord);
	fscanf(offFile, "%i", &nTemp);


	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;
	for (int v = 0; v < nCoord; v++) {
		fscanf(offFile, "%f", &x); coord.push_back(x);
		fscanf(offFile, "%f", &y); coord.push_back(y);
		fscanf(offFile, "%f", &z); coord.push_back(z);
	}
	int i = 0;
	int j = 0;
	int k = 0;
	int d = 0;
	for (int t = 0; t < nIndexCoord; t++) {
		fscanf(offFile, "%i", &d); 
		fscanf(offFile, "%i", &i); coordIndex.push_back(i);
		fscanf(offFile, "%i", &j); coordIndex.push_back(j);
		fscanf(offFile, "%i", &k); coordIndex.push_back(k);
		coordIndex.push_back(-1);
	}

	printf("Filling the data structures\n");

	nCoord_ = (int) coord.size()/3;
	nCoordIndex_ = (int) coordIndex.size()/4;

	// coord
	printf("Filling coord\t");
	coord_ = new float [3 * nCoord_];
	for (int v = 0; v < 3 * nCoord_; v++) {
		coord_[v] = coord[v];
	}

	// fill coordIndex
	printf("Filling coordIndex\t");
	coordIndex_ = new int [nCoordIndex_];
	k=0;
	for (int p = 0; p < nCoordIndex_; p++) {
		for (int h = 0; h <3; h++) {
			coordIndex_[p*3+h] = coordIndex[k];k++;
		}
		k++;
	}	
	printf("Filling done!\n");
	return true;
}


/*
bool IndexedFaceSet::LoadIFSVB(unsigned char *vb, int vbSize) {

//	FreeMem();

	std::vector<float> coord;
	std::vector<float> color;
	std::vector<float> normal;
	std::vector<float> texCoord;

	std::vector<int> coordIndex;
	std::vector<int> colorIndex;
	std::vector<int> normalIndex;
	std::vector<int> texCoordIndex;

	int nCoord		= 0;
	int nCoordTmp	= 0;
	int nNormal		= 0;
	int nTexCoord	= 0;

	BIFS::ostream st;
	st.write( ( char* )vb, vbSize );
	st.seekp( 0 );

	st.readInt( 4 );						// total size of the buffer

	int vbCount = st.readInt( 1 );			// number of vertex buffers

	for ( int i = 0; i < vbCount; i++ )
	{
		int nbNorm = 0;
		int nbTexCoords = 0;

		st.readInt( 4 );						// size of vertex buffer and index buffers
		st.readInt( 4 );						// size of vertex buffer

		bool hasBoneInfls = st.readInt( 1 );	// has bone influence

		// nCoord_		
		nCoordTmp = nCoord;
		int vCount = st.readInt( 4 );			// number of vertices - number of points in Object 
		nCoord += vCount;

		// Coord_
		for ( int j = 0; j < vCount; j++ )		// get coordinates _
		{
			float t;
			t = st.readFloat( 4 );				// X coordinate
			coord.push_back(t);

			t = st.readFloat( 4 );				// Y coordinate
			coord.push_back(t);

			t = st.readFloat( 4 );				// Z coordinate
			coord.push_back(t);

			if ( hasBoneInfls )
			{
				//skiping the bone influences
				st.readFloat( 4 );
				st.readFloat( 4 );
				st.readFloat( 4 );
				st.readFloat( 4 );
				st.readInt( 4 );
			}
		}

		//nNormal_
		nbNorm = st.readInt( 4 );					// number of normals
		nNormal += nbNorm;
		if(nbNorm > 0){
			//Normal_
			for(int k=0 ; k<nbNorm ; k++ ){			// get the normal coordinates
				float t = 0.0f;
				t = st.readFloat( 4 );
				normal.push_back(t);

				t = st.readFloat( 4 );
				normal.push_back(t);

				t = st.readFloat( 4 );
				normal.push_back(t);
			}
		}

		//nTexCoord_
		nbTexCoords = st.readInt( 4 );				// Tecture numbers
		nTexCoord += nbTexCoords;
		if (nbTexCoords > 0){
			//TexCoord_
			for(int k=0 ; k<nbTexCoords ; k++ ){	// get the texture coordinates
				float t = 0.0f;
				t = st.readFloat( 4 );
				texCoord.push_back(t);

				t = st.readFloat( 4 );
				texCoord.push_back(t);
			}
		}

		// get coordIndex_ & nCoordIndex_
		if ( i < vbCount )
		{

			int ibCount = st.readInt( 1 );					// Number of indexbuffers - number of shape

			//int ** indBuff = new int * [ibCount];
			int * intSize = new int [ibCount];

			for ( int j = 0; j < ibCount; j++ )
			{
				int iSize;
				iSize = st.readInt( 4 );

				st.readInt( 4 );							// skiping the index buffer ?????

				int indCount = st.readInt( 4 );				// Number of indices - default 3 ?

				intSize[j] = indCount;
				//indBuff[j] = 0;
				//indBuff[j] = new int [indCount];
				//if(!indBuff[j]) return false; 

				for ( int k = 0; k < indCount; k++ ){
					int i = -1;
					i = st.readInt( 4 );
					//indBuff[j][k] = i;
					coordIndex.push_back(i + nCoordTmp);
				}

				st.readInt( 4 );							//?????????????
				st.readInt( 4 );							//?????????????
			}
		}
	}

	nCoord_			= (int) coord.size()/3;
	nCoordIndex_	= (int) coordIndex.size()/3;
//	nNormal_		= (int) normal.size()/3;
//	nTexCoord_		= (int) texCoord.size()/2;

	// coord
	coord_ = new float [3 * nCoord_];
	fprintf(stderr,"nCoord_ = %d\n",nCoord_);
	for (int v = 0; v < 3 * nCoord_; v++) {		
		coord_[v] = coord[v];
	}

	// normal
	normal_ = new float [3 * nNormal_];
	for (int v = 0; v < 3 * nNormal_; v++) {
		normal_[v] = normal[v];
	}

	// texCoord
	texCoord_ = new float [2 * nTexCoord_];
	for (int v = 0; v < 2 * nTexCoord_; v++) {
		texCoord_[v] = texCoord[v];
	}

	// fill coordIndex
	coordIndex_ = new int [nCoordIndex_*3];
	int k=0;
	for (int p = 0; p < nCoordIndex_; p++) {
		for (int h = 0; h <3; h++) {
			coordIndex_[p*3+h] = coordIndex[k];k++;
		}
	}	

	return true;
}
*/
/*
bool IndexedFaceSet::read(char* fname)
{
  FreeMem();
  bool success = false;
  FILE* fp = fopen(fname,"r");
  if(fp!=(FILE*)0)
    {
      char header[16];
      memset(header,'\0',16);
      fscanf(fp,"%15c",header);
      if(!strcmp(header,VRML_HEADER))
	{
	  vector<char> tkn;

	  // get next token
	  int  nc=0;
	  while((nc=getToken(fp,tkn))>0) {

	    // until the first IndexedFaceSet is found
	    if(!strcmp(tkn.begin(),"IndexedFaceSet"))
	      {
		// next token must be "{"
		nc=getToken(fp,tkn);
		if(nc<=0 || strcmp(tkn.begin(),"{"))
		  break;

		while((nc=getToken(fp,tkn))>0) {

		  if(!strcmp(tkn.begin(),"ccw") ||
		     !strcmp(tkn.begin(),"convex") ||
		     !strcmp(tkn.begin(),"solid") ||
		     !strcmp(tkn.begin(),"colorPerVertex") ||
		     !strcmp(tkn.begin(),"normalPerVertex"))
		    {
		      int type =
			(!strcmp(tkn.begin(),"ccw"))?1:
			(!strcmp(tkn.begin(),"convex"))?2:
			(!strcmp(tkn.begin(),"solid"))?3:
			(!strcmp(tkn.begin(),"colorPerVertex"))?4:
			(!strcmp(tkn.begin(),"normalPerVertex"))?5:
			0;

		      bool value = true;
		      // next token should be TRUE or FALSE
		      nc=getToken(fp,tkn);
		      if(nc<=0)
			break;
		      else if(!strcmp(tkn.begin(),"TRUE"))
			value = true;
		      else if(!strcmp(tkn.begin(),"FALSE"))
			value = false;
		      else
			break;
		      switch(type)
			{
			case 1: _ccw             = value; break;
			case 2: _convex          = value; break;
			case 3: _solid           = value; break;
			case 4: _colorPerVertex  = value; break;
			case 5: _normalPerVertex = value; break;
			}
		    }
		  else if(!strcmp(tkn.begin(),"creaseAngle"))
		    {
		      float value = 0.0;
		      // next token should be a non-negative float
		      nc=getToken(fp,tkn);
		      if(nc>0 && sscanf(tkn.begin(),"%f",&value)==1 && value>=0)
			_creaseAngle = value;
		      else
			break;
		    }
		  else if(!strcmp(tkn.begin(),"color")  ||
			  !strcmp(tkn.begin(),"coord")  ||
			  !strcmp(tkn.begin(),"normal") ||
			  !strcmp(tkn.begin(),"texCoord"))
		    {
		      int type =
			(!strcmp(tkn.begin(),"color"))?1:
			(!strcmp(tkn.begin(),"coord"))?2:
			(!strcmp(tkn.begin(),"normal"))?3:
			(!strcmp(tkn.begin(),"texCoord"))?4:
			0;

		      nc=getToken(fp,tkn);
		      // skip DEF
		      if(!strcmp(tkn.begin(),"DEF"))
			{
			  // skip DEF name
			  nc=getToken(fp,tkn);
			  // get next token
			  nc=getToken(fp,tkn);
			}
		      if(nc<=0                                              ||
			 (type==1 && strcmp(tkn.begin(),"Color"))           ||
			 (type==2 && strcmp(tkn.begin(),"Coordinate"))      ||
			 (type==3 && strcmp(tkn.begin(),"Normal"))          ||
			 (type==4 && strcmp(tkn.begin(),"TextureCoordinate")))
			break;

		      nc=getToken(fp,tkn);
		      if(nc<=0 || strcmp(tkn.begin(),"{"))
			break;

		      nc=getToken(fp,tkn);
		      if(nc<=0                                     ||
			 (type==1 && strcmp(tkn.begin(),"color"))  ||
			 (type==2 && strcmp(tkn.begin(),"point"))  ||
			 (type==3 && strcmp(tkn.begin(),"vector")) ||
			 (type==4 && strcmp(tkn.begin(),"point")))
			break;

		      // next token must be "["
		      nc=getToken(fp,tkn);
		      if(nc<=0 || strcmp(tkn.begin(),"["))
			break;

		      // MFFloat
		      float value;
		      while((nc=getToken(fp,tkn))>0)
			if(sscanf(tkn.begin(),"%f",&value)<=0)
			  break;
			else switch(type)
			  {
			  case 1: _color.push_back(value);    break;
			  case 2: _coord.push_back(value);    break;
			  case 3: _normal.push_back(value);   break;
			  case 4: _texCoord.push_back(value); break;
			  }

		      // next token must be "]"
		      if(nc<=0 || strcmp(tkn.begin(),"]"))
			break;

		      nc=getToken(fp,tkn);
		      if(nc<=0 || strcmp(tkn.begin(),"}"))
			break;

		    }
		  else if(!strcmp(tkn.begin(),"colorIndex") ||
			  !strcmp(tkn.begin(),"coordIndex") ||
			  !strcmp(tkn.begin(),"normalIndex") ||
			  !strcmp(tkn.begin(),"texCoordIndex"))
		    {
		      int type =
			(!strcmp(tkn.begin(),"colorIndex"))?1:
			(!strcmp(tkn.begin(),"coordIndex"))?2:
			(!strcmp(tkn.begin(),"normalIndex"))?3:
			(!strcmp(tkn.begin(),"texCoordIndex"))?4:
			0;

		      // next token must be "["
		      nc=getToken(fp,tkn);
		      if(nc<=0 || strcmp(tkn.begin(),"["))
			break;

		      // MFInt32
		      int value;
		      while((nc=getToken(fp,tkn))>0)
			if(sscanf(tkn.begin(),"%d",&value)<=0)
			  break;
			else switch(type)
			  {
			  case 1: 
			    if(value != -1)
			      _colorIndex.push_back(value);    
			    else
			      if(_colorIndex.size() > 0)
				if(_colorIndex[_colorIndex.size()-1] != -1)
				  _colorIndex.push_back(value);    
			    break;
			  case 2: 
			    if(value != -1)
			      _coordIndex.push_back(value);    
			    else
			      if(_coordIndex.size() > 0)
				if(_coordIndex[_coordIndex.size()-1] != -1)
				  _coordIndex.push_back(value);    
			    break;
			  case 3: 
			    if(value != -1)
			      _normalIndex.push_back(value);    
			    else
			      if(_normalIndex.size() > 0)
				if(_normalIndex[_normalIndex.size()-1] != -1)
				  _normalIndex.push_back(value);    
			    break;
			  case 4: 
			    if(value != -1)
			      _texCoordIndex.push_back(value);    
			    else
			      if(_texCoordIndex.size() > 0)
				if(_texCoordIndex[_texCoordIndex.size()-1] != -1)
				  _texCoordIndex.push_back(value);    
			    break;
			  }

		      // next token must be "]"
		      if(nc<=0 || strcmp(tkn.begin(),"]"))
			break;
		    }
		  else
		    break;
		}

		// next token must be "}"
		nc=getToken(fp,tkn);
		if(nc==1 && !strcmp(tkn.begin(),"}"))
		  success = true;

		break;
	      }
	  }
	}
    }

  return success;
}
*/

void IndexedFaceSet::SetMinMaxCoord()
{
	if(nCoord_ != 0)
	{
		int k, i, j;
		for(k = 0 ; k < 3 ; k++)
		{
			minCoord[k] = MAX_VLAUE;
			maxCoord[k] = MIN_VLAUE;
		}

		for(i = 0 ; i < (int)nCoord_ ; i++)
		{
			for(j = 0 ; j < 3 ; j++)
			{			
				if(coord_[(i*3)+j] < minCoord[j])
				{
					minCoord[j] = coord_[(i*3)+j];
				}
				if(coord_[(i*3)+j] > maxCoord[j])
				{
					maxCoord[j] = coord_[(i*3)+j];
				}			
			}
		}
		
		for(j = 0 ; j < 3 ; j++)
		{
			rangeCoord[j] = (maxCoord[j] - minCoord[j]);
		}
		
	}
}

void IndexedFaceSet::SetMinMaxColor()
{
	if(nColor_ != 0)
	{
		int k, i, j;
		for(k = 0 ; k < 3 ; k++)
		{
			minColor[k] = MAX_VLAUE;
			maxColor[k] = MIN_VLAUE;
		}

		for(i = 0 ; i < (int)nColor_ ; i++)
		{
			for(j = 0 ; j < 3 ; j++)
			{			
				if(color_[(i*3)+j] < minColor[j])
				{
					minColor[j] = color_[(i*3)+j];
				}
				if(color_[(i*3)+j] > maxColor[j])
				{
					maxColor[j] = color_[(i*3)+j];
				}			
			}
		}
		for(j = 0 ; j < 3 ; j++)
		{
			rangeColor[j] = (maxColor[j] - minColor[j]);

		}


	}
}

void IndexedFaceSet::SetMinMaxNormal()
{
	if(nNormal_ != 0)
	{
		int k, i, j;
		for(k = 0 ; k < 3 ; k++)
		{
			minNormal[k] = MAX_VLAUE;
			maxNormal[k] = MIN_VLAUE;
		}

		for(i = 0 ; i < (int)nNormal_ ; i++)
		{
			for(j = 0 ; j < 3 ; j++)
			{			
				if(normal_[(i*3)+j] < minNormal[j])
				{
					minNormal[j] = normal_[(i*3)+j];
				}
				if(normal_[(i*3)+j] > maxNormal[j])
				{
					maxNormal[j] = normal_[(i*3)+j];
				}			
			}
		}
		for(j = 0 ; j < 3 ; j++)
		{
			rangeNormal[j] = (maxNormal[j] - minNormal[j]);
		}

	}
}

void IndexedFaceSet::SetMinMaxTexCoord()
{
	if(nTexCoord_ != 0)
	{
		int k, i, j;
		for(k = 0 ; k < 2 ; k++)
		{
			minTexCoord[k] = MAX_VLAUE;
			maxTexCoord[k] = MIN_VLAUE;
		}

		for(i = 0 ; i < (int)nTexCoord_ ; i++)
		{
			for(j = 0 ; j < 2 ; j++)
			{			
				if(texCoord_[(i*2)+j] < minTexCoord[j])
				{
					minTexCoord[j] = texCoord_[(i*2)+j];
				}
				if(texCoord_[(i*2)+j] > maxTexCoord[j])
				{
					maxTexCoord[j] = texCoord_[(i*2)+j];
				}			
			}
		}
		for(j = 0 ; j < 2 ; j++)
		{
		
			rangeTexCoord[j] = (maxTexCoord[j] - minTexCoord[j]);
			

		}

	}
}