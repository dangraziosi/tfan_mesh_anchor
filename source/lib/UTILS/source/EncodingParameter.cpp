//-------------------------------------------------------------------------
// 
// Copyright Institut Telecom, ETRI and Hanyang University 2008, 2009
// Licensed Materials, Program Property of Institut Telecom, ETRI and Hanyang University
// 
// This software module is an implementation of a part of one or more
// MPEG-4 tools as specified by the MPEG-4 standard (ISO/IEC 14496).
// ISO/IEC gives users of the MPEG-4 standard (ISO/IEC 14496) free
// license to this software module or modifications thereof for use in
// hardware or software products claiming conformance to the MPEG-4
// standard (ISO/IEC 14496).  Those intending to use this software module
// in hardware or software products are advised that its use may infringe
// existing patents.  The original developer of this software module and
// his/her company, the subsequent editors and their companies, and
// ISO/IEC have no liability for use of this software module or
// modifications thereof in an implementation.  Copyright is not released
// for non MPEG-4 standard (ISO/IEC 14496) conforming products.  Institut Telecom 
// ETRI and Hanyang University retain full right to use the code 
// for his/her own purpose, assign or donate the code to a third party and
// to inhibit third parties from using the code for non MPEG-4 standard 
// (ISO/IEC 14496) conforming products. This copyright notice must be included
// in all copies or derivative works.
// 
//    AUTHORS:
//    Khaled Mammou
//    Marius Preda
//    Seung Wook Lee
//    Daiyong Kim
//    KyoungSoo Son
//
//
//    http://www-artemis.it-sudparis.eu/
//    http://www.etri.re.kr/
//    http://dmlab.hanyang.ac.kr/
//    
//  
//    Marius Preda       (marius.preda@int-evry.fr)
//    Khaled Mammou      (khaled_mamou@yahoo.fr)
//    Seung Wook Lee     (tajinet@etri.re.kr)  
//    Daiyong Kim        (dykim@dmlab.hanyang.ac.kr)    
//    KyoungSoo Son      (bonokensin@dmlab.hanyang.ac.kr)
//
//-------------------------------------------------------------------------



#include "EncodingParameter.h"
#include "utils.h"

EncodingParams::EncodingParams()
{
	Initializate();
}

EncodingParams::~EncodingParams()
{
}

void EncodingParams::Initializate()
{
	m_nQCoordBits = DEFAULTQP;
	m_nQTexCoordBits = DEFAULTQP;
	m_nQTexCoordHeight = DEFAULTQP;
	m_nQTexCoordWidth = DEFAULTQP;
	m_nQNormalBits = DEFAULTQP;
	m_nQColorBits = DEFAULTQP;
	m_nQOtherAttributesBits = DEFAULTQP;
	m_encodingMode = QBCR; 
	m_coordPredMode= DEFAULTPRED;
	m_texCoordPredMode = DEFAULTPRED;
	m_normalPredMode = DEFAULTPRED;
	m_colorPredMode = DEFAULTPRED;
	m_otherAttributesPredMode = DEFAULTPRED;
	m_coordIndexPredMode= DEFAULTPRED;
	m_texCoordIndexPredMode = DEFAULTPRED;
	m_normalIndexPredMode = DEFAULTPRED;
	m_colorIndexPredMode = DEFAULTPRED;
	m_otherAttributesIndexPredMode = DEFAULTPRED;
	m_vertexOrderPres = FALSE;
	m_triangleOrderPres = FALSE;
	m_trianglePermPres = FALSE;
	m_explicitCoordQuantization = FALSE;
	m_explicitNormalQuantization = FALSE;
	m_explicitColorQuantization = FALSE;
	m_explicitTexCoordQuantization = FALSE;

}

